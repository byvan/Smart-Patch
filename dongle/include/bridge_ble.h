/**
 * @file bridge_ble.h
 * @brief      BLE Bridge stack definitions.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       January 2021
 */

#ifndef __BRIDGE_BLE_H
#define __BRIDGE_BLE_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <ble_gap.h>

/**
 * @addtogroup   BLEBridge
 * @{
 */

/**
 * @defgroup   BLEBridge_Stack BLE Bridge BLE Stack definitions
 * @{
 */

/* Exported constants --------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

typedef union {
    ble_gap_adv_report_type_t   bitfield;
    uint16_t                    value;
} adv_report_type_t;

typedef struct {
    adv_report_type_t   type;
    ble_data_t          data;
} adv_data_t;

typedef struct {

    ble_gap_addr_t  peer_addr_new;

    /* @todo remove union */
    union {
        uint8_t     pAddr[8];
        uint64_t    lAddr;
    } peer_addr;

    adv_data_t  adv_data;
    adv_data_t  sr_data;
    bool        sr_data_valid;
} ScanData_t;

/* Exported macros -----------------------------------------------------------*/

/* Exported variables --------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/

/**
 * @brief      { function_description }
 *
 * @return     { description_of_the_return_value }
 */
uint32_t BLE_CHAR_Discover(uint16_t serv_handle);

/**
 * @brief      { function_description }
 *
 * @param[in]  serv_uuid  The service uuid
 * @param[in]  char_uuid  The character uuid
 * @param      pData      The data
 * @param      size       The size
 *
 * @return     { description_of_the_return_value }
 */
uint32_t BLE_CHAR_Read(const uint16_t serv_uuid, const uint16_t char_uuid, uint8_t** pData, uint16_t* size);

/**
 * @brief      { function_description }
 *
 * @param[in]  serv_uuid  The serv uuid
 * @param[in]  char_uuid  The character uuid
 * @param[in]  p_data     The data
 * @param[in]  size       The size
 *
 * @return     { description_of_the_return_value }
 */
uint32_t BLE_CHAR_Write(const uint16_t serv_uuid, const uint16_t char_uuid, const uint8_t* p_data, const uint16_t size);

/**
 * @brief      { function_description }
 *
 * @param[in]  pAdrr  The adrr
 *
 * @return     { description_of_the_return_value }
 */
uint32_t BLE_Connect_Address(const uint8_t* pAdrr);

/**
 * @brief      { function_description }
 *
 * @param[in]  index  The index
 *
 * @return     { description_of_the_return_value }
 */
uint32_t BLE_Connect_Index(uint8_t index);

/**
 * @brief      { function_description }
 *
 * @param[in]  serv_handle  The serv handle
 * @param      pBuffer      The buffer
 * @param[in]  size         The size
 */
void BLE_GetChars(const uint16_t serv_handle, uint16_t* pBuffer, const uint16_t size);

/**
 * @brief      { function_description }
 *
 * @param[in]  serv_handle  The serv handle
 *
 * @return     { description_of_the_return_value }
 */
uint16_t BLE_GetNumberOfCharacteristics(uint16_t serv_handle);

/**
 * @brief      { function_description }
 *
 * @return     { description_of_the_return_value }
 */
uint8_t BLE_GetNumberOfScannedDevices(void);

/**
 * @brief      { function_description }
 *
 * @return     { description_of_the_return_value }
 */
uint16_t BLE_GetNumberOfServices(void);

/**
 * @brief      { function_description }
 *
 * @param[in]  index  The index
 *
 * @return     { description_of_the_return_value }
 */
const ScanData_t* const BLE_GetScannedDevice(uint8_t index);

/**
 * @brief      { function_description }
 *
 * @param      pBuffer  The buffer
 * @param[in]  size     The size
 */
void BLE_GetServices(uint16_t* pBuffer, const uint16_t size);

/**
 * @brief      Initialises the BLE stack.
 *
 * @return     { description_of_the_return_value }
 */
uint32_t BLE_Init(void);

/**
 * @brief      { function_description }
 *
 * @return     { description_of_the_return_value }
 */
uint32_t BLE_Scan(void);

/**
 * @brief      { function_description }
 *
 * @param[in]  start_handle  The start handle
 *
 * @return     { description_of_the_return_value }
 */
uint32_t BLE_SERV_Discover(uint16_t start_handle);

/**
 * @}
 */

/**
 * @}
 */

#endif /* __BRIDGE_BLE_H included */
