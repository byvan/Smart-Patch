/**
 * @file serial_commands.h
 * @brief      BLE Bridge serial command definitions.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       January 2021
 */

#ifndef __SERIAL_COMMANDS_H
#define __SERIAL_COMMANDS_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/**
 * @addtogroup   BLEBridge_Serial
 * @{
 */

/**
 * @defgroup   Serial_Commands BLEBridge Serial Commands
 * @{
 */

/* Exported constants --------------------------------------------------------*/

#define COMMAND_FAM_BLE                         0x0A
#define COMMAND_IDX_SCAN                        0x01
#define COMMAND_IDX_GET_SCANNED                 0x02
#define COMMAND_IDX_GET_DEVICE                  0x03
#define COMMAND_IDX_CONNECT                     0x10

#define COMMAND_FAM_SERV                        0x0B
#define COMMAND_IDX_DISCOVER                    0x01
#define COMMAND_IDX_GET_DISC                    0x02

#define COMMAND_FAM_CHAR                        0x0C
// #define COMMAND_IDX_DISCOVER                    0x01
// #define COMMAND_IDX_GET_DISC                    0x02
#define COMMAND_IDX_READ                        0x03
#define COMMAND_IDX_WRITE                       0x04

#define COMMAND_FAM_DEVICE                      0xF0
#define COMMAND_IDX_STATUS                      0x01

/* Exported types ------------------------------------------------------------*/

/* Exported macros -----------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/

/**
 * @brief      { function_description }
 *
 * @param[in]  pCommand  The command
 * @param[in]  size      The size
 * @param      err       The error
 * @param      confirm   The confirm
 *
 * @return     { description_of_the_return_value }
 */
uint32_t Command_Parse_BLE(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm);

/**
 * @brief      { function_description }
 *
 * @param[in]  pCommand  The command
 * @param[in]  size      The size
 * @param      err       The error
 * @param      confirm   The confirm
 *
 * @return     { description_of_the_return_value }
 */
uint32_t Command_Parse_CHAR(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm);

/**
 * @brief      { function_description }
 *
 * @param[in]  pCommand  The command
 * @param[in]  size      The size
 * @param      err       The error
 * @param      confirm   The confirm
 *
 * @return     { description_of_the_return_value }
 */
uint32_t Command_Parse_DEVICE(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm);

/**
 * @brief      { function_description }
 *
 * @param[in]  pCommand  The command
 * @param[in]  size      The size
 * @param      err       The error
 * @param      confirm   The confirm
 *
 * @return     { description_of_the_return_value }
 */
uint32_t Command_Parse_SERV(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm);

/**
 * @}
 */

/**
 * @}
 */

#endif /* __SERIAL_COMMANDS_H included */
