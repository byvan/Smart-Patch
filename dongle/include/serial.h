/**
 * @file serial.h
 * @brief      BLE Bridge serial interface definitions.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       January 2021
 */

#ifndef __SERIAL_H
#define __SERIAL_H

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>

/**
 * @addtogroup BLEBridge
 * @{
 */

/**
 * @defgroup   BLEBridge_Serial BLE Bridge Serial Module
 * @{
 */

/* Exported constants --------------------------------------------------------*/

/*! Execution command character ('!') for the command transmission */
#define SERIAL_CMD_EXEC                         0x21
/*! Setter command character ('<') for the command transmission */
#define SERIAL_CMD_GETTER                       0x3C
/*! Setter command character ('>') for the command transmission */
#define SERIAL_CMD_SETTER                       0x3E
/*! Termination character for the command transmission */
#define SERIAL_CMD_TERMINATOR                   0xFA

/*! Helper macro to write the response */
#define WRITE_RESPONSE( data, ... )             printf("%c" data "\r\n", SERIAL_CMD_GETTER, ##__VA_ARGS__ )
/*! Helper macro to write data to the interface*/
#define WRITE_DATA( data )                      
/*! Helper macro to return an error */
#define WRITE_ERROR( err )                      WRITE_RESPONSE("ERR=%c", err)
/*! Helper macro to return ok */
#define WRITE_OK( )                             WRITE_RESPONSE("OK+%c", 0)

// #define WRITE_ERROR()                           printf("%cERR\r\n", SERIAL_CMD_GETTER)

/* Exported types ------------------------------------------------------------*/

/* Exported macros -----------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/

/**
 * @brief      { function_description }
 */
void Serial_Flush(void);

/**
 * @brief      Initialises the serial interface of the dongle.
 *
 * @return     Any error that comes up during initialisation.
 */
uint32_t Serial_Init(void);

/**
 * @brief      Processes any serial event, that might have come up.
 *
 * @param[out] pending  The pending flag that will be or-ed with it's initial
 *                      value.
 *
 * @return     Any error that comes up during processing.
 */
uint32_t Serial_Process(bool* const pending);

/**
 * @brief      { function_description }
 *
 * @param[in]  pData  The data
 * @param[in]  size   The size
 *
 * @return     { description_of_the_return_value }
 */
uint32_t Serial_Write(const uint8_t* pData, const uint32_t size);

/**
 * @}
 */

/**
 * @}
 */

#endif /* __SERIAL_H included */
