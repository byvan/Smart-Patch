/**
 * @file device.c
 * @brief      BLE Bridge device implementation.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       January 2021
 */

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <ble_gap.h>
#include <nrf_drv_gpiote.h>
#include <nrf_pwr_mgmt.h>

#include "bridge_ble.h"
#include "device.h"
#include "macros.h"
#include "serial.h"

#include <nrf_delay.h>

/**
 * @addtogroup   BLEBridge_Device
 * @{
 */

/**
 * @defgroup   BLEBridge_Device_ic Device Module internal components
 * @{
 */

/* Private defines -----------------------------------------------------------*/

#define BIT_ISSET( reg, bit)    (*reg & (1 << bit))
#define BIT_SET( reg, bit )     (*reg |= (1 << bit))
#define BIT_CLEAR( reg, bit )   (*reg &= ~(1 << bit))

#define ESC_16bit               "%c%c"
#define ESC_32bit               "%c%c%c%c"

#define UNPACK_16bit( x )       *((uint8_t*) x), *(((uint8_t*) x) + 1)
#define UNPACK_32bit( x )       *((uint8_t*) x), *(((uint8_t*) x) + 1), *(((uint8_t*) x) + 2), *(((uint8_t*) x) + 3)

#define LED_SCAN            13

#define QUEUE_LENGTH        5

/* Private macros ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

typedef struct {
    DeviceOperationType_t   type;
    void*                   args;
    uint16_t                arglen;
} DeviceTask_t;

/* Private function prototypes -----------------------------------------------*/

/**
 * @brief      { function_description }
 *
 * @todo       add return value parameter.
 */
static void ble_complete(void);

/**
 * @brief      { function_description }
 */
static void ble_connected(void);

/**
 * @brief      { function_description }
 *
 * @param[in]  index  The index
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t ble_connect_device(uint8_t* arg, uint16_t arglen);

/**
 * @brief      { function_description }
 */
static void ble_disconnected(void);

/**
 * @brief      { function_description }
 *
 * @param[in]  index  The index
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t ble_get_deviceinfo(uint8_t index);

/**
 * @brief      { function_description }
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t ble_get_scanned(void);

/**
 * @brief      { function_description }
 *
 * @return     { description_of_the_return_value }
 */
// static uint32_t ble_parse_scanned(void);

/**
 * @brief      { function_description }
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t ble_scan(void);

/**
 * @brief      { function_description }
 *
 * @param[in]  handle  The handle
 */
static void ble_scancomplete(uint16_t handle);

/**
 * @brief      { function_description }
 *
 * @param[in]  serv_handle  The serv handle
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t char_discover(uint16_t serv_handle);

/**
 * @brief      { function_description }
 *
 * @param[in]  serv_handle  The serv handle
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t char_get(uint16_t serv_handle);

/**
 * @brief      { function_description }
 *
 * @param[in]  char_uuid  The character uuid
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t char_read(uint16_t* params);

/**
 * @brief      { function_description }
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t char_write(const uint16_t* params, const uint8_t* p_data, const uint16_t size);

/**
 * @brief      Gets the status of the device.
 *
 * @return     The status.
 */
static uint8_t get_status(void);

/**
 * @brief      { function_description }
 *
 * @param[in]  op    The operation
 */
static uint8_t schedule(DeviceOperationType_t op, void* args, uint16_t arglen);

/**
 * @brief      { function_description }
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t serv_discover(void);

/**
 * @brief      { function_description }
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t serv_get(void);

/* Private variables ---------------------------------------------------------*/

static uint8_t Status = 0;
static uint16_t res_handle = -1;
// static bool ble_busy = false;
static DeviceTask_t    Queue[QUEUE_LENGTH];     /*!< Queue for scheduled operations */
static DeviceTask_t*   pQueueRead;              /*!< Queue read pointer */
static DeviceTask_t*   pQueueWrite;             /*!< Queue write pointer */

/*! Access handler to the device functions */
DeviceHandler_t Device;

/**
 * @}
 */

/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/

uint32_t Device_Init(void)
{
    Device.BLE_Complete = ble_complete;
    Device.BLE_ScanComplete = ble_scancomplete;
    Device.Connected = ble_connected;
    Device.Disconnected = ble_disconnected;
    Device.GetNumberOfDevices = BLE_GetNumberOfScannedDevices;
    Device.GetStatus = get_status;
    Device.Schedule = schedule;

    pQueueRead = Queue;
    pQueueWrite = Queue;

    /** Initialise LEDs */
    nrf_drv_gpiote_out_config_t pin_cfg = GPIOTE_CONFIG_OUT_SIMPLE(true);
    RET_ON_ERROR (nrf_drv_gpiote_out_init(LED_SCAN, &pin_cfg) );

    return 0;
}

uint32_t Device_Process(bool* const pending)
{
    if( pQueueRead->type == OP_NONE ) {
        return 0;
    }

    uint32_t err_code = 0;

    switch( pQueueRead->type ) {
        case OP_BLE_SCAN:
            err_code = ble_scan();
            break;

        case OP_BLE_GET_SCANNED:
            err_code = ble_get_scanned();
            break;

        case OP_BLE_GET_DEVICE:
            err_code = ble_get_deviceinfo( *((uint8_t*) pQueueRead->args) );
            break;

        case OP_BLE_CONNECT_DEVICE:
            err_code = ble_connect_device( pQueueRead->args, pQueueRead->arglen );
            break;

        case OP_SERV_DISCOVER:
            err_code = serv_discover();
            break;

        case OP_SERV_GET:
            err_code = serv_get();
            break;

        case OP_CHAR_DISCOVER:
            err_code = char_discover( *((uint16_t*) pQueueRead->args) );
            break;

        case OP_CHAR_GET:
            err_code = char_get( *((uint16_t*) pQueueRead->args) );
            break;

        case OP_CHAR_READ:
            err_code = char_read( (uint16_t*) pQueueRead->args );
            break;

        case OP_CHAR_WRITE:
            err_code = char_write( (uint16_t*) pQueueRead->args, (pQueueRead->args + 4), pQueueRead->arglen - 4 );
            break;

        default:
            // No implementation needed.
            break;
    }

    free(pQueueRead->args);
    pQueueRead->args = 0;
    pQueueRead->arglen = 0;

    RET_ON_ERROR( err_code );

    /* advance pointer */
    pQueueRead->type = OP_NONE;
    pQueueRead++;
    if( pQueueRead == &Queue[QUEUE_LENGTH] ) {
        pQueueRead = Queue;
    }

    *pending = (*pending | ( pQueueRead->type != OP_NONE ));
    return err_code;
}

void Device_Ready(void)
{
    WRITE_OK();
}

/* Private functions ---------------------------------------------------------*/

static void ble_complete(void)
{
    BIT_CLEAR(&Status, DEV_STATUS_BLEBUSY);
    nrf_drv_gpiote_out_clear(LED_SCAN);
}

static void ble_connected(void)
{
    BIT_SET(&Status, DEV_STATUS_CONNECTED);
    ble_complete();
}

static uint32_t ble_connect_device(uint8_t* arg, uint16_t arglen)
{
    if( arglen == 1 ) {
        /* Scan index */

        if( BIT_ISSET(&Status, DEV_STATUS_BLEBUSY) ) {
            WRITE_ERROR(ERR_DEVICE_BUSY);
            return 0;
        }

        if( BIT_ISSET(&Status, DEV_STATUS_CONNECTED) ) {
            WRITE_ERROR(ERR_UNSUPPORTED);
            return 0;
        }

        BIT_SET(&Status, DEV_STATUS_BLEBUSY);
        RET_ON_ERROR( BLE_Connect_Index(*arg) );

        WRITE_RESPONSE("OK+%c", Status);
    }
    else if( arglen == BLE_GAP_ADDR_LEN ) {
        /* Received address */

        if( BIT_ISSET(&Status, DEV_STATUS_BLEBUSY) ) {
            WRITE_ERROR(ERR_DEVICE_BUSY);
            return 0;
        }

        if( BIT_ISSET(&Status, DEV_STATUS_CONNECTED) ) {
            WRITE_ERROR(ERR_UNSUPPORTED);
            return 0;
        }

        BIT_SET(&Status, DEV_STATUS_BLEBUSY);
        RET_ON_ERROR( BLE_Connect_Address(arg) );

        WRITE_RESPONSE("OK+%c", Status);
    }
    else {
        /* Invalid function call, most likely a firmware error. */
        return -1;
    }

    return 0;
}

static void ble_disconnected(void)
{
    BIT_CLEAR(&Status, DEV_STATUS_CONNECTED);
    ble_complete();
}

#include <inttypes.h>

static uint32_t ble_get_deviceinfo(uint8_t index)
{
    const ScanData_t* sdata = BLE_GetScannedDevice(index);

    if( !sdata ) {
        WRITE_ERROR(ERR_INVALID_PARAM);
        return 0;
    }

    /* Write length of response */
    uint16_t len = BLE_GAP_ADDR_LEN + sdata->adv_data.data.len + sdata->sr_data.data.len;
    WRITE_RESPONSE("OK+%c%c", *((uint8_t*)&len), *(((uint8_t*)&len) + 1));

    /* Write the data */
    RET_ON_ERROR( Serial_Write(sdata->peer_addr.pAddr, BLE_GAP_ADDR_LEN) );
    RET_ON_ERROR( Serial_Write(sdata->adv_data.data.p_data, sdata->adv_data.data.len) );
    RET_ON_ERROR( Serial_Write(sdata->sr_data.data.p_data, sdata->sr_data.data.len) );
    Serial_Flush();

    return 0;
}

static uint32_t ble_get_scanned(void)
{
    WRITE_RESPONSE("OK+%c", (char) BLE_GetNumberOfScannedDevices() );
    return 0;
}

#if 0
static uint32_t ble_parse_scanned(void)
{
    uint8_t ndevices = BLE_GetNumberOfScannedDevices();
    const ScanData_t* pDev;

    for( uint8_t i = 0; i < ndevices; ++i ) {
        printf("Parsing device %u\r\n", i);

        pDev = BLE_GetScannedDevice(i);
        if( !pDev ) {
            printf("  failed!\r\n");
            continue;
        }


        for( uint16_t idx = 0; idx < pDev->adv_data.data.len; ) {
            uint8_t length = pDev->adv_data.data.p_data[idx];

            if( length == 0 ) break;

            uint8_t type = pDev->adv_data.data.p_data[idx+1];
            uint16_t end = idx + length + 1;

            if( type == 0x08 || type == 0x09 ) {

                printf("Device Name: ");

                for(uint16_t c = idx + 2; c < end; ++c) {
                    printf("%c", (char) pDev->adv_data.data.p_data[c]);
                    nrf_delay_ms(2);
                }
                printf("\r\n");
            }

            /* next length octet */
            idx = end;
        }


        nrf_delay_ms(5);





        // for(uint16_t j = 0; j < pDev->adv_data.data.len; ++j ) {
        //     printf("  [%u]  0x%02X\r\n", j, pDev->adv_data.data.p_data[j]);
        //     nrf_delay_ms(5);
        // }


        // uint8_t seg = 1;
        // for( uint16_t idx = 0; idx < pDev->adv_data.data.len; ) {
        //     uint16_t length = pDev->adv_data.data.p_data[idx];
        //     uint16_t end = idx + length + 1;
        //     printf("  seg %u - length: %u\r\n    ", seg++, length);

        //     for( idx += 1; idx < end; ++idx ) {
        //         printf("%02X ", pDev->adv_data.data.p_data[idx]);
        //     }
        //     printf("\r\n");
        //     nrf_delay_ms(5);
        // }
    }

    printf("Scan compeleted!\r\n");

    return 0;
}
#endif

static uint32_t ble_scan(void)
{
    if( BIT_ISSET(&Status, DEV_STATUS_BLEBUSY) ) {
        WRITE_ERROR(ERR_DEVICE_BUSY);
        return 0;
    }

    // ble_busy = true;
    BIT_SET(&Status, DEV_STATUS_BLEBUSY);
    nrf_drv_gpiote_out_clear(LED_SCAN);

    RET_ON_ERROR( BLE_Scan() );

    WRITE_OK();

    return 0;
}

static void ble_scancomplete(uint16_t handle)
{
    BIT_CLEAR(&Status, DEV_STATUS_SCAN);
    res_handle = handle;
}

static uint32_t char_discover(uint16_t serv_handle)
{
    if( BIT_ISSET(&Status, DEV_STATUS_BLEBUSY) ) {
        WRITE_ERROR(ERR_DEVICE_BUSY);
        return 0;
    }

    if( ! BIT_ISSET(&Status, DEV_STATUS_CONNECTED) ) {
        WRITE_ERROR(ERR_DEVICE_NOT_CONNECTED);
        return 0;
    }

    BIT_SET(&Status, DEV_STATUS_BLEBUSY);

    uint32_t err_code = BLE_CHAR_Discover(serv_handle);

    if( err_code == DEV_ERROR_BLE_PARAM ) {
        WRITE_ERROR(ERR_INVALID_PARAM);
        ble_complete();
        return 0;
    }

    RET_ON_ERROR( err_code );

    WRITE_OK();

    return 0;
}

static uint32_t char_get(uint16_t serv_handle)
{
    /* Write length of the response */
    uint16_t n_chars = BLE_GetNumberOfCharacteristics(serv_handle);
    uint32_t datalen = 2 * n_chars;  // each characteristics identifier is 16 bit.
    WRITE_RESPONSE("OK+" ESC_32bit, UNPACK_32bit(&datalen) );

    /* Write the data */
    uint16_t* chars = (uint16_t*) malloc(n_chars);
    if( !chars )    return NRF_ERROR_NO_MEM;
    BLE_GetChars(serv_handle, chars, n_chars);

    RET_ON_ERROR( Serial_Write((uint8_t*) chars, datalen) );
    Serial_Flush();

    return 0;
}

static uint32_t char_read(uint16_t* params)
{
    /* Check if any flags are not as they are supposed to be */
    if( BIT_ISSET(&Status, DEV_STATUS_BLEBUSY) ) {
        WRITE_ERROR(ERR_DEVICE_BUSY);
        return 0;
    }

    if( ! BIT_ISSET(&Status, DEV_STATUS_CONNECTED) ) {
        WRITE_ERROR(ERR_DEVICE_NOT_CONNECTED);
        return 0;
    }

    BIT_SET(&Status, DEV_STATUS_BLEBUSY);

    /* Issue the read request */
    uint8_t* pData = 0;
    uint16_t size = 0;
    uint32_t err_code = BLE_CHAR_Read(params[0], params[1], &pData, &size);

    /* Check errors */
    if( err_code == DEV_ERROR_BLE_PARAM ) {
        free(pData);
        WRITE_ERROR(ERR_INVALID_PARAM);
        ble_complete();
        return 0;
    }
    if( err_code ) {
        free(pData);
        return err_code;
    }

    /* Wait until the request is complete */
    while( BIT_ISSET(&Status, DEV_STATUS_BLEBUSY) ) {
        nrf_pwr_mgmt_run();
    }

    /* Write length of the response */
    WRITE_RESPONSE("OK+" ESC_16bit, UNPACK_16bit(&size));

    /* Write the data */
    Serial_Write(pData, size);
    Serial_Flush();

    free(pData);

    return 0;
}

static uint32_t char_write(const uint16_t* params, const uint8_t* p_data, const uint16_t size)
{
    /* Check if any flags are not as they are supposed to be */
    if( BIT_ISSET(&Status, DEV_STATUS_BLEBUSY) ) {
        WRITE_ERROR(ERR_DEVICE_BUSY);
        return 0;
    }

    if( ! BIT_ISSET(&Status, DEV_STATUS_CONNECTED) ) {
        WRITE_ERROR(ERR_DEVICE_NOT_CONNECTED);
        return 0;
    }

    BIT_SET(&Status, DEV_STATUS_BLEBUSY);

    RET_ON_ERROR( BLE_CHAR_Write(params[0], params[1], p_data, size) );

    /* Wait until the request is complete */
    while( BIT_ISSET(&Status, DEV_STATUS_BLEBUSY) )    nrf_pwr_mgmt_run();

    WRITE_OK();

    return 0;
}

static uint8_t get_status(void)
{
    return Status;
}

static uint8_t schedule(DeviceOperationType_t op, void* args, uint16_t arglen)
{
    if( pQueueWrite->type != OP_NONE ) {
        return 1;
    }

    pQueueWrite->type = op;
    pQueueWrite->args = args;
    pQueueWrite->arglen = arglen;

    pQueueWrite++;
    if( pQueueWrite == &Queue[QUEUE_LENGTH] ) {
        pQueueWrite = Queue;
    }

    return 0;
}

static uint32_t serv_discover(void)
{
    if( BIT_ISSET(&Status, DEV_STATUS_BLEBUSY) ) {
        WRITE_ERROR(ERR_DEVICE_BUSY);
        return 0;
    }

    if( ! BIT_ISSET(&Status, DEV_STATUS_CONNECTED) ) {
        WRITE_ERROR(ERR_DEVICE_NOT_CONNECTED);
        return 0;
    }

    BIT_SET(&Status, DEV_STATUS_BLEBUSY);
    
    uint16_t start_handle = 0x0001;
    res_handle = -1;
    
    while(1) {

        BIT_SET(&Status, DEV_STATUS_SCAN);
        RET_ON_ERROR( BLE_SERV_Discover(start_handle) );

        /* Wait until the request is complete */
        while( BIT_ISSET(&Status, DEV_STATUS_SCAN) )    nrf_pwr_mgmt_run();

        if( res_handle == ((uint16_t) -1) || res_handle < start_handle ) {
            break;
        }

        start_handle = res_handle + 1;
    }

    BIT_CLEAR(&Status, DEV_STATUS_BLEBUSY);
    

    WRITE_OK();

    return 0;
}

static uint32_t serv_get(void)
{
    /* Write length of the response */
    uint16_t n_services = BLE_GetNumberOfServices();
    uint32_t datalen = 2 * n_services;  // each service identifier is 16 bit.
    WRITE_RESPONSE("OK+" ESC_32bit, UNPACK_32bit(&datalen) );

    /* Write the data */
    uint16_t* services = (uint16_t*) malloc(n_services);
    if( !services ) return NRF_ERROR_NO_MEM;
    BLE_GetServices(services, n_services);

    RET_ON_ERROR( Serial_Write((uint8_t*) services, datalen) );
    Serial_Flush();

    return 0;
}
