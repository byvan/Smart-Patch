/**
 * @file serial.c
 * @brief      BLE Bridge serial interface implementation.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       January 2021
 */

/* Includes ------------------------------------------------------------------*/
#include <app_uart.h>
#include <nrf_uarte.h>

#include "device.h"
#include "macros.h"
#include "serial.h"
#include "serial_commands.h"

/**
 * @addtogroup   BLEBridge_Serial
 * @{
 */

/**
 * @defgroup   BLEBridge_Serial_ic Serial Module internal components
 * @{
 */

/* Private defines -----------------------------------------------------------*/

/*! Size of the internal command buffer */
#define SERIAL_CMD_BUFFER_SIZE              32

/*! Size of the internal serial RX buffer [bytes] */
#define SERIAL_RX_BUFFER_SIZE               128
/*! Size of the internal serial TX buffer [bytes] */
#define SERIAL_TX_BUFFER_SIZE               128

/*! UART RX Pin */
#define RX_PIN_NUMBER           8
/*! UART TX Pin */
#define TX_PIN_NUMBER           6

/* Private macros ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/**
 * @brief      State enumerator of the serial module.
 */
typedef enum {
    STATE_WAIT_COMMAND,         /*!< The module is waiting for a command */
    STATE_PARSE_COMMAND,        /*!< Parse the received command */
} SerialState_t;

/* Private function prototypes -----------------------------------------------*/

/**
 * @brief      UART event handler.
 *
 * @param      p_event  Pointer to the event register.
 */
static void cb_uart(app_uart_evt_t* p_event);

/**
 * @brief      Process a serial event in the #STATE_PARSE_COMMAND state.
 *
 * @param[out] pending  Flag indicating if further action is pending.
 *
 * @return     Any error that comes up.
 */
static uint32_t process_parse_command(bool* const pending);

/**
 * @brief      Processes a serial event in the #STATE_WAIT_COMMAND state.
 *
 * @param[out] pending  Flag indicating if further action is pending.
 *
 * @return     Any error that comes up.
 */
static uint32_t process_wait_command(bool* const pending);

/* Private variables ---------------------------------------------------------*/

static SerialState_t state;
static uint8_t cmd_buffer[SERIAL_CMD_BUFFER_SIZE] = {0};

static uint32_t bytes_available = 0;
static uint32_t bytes_processed = 0;

/**
 * @}
 */

/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/

void Serial_Flush(void)
{
    printf("\r\n");
}

uint32_t Serial_Init(void)
{
    state = STATE_WAIT_COMMAND;

    /* Initialise the UART module. */
    const app_uart_comm_params_t comm_params = {
        RX_PIN_NUMBER,
        TX_PIN_NUMBER,
        NRF_UARTE_PSEL_DISCONNECTED,
        NRF_UARTE_PSEL_DISCONNECTED,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        NRF_UARTE_BAUDRATE_115200
    };

    uint32_t err_code = 0;
    APP_UART_FIFO_INIT(
        &comm_params,
        SERIAL_RX_BUFFER_SIZE,
        SERIAL_TX_BUFFER_SIZE,
        cb_uart,
        APP_IRQ_PRIORITY_LOWEST,
        err_code);

    return err_code;
}

uint32_t counter = 0;

uint32_t Serial_Process(bool* const pending)
{
    

    bool _pending = false;

    // printf("\r\nCounter: %lu / %u", counter++, (uint8_t) state);

    uint32_t err_code = 0;

    switch( state ) {
        case STATE_WAIT_COMMAND:
            err_code = process_wait_command(&_pending);
            break;

        case STATE_PARSE_COMMAND:
            err_code = process_parse_command(&_pending);
            break;
    }

    RET_ON_ERROR( err_code );

    *pending = *pending || _pending;

    return 0;
}

uint32_t Serial_Write(const uint8_t* pData, const uint32_t size)
{
    for( uint32_t i = 0; i < size; ++i ) {
        RET_ON_ERROR( app_uart_put(pData[i]) );
    }

    return 0;
}

/* Private functions ---------------------------------------------------------*/
#include <nrf_drv_gpiote.h>

static void cb_uart(app_uart_evt_t* p_event)
{
    switch( p_event->evt_type ) {
        /* A byte was received */
        case APP_UART_DATA_READY:
            bytes_available++;
            break;

        case APP_UART_FIFO_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        case APP_UART_COMMUNICATION_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_TX_EMPTY:
            break;

        case APP_UART_DATA:
            // nrf_drv_gpiote_out_toggle(16);
            break;
    }
}

static uint32_t process_parse_command(bool* const pending)
{
    uint8_t c_err = 0;
    bool confirm = true;

    switch( cmd_buffer[0] ) {
        case COMMAND_FAM_BLE:       // 0x0A
            RET_ON_ERROR( Command_Parse_BLE(&cmd_buffer[1], bytes_processed - 1, &c_err, &confirm) );
            break;

        case COMMAND_FAM_SERV:      // 0x0B
            RET_ON_ERROR( Command_Parse_SERV(&cmd_buffer[1], bytes_processed - 1, &c_err, &confirm) );
            break;

        case COMMAND_FAM_CHAR:      // 0x0C
            RET_ON_ERROR( Command_Parse_CHAR(&cmd_buffer[1], bytes_processed - 1, &c_err, &confirm) );
            break;

        case COMMAND_FAM_DEVICE:    // 0xF0
            RET_ON_ERROR( Command_Parse_DEVICE(&cmd_buffer[1], bytes_processed - 1, &c_err, &confirm) );
            break;
        
        default:
            /* Invalid family byte */
            c_err = ERR_COMMAND_USER;
            break;
    }

    if( c_err ) {
        WRITE_ERROR(c_err);
    }
    else if( confirm ) {
        WRITE_OK();
    }

    nrf_drv_gpiote_out_toggle(15);


    // for(uint16_t i = 0; i < bytes_processed; ++i) {
    //     printf("\r\n0x%02X", cmd_buffer[i]);
    // }

    // printf("\r\nProcessing command\r\n");
    // printf("\r\n");



    state = STATE_WAIT_COMMAND;

    bytes_processed = 0;

    *pending = false;
    return 0;
}

static uint32_t process_wait_command(bool* const pending)
{
    if( !bytes_available )  return 0;

    /**
     * In the STATE_WAIT_COMMAND state, we wait for the command to be terminated
     * by the termination character.
     */
    uint8_t data = 0;
    while( bytes_available ) {
        RET_ON_ERROR( app_uart_get(&data) );
        RET_ON_ERROR( app_uart_put((char)data) );
        --bytes_available;

        cmd_buffer[bytes_processed++] = data;

        if( data != SERIAL_CMD_TERMINATOR ) {
            RET_ON_ERROR( (bytes_processed == SERIAL_CMD_BUFFER_SIZE) ? -1 : 0 );
        }
        else {
            nrf_drv_gpiote_out_toggle(16);
            bytes_available = 0;
            while(!app_uart_get(&data));
            // RET_ON_ERROR( app_uart_flush() );

            printf("\r\n");
            state = STATE_PARSE_COMMAND;
            *pending = true;
        }
    }

    return 0;
}
