/**
 * @file serial_commands.c
 * @brief      BLE Bridge serial interface implementation.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       January 2021
 */

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ble_gap.h>
#include "device.h"
#include "macros.h"
#include "serial.h"
#include "serial_commands.h"

/**
 * @addtogroup   Serial_Commands
 * @{
 */

/**
 * @defgroup   Serial_Commands_ic Serial Commands internal components
 * @{
 */

/* Private defines -----------------------------------------------------------*/

/* Private macros ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/**
 * @brief      { function_description }
 *
 * @param[in]  pCommand  The command
 * @param[in]  size      The size
 * @param      err       The error
 * @param      confirm   The confirm
 */
static void command_ble_connect(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm);

/**
 * @brief      { function_description }
 *
 * @param[in]  pCommand  The command
 * @param[in]  size      The size
 * @param      err       The error
 * @param      confirm   The confirm
 */
static void command_ble_get_device(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm);

/**
 * @brief      { function_description }
 *
 * @param[in]  pCommand  The command
 * @param[in]  size      The size
 * @param      err       The error
 * @param      confirm   The confirm
 */
static void command_ble_get_scanned(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm);

/**
 * @brief      { function_description }
 *
 * @param[in]  pCommand  The command
 * @param[in]  size      The size
 * @param      err       The error
 * @param      confirm   The confirm
 */
static void command_ble_scan(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm);

/**
 * @brief      { function_description }
 *
 * @param[in]  pCommand  The command
 * @param[in]  size      The size
 * @param      err       The error
 * @param      confirm   The confirm
 */
static void command_char_discover(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm);

/**
 * @brief      { function_description }
 *
 * @param[in]  pCommand  The command
 * @param[in]  size      The size
 * @param      err       The error
 * @param      confirm   The confirm
 */
static void command_char_get(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm);

/**
 * @brief      { function_description }
 *
 * @param[in]  pCommand  The command
 * @param[in]  size      The size
 * @param      err       The error
 * @param      confirm   The confirm
 */
static void command_char_read(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm);

/**
 * @brief      { function_description }
 *
 * @param[in]  pCommand  The command
 * @param[in]  size      The size
 * @param      err       The error
 * @param      confirm   The confirm
 */
static void command_char_write(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm);

/**
 * @brief      { function_description }
 *
 * @param[in]  pCommand  The command
 * @param[in]  size      The size
 * @param      err       The error
 * @param      confirm   The confirm
 */
static void command_device_status(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm);

/**
 * @brief      { function_description }
 *
 * @param[in]  pCommand  The command
 * @param[in]  size      The size
 * @param      err       The error
 * @param      confirm   The confirm
 */
static void command_serv_discover(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm);

/**
 * @brief      { function_description }
 *
 * @param[in]  pCommand  The command
 * @param[in]  size      The size
 * @param      err       The error
 * @param      confirm   The confirm
 */
static void command_serv_get(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm);

/* Private variables ---------------------------------------------------------*/

/**
 * @}
 */

/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/

uint32_t Command_Parse_BLE(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm)
{
    if( size < 2 ) {
        *err = ERR_COMMAND_USER;
        return 0;
    }

    switch( *pCommand ) {
        case COMMAND_IDX_SCAN:          // 0x01
            command_ble_scan((pCommand + 1), size - 1, err, confirm);
            break;

        case COMMAND_IDX_GET_SCANNED:   // 0x02
            command_ble_get_scanned((pCommand + 1), size - 1, err, confirm);
            break;

        case COMMAND_IDX_GET_DEVICE:    // 0x03
            command_ble_get_device((pCommand + 1), size - 1, err, confirm);
            break;

        case COMMAND_IDX_CONNECT:       // 0x10
            command_ble_connect((pCommand + 1), size - 1, err, confirm);
            break;

        default:
            /* Invalid index byte */
            *err = ERR_COMMAND_USER;
            break;
    }

    return 0;
}

uint32_t Command_Parse_CHAR(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm)
{
    if( size < 2 ) {
        *err = ERR_COMMAND_USER;
        return 0;
    }

    switch( *pCommand ) {
        case COMMAND_IDX_DISCOVER:  // 0x01
            command_char_discover((pCommand + 1), size - 1, err, confirm);
            break;

        case COMMAND_IDX_GET_DISC:  // 0x02
            command_char_get((pCommand + 1), size - 1, err, confirm);
            break;

        case COMMAND_IDX_READ:      // 0x03
            command_char_read((pCommand + 1), size - 1, err, confirm);
            break;

        case COMMAND_IDX_WRITE:     // 0x04
            command_char_write((pCommand + 1), size - 1, err, confirm);
            break;

        default:
            /* Invalid index byte */
            *err = ERR_COMMAND_USER;
            break;
    }

    return 0;
}

uint32_t Command_Parse_DEVICE(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm)
{
    if( size < 2 ) {
        *err = ERR_COMMAND_USER;
        return 0;
    }

    switch( *pCommand ) {
        case COMMAND_IDX_STATUS:        // 0x01
            command_device_status((pCommand + 1), size - 1, err, confirm);
            break;
        
        default:
            /* Invalid index byte */
            *err = ERR_COMMAND_USER;
            break;
    }

    return 0;
}

uint32_t Command_Parse_SERV(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm)
{
    if( size < 2 ) {
        *err = ERR_COMMAND_USER;
        return 0;
    }

    switch( *pCommand ) {
        case COMMAND_IDX_DISCOVER:      // 0x01
            command_serv_discover((pCommand + 1), size - 1, err, confirm);
            break;

        case COMMAND_IDX_GET_DISC:      // 0x02
            command_serv_get((pCommand + 1), size - 1, err, confirm);
            break;

        default:
            /* Invalid index byte */
            *err = ERR_COMMAND_USER;
            break;
    }

    return 0;
}

/* Private functions ---------------------------------------------------------*/

static void command_ble_connect(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm)
{
    if( size == 2 ) {
        /* received an index from the scan list */

        if( !( Device.GetNumberOfDevices() > *pCommand ) ) {
            *err = ERR_INVALID_PARAM;
            return;
        }

        uint8_t* pargs = (uint8_t*) malloc(1);
        *pargs = *pCommand;

        *confirm = false;
        *err = Device.Schedule(OP_BLE_CONNECT_DEVICE, pargs, 1);
    }
    else if( size == (BLE_GAP_ADDR_LEN + 1) ) {
        /* received an address to connect to */

        uint8_t* pargs = (uint8_t*) malloc(BLE_GAP_ADDR_LEN);
        memcpy(pargs, pCommand, BLE_GAP_ADDR_LEN);

        *confirm = false;
        *err = Device.Schedule(OP_BLE_CONNECT_DEVICE, pargs, BLE_GAP_ADDR_LEN);
    }
    else {
        /* bad command */
        *err = ERR_COMMAND_USER;
    } 
}

static void command_ble_get_device(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm)
{
    if( size != 2 ) {
        /* bad command */
        *err = ERR_COMMAND_USER;
        return;
    }

    if( !( Device.GetNumberOfDevices() > *pCommand ) ) {
        *err = ERR_INVALID_PARAM;
        return;
    }

    uint8_t* pargs = (uint8_t*) malloc(1);
    *pargs = *pCommand;

    *confirm = false;
    *err = Device.Schedule(OP_BLE_GET_DEVICE, pargs, 1);
}

static void command_ble_get_scanned(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm)
{
    if( size != 1 ) {
        /* bad command */
        *err = ERR_COMMAND_USER;
        return;
    }

    *confirm = false;
    *err = Device.Schedule(OP_BLE_GET_SCANNED, NULL, 0);

    (void)pCommand;
}

static void command_ble_scan(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm)
{
    if( size != 1 ) {
        /* bad command */
        *err = ERR_COMMAND_USER;
        return;
    }
    
    *confirm = false;
    *err = Device.Schedule(OP_BLE_SCAN, NULL, 0);
    
    (void) pCommand;
}

static void command_char_discover(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm)
{
    if( size != 3 ) {
        /* bad command */
        *err = ERR_COMMAND_USER;
        return;
    }

    uint16_t* pServ = (uint16_t*) malloc(2);
    memcpy((uint8_t*) pServ, pCommand, 2);

    *confirm = false;
    *err = Device.Schedule(OP_CHAR_DISCOVER, pServ, 2);
}

static void command_char_get(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm)
{
    if( size != 3 ) {
        /* bad command */
        *err = ERR_COMMAND_USER;
        return;
    }

    uint16_t* pServ = (uint16_t*) malloc(2);
    memcpy((uint8_t*) pServ, pCommand, 2);

    *confirm = false;
    *err = Device.Schedule(OP_CHAR_GET, pServ, 2);
}

static void command_char_read(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm)
{
    if( size != 5 ) {
        /* bad command */
        *err = ERR_COMMAND_USER;
        return;
    }

    uint16_t* pChar = (uint16_t*) malloc(4);
    memcpy((uint8_t*) pChar, pCommand, 4);

    *confirm = false;
    *err = Device.Schedule(OP_CHAR_READ, pChar, 4);
}

static void command_char_write(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm)
{
    if( size < 5 ) {
        /* bad command */
        *err = ERR_COMMAND_USER;
        return;
    }

    uint8_t* p_args = (uint8_t*) malloc( size - 1 );
    memcpy(p_args, pCommand, size - 1);

    *confirm = false;
    *err = Device.Schedule(OP_CHAR_WRITE, p_args, size - 1);
}

static void command_device_status(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm)
{
    if( size != 1 ) {
        /* bad command */
        *err = ERR_COMMAND_USER;
        return;
    }

    *confirm = false;
    WRITE_RESPONSE("OK+%c", Device.GetStatus());

    (void)pCommand;
    (void)err;
}

static void command_serv_discover(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm)
{
    if( size != 1 ) {
        /* bad command */
        *err = ERR_COMMAND_USER;
        return;
    }

    *confirm = false;
    *err = Device.Schedule(OP_SERV_DISCOVER, NULL, 0);

    (void) pCommand;
}

static void command_serv_get(const uint8_t* pCommand, const uint16_t size, uint8_t* const err, bool* const confirm)
{
    if( size != 1 ) {
        /* bad command */
        *err = ERR_COMMAND_USER;
        return;
    }

    *confirm = false;
    *err = Device.Schedule(OP_SERV_GET, NULL, 0);

    (void) pCommand;
}
