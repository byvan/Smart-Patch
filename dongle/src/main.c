/**
 * @file main.c
 * @brief      BLE Bridge dongle firmware.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       January 2021
 */

/* Includes ------------------------------------------------------------------*/
#include <app_error.h>
#include <app_uart.h>
#include <nrf_delay.h>
#include <nrf_drv_gpiote.h>
#include <nrf_pwr_mgmt.h>
#include <nrf_uart.h>
#include <nrf_uarte.h>

#include "bridge_ble.h"
#include "device.h"
#include "macros.h"
#include "serial.h"

/**
 * @defgroup   BLEBridge Smart Patch Dongle Firmware
 * @{
 */

/**
 * @defgroup   main_ic Main Application internal components
 * @{
 */

/* Private defines -----------------------------------------------------------*/

#define LED_2   14
#define LED_3   15
#define LED_4   16

// #define UART_TX_BUF_SIZE 256                         /**< UART TX buffer size. */
// #define UART_RX_BUF_SIZE 256                         /**< UART RX buffer size. */

/* Private macros ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/**
 * @brief      Initialises the device peripherals.
 *
 * @return     Any error that comes up.
 */
static uint32_t peripherals_init(void);

/* Private variables ---------------------------------------------------------*/

/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/

int main(void)
{
    /* Initialise system components */
    APP_ERROR_CHECK( peripherals_init() );

    /* Initialise the BLE stack */
    APP_ERROR_CHECK( BLE_Init() );

    /* Initialise core device functionality */
    APP_ERROR_CHECK( Device_Init() );

    /* Signal the application the ready state */
    Device_Ready();

    // nrf_drv_gpiote_out_clear(LED_1);


    bool action_pending;

    while(1) {

        action_pending = false;
        APP_ERROR_CHECK( Serial_Process(&action_pending) );
        APP_ERROR_CHECK( Device_Process(&action_pending) );

        if( ! action_pending ) {
            nrf_pwr_mgmt_run();
        }


        // 
    }

    // while (true)
    // {
    //     nrf_drv_gpiote_out_toggle(LED_2);

    //     uint8_t cr;
    //     while (app_uart_get(&cr) != NRF_SUCCESS);
    //     while (app_uart_put(cr) != NRF_SUCCESS);

    //     if (cr == 'q' || cr == 'Q')
    //     {
    //         printf(" \r\nExit!\r\n");

    //         while (true)
    //         {
    //             // Do nothing.
    //         }
    //     }
    // }
}

/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/

static uint32_t peripherals_init(void)
{
    /** Initialise GPIO peripheral */
    RET_ON_ERROR( nrf_drv_gpiote_init() );

    /** Initialise the serial interface */
    RET_ON_ERROR( Serial_Init() );





    /** Initialise LEDs */
    nrf_drv_gpiote_out_config_t pin_cfg = GPIOTE_CONFIG_OUT_SIMPLE(true);

    RET_ON_ERROR (nrf_drv_gpiote_out_init(LED_2, &pin_cfg) );
    RET_ON_ERROR (nrf_drv_gpiote_out_init(LED_3, &pin_cfg) );
    RET_ON_ERROR (nrf_drv_gpiote_out_init(LED_4, &pin_cfg) );

    return 0;
}
