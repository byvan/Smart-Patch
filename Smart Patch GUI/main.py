#! python3

### Imports
from base import SmartPatchBase
from MainWindow import *
from PyQt5.QtWidgets import QApplication

cfgBATTERY_INTERVAL = 10000

### Main
if __name__ == '__main__':

    app = SmartPatchBase(batteryInterval=cfgBATTERY_INTERVAL)
    app.run()
