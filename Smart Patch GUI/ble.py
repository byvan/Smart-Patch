#! python3

import asyncio
from bleak import *
from bleak.uuids import uuid16_dict
from bleak.backends.corebluetooth import *
from bleak.backends.corebluetooth.client import *
from PyQt5.QtCore import *

uuid16_dict = {v: k for k, v in uuid16_dict.items()}

BATTERY_LEVEL_UUID = "0000{0:x}-0000-1000-8000-00805f9b34fb".format(uuid16_dict.get("Battery Level"))
DEVICE_NAME_UUID = "0000{0:x}-0000-1000-8000-00805f9b34fb".format(uuid16_dict.get("Device Name"))
MANUFACTURER_NAME_UUID = "0000{0:x}-0000-1000-8000-00805f9b34fb".format(uuid16_dict.get("Manufacturer Name String"))
MODEL_NBR_UUID = "0000{0:x}-0000-1000-8000-00805f9b34fb".format(uuid16_dict.get("Model Number String"))

ACQ_EN_CHAR = "4201"

device_characteristics = {
    "DeviceSettings"    : "4101",
    "DFURegister"       : "4102",
    "DFUACKRegister"    : "4103"
}

class BLEManager(QThread):

    ##
    ## Signal to notify application about the status of the DFU initialisation
    ## sequence.
    ##
    sig_SensorDFU_ACK = pyqtSignal(bytearray)

    ##
    ##
    ##
    sig_SensorDFU_Read_complete = pyqtSignal(bytearray)

    ##
    ## Initialises the sensor DFU on the Smart Patch.
    ##
    ## Once completed, the function will emit the value of the device settings
    ## register through the @sig_SensorDFU_ACK signal.
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def SensorDFU_Initialise(self) -> None:
        print("[Sensor DFU] Send initialisation command.")

        # read and modify the device settings register
        settings = self.loop.run_until_complete(self._read_Char(self.characteristics['DeviceSettings']))
        settings[0] |= 8

        # write back the modified value
        if not self.loop.run_until_complete(self._write_Char(self.characteristics['DeviceSettings'], settings)):
            print("Failed to write data to DFU register!")
            self.sig_lost.emit()
            return

        # read the DFU ACK register (should be 0 on success)
        # self.SensorDFU_Read()
        ack = self.loop.run_until_complete(self._get_sensorDFU_ack())
        self.sig_SensorDFU_ACK.emit(ack)
        # @TODO comnbine with DFU_Read?

        # val = self.loop.run_until_complete(self._read_Char(self.characteristics['DeviceSettings']))
        # self.sig_DFUinit_cb.emit(val[0])
    
    ##
    ## Reads the sensor DFU ACK register.
    ##
    ## Once completed, the function will emit the value of the register through
    ## the @sig_SensorDFU_Read_complete signal.
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def SensorDFU_Read(self) -> None:
        ack = self.loop.run_until_complete(self._get_sensorDFU_ack())
        print("ACKCMPL: {}".format(ack))
        if ack != -1:
            self.sig_SensorDFU_Read_complete.emit(ack)

    ##
    ## Writes data to the sensor DFU register.
    ##
    ## :param      data:  The data to be written
    ## :type       data:  bytearray
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def SensorDFU_Write(self, data: bytearray) -> None:
        print("[Sensor DFU] Writing sequence #{}".format(data[0]))
        # @todo check for errors
        if not self.loop.run_until_complete(self._write_Char(self.characteristics['DFURegister'], data)):
            print("Failed to write data to DFU register!")
            self.sig_lost.emit()

        ack = self.loop.run_until_complete(self._get_sensorDFU_ack())

        if ack == -1:
            ack = self.loop.run_until_complete(self._get_sensorDFU_ack())

        self.sig_SensorDFU_ACK.emit(ack)







    async def _get_sensorDFU_ack(self):
        data = await self._read_Char(self.characteristics['DFUACKRegister'])

        # TODO: make better, but here the connection timed out
        if len(data) == 1 and data[0] == -1:
            return -1

        return data




    sig_lost = pyqtSignal()
    sig_connect_cb = pyqtSignal(int)
    sig_scan_cb = pyqtSignal([list])
    # sig_DFUinit_cb = pyqtSignal(int)
    sig_DFUData_cb = pyqtSignal(int)

    setDeviceName = None


    characteristics = {'Enable': None}

    connectedDevice = None
    batteryServicePresent = False
    deviceNamePresent = False
    modelNbrPresent = False

    # sig_ScanComplete = pyqtSignal([list])
    # sig_ConnSuccess = pyqtSignal()
    # sig_ConnFailed = pyqtSignal()
    # sig_ConnLost = pyqtSignal()
    sig_Enabled = pyqtSignal(int)
    sig_BatteryLevel = pyqtSignal(int)

    setter_DeviceName = None

    def Connect(self, id):
        print("Connecting to {0} ({1})".format(self.devices[id].name, self.devices[id].address))
        self.loop.run_until_complete(self._connect(id))

        if self.isConnected():
            if self.setDeviceName:
                self.setDeviceName(self.devices[id].name)

            self.sig_connect_cb.emit(1)
        else:
            self.sig_connect_cb.emit(0)
            
        #     print("Connected!")
        #     if self.batteryServicePresent:
        #         self.batteryServiceActivate(True)
        #     else:
        #         self.batteryServiceActivate(False)

        #     self.sig_ConnSuccess.emit()
        # else:
        #     print("Failed!")
        #     self.sig_ConnFailed.emit()

    def Disconnect(self):
        self.loop.run_until_complete(self.connectedDevice.disconnect())
        # self.batteryServicePresent = False
        # self.deviceNamePresent = False
        # self.modelNbrPresent = False
        # self.batteryServiceActivate(False)
        # self.setter_DeviceName("Unknown")
        print("Disconnected!")

    def InitDFU(self):
        val = self.loop.run_until_complete(self._read_Char(self.characteristics['DeviceSettings']))
        val[0] |= 8
        self.loop.run_until_complete(self._write_Char(self.characteristics['DeviceSettings'], val))
        val = self.loop.run_until_complete(self._read_Char(self.characteristics['DeviceSettings']))
        self.sig_DFUinit_cb.emit(val[0])

    def Scan(self):
        self.scannedList = []
        print("Scanning for devices...")
        scanner = Scanner(self.loop, self.scannedList, self._cb_scan)
        self.threadPool.start(scanner)

    def SendDFUData(self, data):
        print("Sending sequence {}".format(data[0]))
        self.loop.run_until_complete(self._write_Char(self.characteristics['DFURegister'], data))
        return self.ReadDFUData()

    def ReadDFUData(self):
        val = self.loop.run_until_complete(self._read_Char(self.characteristics['DFUACKRegister']))
        return (val[0] | (val[1] << 8))

    def LostSignal(self, callback):
        self.sig_lost.connect(callback)


    """docstring for BLEManager"""
    def __init__(self, batteryServiceActivate, threadPool):
        super(BLEManager, self).__init__()
        self.batteryServiceActivate = batteryServiceActivate
        self.threadPool = threadPool
        self.loop = asyncio.get_event_loop()

        # self.sig_DFUData_cb.emit(ack)

    def _cb_scan(self):
        self.devices = []
        for d in self.scannedList:
            # if d.name == "Smart Patch":
            # self.devices.append({"addr": d.address, "name": d.name})
            self.devices.append(d)

        self.sig_scan_cb.emit(self.devices)






    def acq_enable(self):
        val = self.loop.run_until_complete(self._read_Char(self.characteristics['Enable']))
        if val[0]:
            val[0] = 0
        else:
            val[0] = 1

        self.loop.run_until_complete(self._write_Char(self.characteristics["Enable"], val))

        val = self.loop.run_until_complete(self._read_Char(self.characteristics['Enable']))
        self.sig_Enabled.emit(val[0])
           
    async def _connect(self, id):
        self.connectedDevice = BleakClientCoreBluetooth(self.devices[id])

        try:
            await self.connectedDevice.connect()
        except Exception: #asyncio.exceptions.TimeoutError:
            return

        if not await self.connectedDevice.is_connected():
            return

        await self.connectedDevice.get_services()
        for srvc in self.connectedDevice.services:
            # print("----------------")
            # print(srvc)
            for char in srvc.characteristics:
                # print(char)

                self.mapCharacteristics(char)

                # if char.uuid == BATTERY_LEVEL_UUID:
                #     self.batteryServicePresent = True
                # elif char.uuid == DEVICE_NAME_UUID:
                #     self.deviceNamePresent = True
                # elif char.uuid == MODEL_NBR_UUID:
                #     self.modelNbrPresent = True

        # if self.deviceNamePresent:
        #     res = await self._read_Char(DEVICE_NAME_UUID)
        #     self.setter_DeviceName(res.decode())
        # elif self.modelNbrPresent:
        #     res = await self._read_Char(MODEL_NBR_UUID)
        #     self.setter_DeviceName(res.decode())     
        
        # await self.connectedDevice.start_notify(self.characteristics['DFUACKRegister'], self.dfu_acked)


    

    def isConnected(self):
        if self.connectedDevice:
            return self.loop.run_until_complete(self.connectedDevice.is_connected())
        else:
            return False;

    def isCharacteristic(self, uuid, target):
        mask = uuid[:4] + "0000" + uuid[8:]
        char_uuid = utils._convert_uuid_to_int(uuid) - utils._convert_uuid_to_int(mask)

        return char_uuid == utils._convert_uuid_to_int("0000{}-0000-0000-0000-000000000000".format(target))

    def mapCharacteristics(self, char):
        for k in device_characteristics:
            if self.isCharacteristic(char.uuid, device_characteristics[k]):
                self.characteristics[k] = char
                print("Characteristic found: {} - {}".format(k, device_characteristics[k]))

    def readBattery(self):
        res = self.loop.run_until_complete(self._read_Char(BATTERY_LEVEL_UUID))
        self.sig_BatteryLevel.emit(int(res[0]))

    def run(self):
        print("Connectivity Manager started!")
        self.Scan()

    

    async def _connect_Device(self, addr):
        print("DM")
        # device = BleakClientCoreBluetooth(addr)

        # device = await BleakScanner.find_device_by_address(addr) 
        # async with BleakClient(device) as client:
        # # with BleakClient(device) as client:
        #     svcs = await client.get_services()
        #     self.services = []
        #     for s in svcs:
        #         self.services.append(s)
        #         print("------------------------")
        #         print(s)

        #         for c in s.characteristics:
        #             if c.uuid == UUID_BATTERY_LEVEL:
        #                 self.char_Battery = c
        #                 self.batteryServicePresent = True
            
        #     self.device = client

    async def _write_Char(self, char, data):
        try:
            await self.connectedDevice.write_gatt_char(char, data)
            return True
        except asyncio.TimeoutError:
            self.batteryServicePresent = False
            self.deviceNamePresent = False
            self.modelNbrPresent = False
            self.batteryServiceActivate(False)
            # self.setter_DeviceName("Unknown")
            print("TX Connection timed out!")
            # self.sig_lost.emit()
            return False

    async def _read_Char(self, char):
        try:
            return await self.connectedDevice.read_gatt_char(char)
        except asyncio.TimeoutError:
            self.batteryServicePresent = False
            self.deviceNamePresent = False
            self.modelNbrPresent = False
            self.batteryServiceActivate(False)
            # self.setter_DeviceName("Unknown")
            print("RX Connection timed out!")
            self.sig_lost.emit()
            return [-1]



class ConnectivityManager(QThread):

    connected = False
    threadPool = QThreadPool()

    scanComplete = pyqtSignal([list])
    connComplete = pyqtSignal()
    connFailed = pyqtSignal()
    batUpdate = pyqtSignal(int)

    batteryServicePresent = False
    batteryTimer = QTimer()

    """docstring for ConnectivityManager"""
    def __init__(self, ScanSlot, ConnSlot, DiscSlot):
        super(ConnectivityManager, self).__init__()
        ScanSlot.connect(self._scan)
        ConnSlot.connect(self._conn)
        DiscSlot.connect(self._disc)

        self.batteryTimer.timeout.connect(self._cb_batTimer)

    def run(self):
        print("Started Connectivity Manager")

    def _cb_batTimer(self):
        print("Timer elapsed!")

    def _conn(self, id):
        self.services = []

        loop = asyncio.get_event_loop()
        self.connectedDevice = self.devices[id]
        # connector = Connector(loop, (self.connectedDevice["addr"]), self.services, self._conn_s, self._conn_f)
        print("Connecting to {0} ({1})".format(self.connectedDevice["name"], self.connectedDevice["addr"]))
        # self.threadPool.start(connector)

    def _conn_f(self):
        connected = False
        print("Failed!")
        self.connFailed.emit()

    def _conn_s(self, client):
        self.client = client
        self.connected = True
        print("Connected!")

        for s in self.services:
            print("------------------------")
            print(s)

            for c in s.characteristics:
                if c.uuid == UUID_BATTERY_LEVEL:
                    self.batteryServicePresent = True

        if self.batteryServicePresent:
            self.batteryTimer.start(10000)

        self.connComplete.emit()

    def _disc(self):
        self.batteryServicePresent = False
        self.batteryTimer.stop()
        self.connected = False
        self.connectedDevice = {}
        self.services = []
        print("Disconnecting!")

    def _scan(self):
        self.scannedList = []
        loop = asyncio.get_event_loop()
        scanner = Scanner(loop, self.scannedList, self._scan_cb)
        print("Scanning for devices...")
        self.threadPool.start(scanner)

    def _scan_cb(self):
        self.devices = []
        for d in self.scannedList:
            # if d.name == "Smart Patch":
            self.devices.append({"addr": d.address, "name": d.name})

        self.scanComplete.emit(self.devices)

    def isConnected(self):
        return self.connected


class Scanner(QRunnable):
        """docstring for __Scanner"""
        def __init__(self, loop, deviceList, callback):
            super(Scanner, self).__init__()
            self.loop = loop
            self.cb = callback
            self.deviceList = deviceList

        async def __scan_Devices(self):
            devices = await discover()
            for d in devices:
                self.deviceList.append(d)

        def run(self):
            self.loop.run_until_complete(self.__scan_Devices())
            self.cb()
