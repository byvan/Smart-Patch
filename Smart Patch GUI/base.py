#! python3

from MainWindow import *
from ble import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

class SmartPatchBase(QApplication):

    threadPool = QThreadPool()
    tim_BatteryLevel = QTimer()

    """docstring for SmartPatchBase"""
    def __init__(self, batteryInterval=1000):
        super(SmartPatchBase, self).__init__([])
        self.setStyle("Macintosh")
        self._initTimers()

        self.config = {"batteryInterval": batteryInterval}

        self.manager = BLEManager(self._tim_BatteryLevel_Activate, self.threadPool)
        self.mainWindow = Window()
        # self.mainWindow = Window(self.manager.isConnected)

        self.manager.setDeviceName = self.mainWindow.setDeviceName
        self.mainWindow.ConnectSignal(self.manager.Connect, self.manager.sig_connect_cb)
        self.mainWindow.DisconnectSignal(self.manager.Disconnect)
        self.mainWindow.ScanningSignal(self.manager.Scan, self.manager.sig_scan_cb)

        self.manager.LostSignal(self.mainWindow.ConnectionLost)

        # self.mainWindow.DFUSignal(self.manager.InitDFU, self.manager.sig_DFUinit_cb, self.manager.SendDFUData, self.manager.sig_DFUData_cb, self.manager.ReadDFUData)








        # self.mainWindow.sig_Scan.connect(self.manager.scan)
        # self.manager.sig_ScanComplete.connect(self.mainWindow._cb_ScanComplete)
        # self.mainWindow.sig_Conn.connect(self.manager.connect)
        # self.manager.sig_ConnSuccess.connect(self.mainWindow._cb_ConnSuccess)
        # self.manager.sig_ConnFailed.connect(self.mainWindow._cb_ConnFailed)
        # self.mainWindow.sig_Disc.connect(self.manager.disconnect)
        # self.manager.sig_ConnLost.connect(self.mainWindow._cb_ConnLost)
        self.manager.sig_BatteryLevel.connect(self.mainWindow._cb_BatteryLevel)
        self.mainWindow.sig_Enable.connect(self.manager.acq_enable)
        self.manager.sig_Enabled.connect(self.mainWindow._cb_ACQEnabled)

        # self.manager.setDeviceName(self.mainWindow.setDeviceName)
        # self.mainWindow.sig_Conn.connect(self._cb_Conn)
        # 
        # 
        # 
        # 
        # 
        
        # Register callbacks and signals for the sensor DFU
        self.mainWindow.SensorDFU_RegisterCallbacks(
            self.manager.SensorDFU_Initialise, self.manager.sig_SensorDFU_ACK,
            self.manager.SensorDFU_Write,
            self.manager.SensorDFU_Read, self.manager.sig_SensorDFU_Read_complete)


    def run(self):
        self.manager.run()
        self.mainWindow.show()
        self.exec()
        # super(SmartPatchBase, self).exec_()

    def _cb_tim_BatteryLevel(self):
        self.manager.readBattery()

    def test(self, data):
        print("Hello")

    def _initTimers(self):
        self.tim_BatteryLevel.timeout.connect(self._cb_tim_BatteryLevel)

    def _tim_BatteryLevel_Activate(self, state):
        if state:
            self._cb_tim_BatteryLevel()
            self.tim_BatteryLevel.start(self.config["batteryInterval"])
        else:
            self.tim_BatteryLevel.stop()
    

        
