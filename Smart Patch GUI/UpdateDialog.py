#! python3

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtTest import *
from typing import Callable

class UpdateDialog(QDialog):

    """
    #################################
            Public Components
    #################################
    """

    ##
    ## Signal to tell the BLE module to initate the sensor DFU.
    ##
    sig_DFUInit = pyqtSignal()
    ##
    ## Signal to command the BLE module to transfer data to the DFU register.
    ##
    sig_sendData = pyqtSignal(bytearray)
    ##
    ## Signal to command the BLE module to read the DFU ACK register.
    ##
    sig_readData = pyqtSignal()

    #---------------------------------------------------------------------------
    ## Parses the msbl file and checks its validity.
    ##
    ## :param      filename:  The filename
    ## :type       filename:  str
    ##
    ## :returns:   Whether the file can be used or not
    ## :rtype:     bool
    ##
    def Parse_File(self, filename: str) -> bool:

        self.log.append("Reading file \"" + filename + "\"...");

        file = open(filename, "rb")
        self.file_data = file.read()
        file.close()

        # File must start with b'msbl'
        if self.file_data[:4] != b'msbl':
            return False

        self.data_IV = self.file_data[0x28:0x33]
        self.data_AB = self.file_data[0x34:0x44]
        self.data_PS = self.file_data[0x44]

        self.log.append("Reading msbl file... {} pages to flash.".format(self.data_PS))
        self.log.append("\nPress \"Run\" to continue.\n")

        return True

    #---------------------------------------------------------------------------
    ## Main dialog function. The return result can be used by the parent to
    ## determine further actions.
    ##
    ## :returns:   The error code of the dialog.
    ## :rtype:     int
    ##
    def Run(self) -> int:
        return self.exec_()

    """
    #################################
            Private Components
    #################################
    """

    ##
    ## Internal signal to trigger transmission of a firmware page.
    ##
    _sig_writePage = pyqtSignal()

    ##
    ## The ACK number from the DFU ACK register, as set by the BLE module
    ##
    _ackedNumber = -1

    ##
    ## Flag to indicate if the acknowledged sequence number has been updated.
    ##
    _ackedNumber_valid = False

    ##
    ## The sequence number we expect the device to acknowledge.
    ##
    _sequenceNumber = 0

    ##
    ## The segment number we expect the device to acknowledge.
    ##
    _segmentNumber = 0

    ##
    ## The exit codes to be printed in the log window.
    ##
    _EXIT_CODES = {
        -1:     "DEV_NOT_RESPONDING",
        -2:     "DFU_INIT_FAILED"
    }

    ##
    ## Buffer length for DFU data on the Smart Patch.
    ##
    _DFU_BUFFER_LEN = (128 + 8)

    #---------------------------------------------------------------------------
    ## Constructor of the sensor DFU dialog window.
    ##
    ## :param      ble_DFUInit:        Callback of the BLE module to initiate
    ##                                 the sensor DFU.
    ## :type       ble_DFUInit:        Callable(None) -> None
    ## :param      ble_DFUInit_cbsig:  The callback signal once DFU
    ##                                 initialisation is complete.
    ## :type       ble_DFUInit_cbsig:  pyqtSignal(bytearray)
    ## :param      ble_DFUData:        Callback of the BLE module to trigger
    ##                                 data transmission to the DFU register.
    ## :type       ble_DFUData:        Callable(bytearray) -> None
    ## :param      ble_DFURead:        Callback of the BLE module to trigger a
    ##                                 read operation of the DFU ACK register.
    ## :type       ble_DFURead:        Callable(None) -> None
    ## :param      ble_DFURead_cbsig:  The callback signal once the DFU ACK
    ##                                 register has been read.
    ## :type       ble_DFURead_cbsig:  pyqtSignal(bytearray)
    ##
    def __init__(self,
        ble_DFUInit: Callable[[None], None], ble_DFUInit_cbsig: pyqtSignal,
        ble_DFUData: Callable[[bytearray], None],
        ble_DFURead: Callable[[None], None], ble_DFURead_cbsig: pyqtSignal):
        super(UpdateDialog, self).__init__()

        # Connect DFU initialisation signals
        self.sig_DFUInit.connect(ble_DFUInit)
        ble_DFUInit_cbsig.connect(self._cb_ble_DFU)

        # Connect DFU data signals
        self.sig_sendData.connect(ble_DFUData)
        self.sig_readData.connect(ble_DFURead)
        ble_DFURead_cbsig.connect(self._cb_ble_ACK)
        self._sig_writePage.connect(self._cb_writePage)

        # Initialise the layout
        self._initLayout()

    #---------------------------------------------------------------------------
    ## Prepares the end of the DFU process and closes the dialog.
    ##
    ## :param      code:  The code to return to the parent window.
    ## :type       code:  int
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def _closeDialog(self, code: int) -> None:
        # Show the error message if an error occurred
        if code:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Update failed with exit code {} ({})!".format(self._EXIT_CODES[code], code))
            msg.exec()

        self.done(code)

    #---------------------------------------------------------------------------
    ## Initializes the layout.
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def _initLayout(self) -> None:
        self.setMinimumWidth(750)
        layout = QVBoxLayout()

        # The progress bar for visual feedback
        self.progress = QProgressBar()
        self.progress.setMinimum(0)
        self.progress.setMaximum(100)
        layout.addWidget(self.progress)


        self.log = QTextEdit("Initiating Update!")
        p = self.log.palette()
        p.setColor(QPalette.Base, Qt.black)
        p.setColor(QPalette.Text, Qt.green)
        self.log.setPalette(p)

        self.log.setReadOnly(True)
        layout.addWidget(self.log)

        # The Run button, triggering the update process
        self.btn_run = QPushButton("Run")
        self.btn_run.clicked.connect(self._btn_run_pressed)
        layout.addWidget(self.btn_run)

        # The cancel button, triggering cancellation
        self.btn_cancel = QPushButton("Cancel")
        self.btn_cancel.clicked.connect(self._btn_cancel_pressed)
        layout.addWidget(self.btn_cancel)

        self.setLayout(layout)

    #---------------------------------------------------------------------------
    ## Reads the DFU ACK register a number of times, expecting a change in the
    ## value.
    ##
    ## Between repeats, the thread waits for 500ms to allow time for the BLE
    ## module to read the data.
    ##
    ## :param      repeats:   The number of repeats to try reading.
    ## :type       repeats:   int
    ## :param      expected:  The expected ACK value
    ## :type       expected:  int
    ##
    ## :returns:   Whether the operation was successful
    ## :rtype:     bool
    ##
    def _readACK(self, repeats: int, expected: int) -> bool:

        old_ack = self._ackedNumber
        self._ackedNumber_valid = False

        while repeats and not self._ackedNumber_valid:
            self.sig_readData.emit()
            QTest.qWait(500)

            if not self._ackedNumber_valid:
                repeats -= 1
            elif self._ackedNumber == expected:
                return True
            elif self._ackedNumber == old_ack:
                repeats -= 1
                self._ackedNumber_valid = False

            print("Repeating: want {} - is {}, valid({})".format(expected, self._ackedNumber, self._ackedNumber_valid))

        return self._ackedNumber_valid and (self._ackedNumber == expected)

    #---------------------------------------------------------------------------
    ## Writes an initialize data.
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def _writeInitData(self) -> None:
        # Prepare the data to be written
        data = [ self._sequenceNumber ]
        data.append(self.data_PS)
        data = bytearray(data) + self.data_IV + self.data_AB

        self.log.append("Writing initialisation data.")
        self._sequenceNumber = 1
        self._segmentNumber = 0
        self.sig_sendData.emit(data)
        self.progress.setValue(5)

        QTest.qWait(1000)
        if not self._readACK(4, 1):
            self.log.append("Failed to write initialisation data!")
        else:
            self.progress.setValue(10)
            self.btn_flash.setEnabled(True)
            self.log.append("\nPress \"Flash\" to flash the data.\n")


    """
    #################################
            Event Callbacks
    #################################
    """

    #---------------------------------------------------------------------------
    ## Callback that is called when the DFU ACK register is read by the BLE
    ## module.
    ##
    ## :param      val:  The value of the register.
    ## :type       val:  int
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def _cb_ble_ACK(self, val: bytearray) -> None:
        self._ackedNumber = val[0]
        self._ackedNumber_valid = True

        print("_cb_ble_ACK: val = {}/{}".format(val[0], val[1]))

    #---------------------------------------------------------------------------
    ## Callback that is called when the DFU ACK register is read by the BLE
    ## module.
    ##
    ## :param      val:  The value of the DFU ACK register.
    ## :type       val:  bytearray
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def _cb_ble_DFU(self, val: bytearray) -> None:

        if self._sequenceNumber == 0:
            # We're still in the initialisation phase

            self.progress.setValue(2)

            if val[0] or val[1]:
                # Sometimes it happens, that the register is read before the device
                # is ready, so give it some time
                if not self._readACK(4, 0):
                    # Something must have gone wrong
                    self.log.append("Failed to set MAX32664 in bootloader mode! ({})".format(val))
                    self._closeDialog(-1)
                    return

                #val[] = self._ackedNumber

                # if val:
                #     # Something must have gone wrong
                #     self.log.append("Failed to set MAX32664 in bootloader mode! ({})".format(val))
                #     self._closeDialog(-2)
                #     return

            # The ACKed number is 0 so we can send the initialisation data
            self._pageSize = (val[3] | (val[4] << 8))
            self._bytesPerPage = self._pageSize + 16
            self._totSegments = val[2]

            msg = "MAX32664 in bootloader mode. (Page Size: {})".format(self._pageSize)
            print(msg)
            self.log.append(msg)
            self.progress.setValue(4)
            self._writeInitData()

        else:
            print("Received ACK {}/{}, with sequence {}/{}".format(val[0], val[1], self._sequenceNumber, self._segmentNumber))
            QTest.qWait(10)

            if val[0] > self.data_PS:
                self.log.append("Done!")
                self.progress.setValue(100)

                msg = QMessageBox()
                msg.setIcon(QMessageBox.Information)
                msg.setText("Update was successful!")
                msg.exec()

                self._closeDialog(0)
                return

            else:
                self._sig_writePage.emit()

    #---------------------------------------------------------------------------
    ## The callback that is called when the @_sig_writePage signal is emitted.
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def _cb_writePage(self) -> None:
        
        idx_low = 0x4c + (self._sequenceNumber - 1) * self._bytesPerPage + self._segmentNumber * self._DFU_BUFFER_LEN
        idx_high = 0x4c + (self._sequenceNumber - 1) * self._bytesPerPage  + (self._segmentNumber + 1) * self._DFU_BUFFER_LEN

        if idx_high > (0x4c + self._sequenceNumber * self._bytesPerPage):
            idx_high = (0x4c + self._sequenceNumber * self._bytesPerPage)

        data = bytearray([ self._sequenceNumber ]) + self.file_data[idx_low:idx_high]

        msg = "\tWriting segment 0x{:02X} - 0x{:02X} ({} bytes).".format(idx_low, idx_high-1, idx_high - idx_low)
        print(msg)
        self.log.append(msg)
        self.progress.setValue(10 + int( (90/self._totSegments) * (self._totSegments*self._sequenceNumber + self._segmentNumber)/(1 + self.data_PS)) )
        
        self._segmentNumber += 1

        if self._segmentNumber == self._totSegments:
            self._sequenceNumber += 1
            self._segmentNumber = 0

        self.sig_sendData.emit(data)


    """
    #################################
            Button Callbacks
    #################################
    """
    
    #---------------------------------------------------------------------------
    ## Callback that is called when the "Cancel" button is pressed.
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def _btn_cancel_pressed(self) -> None:
        # TODO: Clean up
        self._closeDialog(0)

    #---------------------------------------------------------------------------
    ## Callback that is called when the "Run" button is pressed.
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def _btn_run_pressed(self) -> None:
        self.log.append("Initiating bootloading process...")
        self.sig_DFUInit.emit()
