#! python3

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

class MainWindow(QWidget):
    """docstring for MainWindow"""
    def __init__(self):
        super(MainWindow, self).__init__()
        
        self.param_rr = 16
        self.param_spo2 = 96
        self.param_supp = False
        self.param_temp = 37.0
        self.param_bp = 135
        self.param_hr = 71
        self.param_consc = True

        self.initLayout()

    def initLayout(self):
        self.mainLayout = QGridLayout()
        self.mainLayout.addWidget(QLabel("Respiratory Rate"), 0, 0, 1, 1)
        self.mainLayout.addWidget(QLabel("Oxygen Saturation"),  1, 0, 1, 1)
        self.mainLayout.addWidget(QLabel("Supplemental Oxygen"), 2, 0, 1, 1)
        self.mainLayout.addWidget(QLabel("Temperature"), 3, 0, 1, 1)
        self.mainLayout.addWidget(QLabel("Systolic Blood Pressure"), 4, 0, 1, 1)
        self.mainLayout.addWidget(QLabel("Heart Rate"), 5, 0, 1, 1)
        self.mainLayout.addWidget(QLabel("Level of Consciousness"), 6, 0, 1, 1)

        self.init_RR()
        self.init_SpO2()
        self.init_Supp()
        self.init_Temp()
        self.init_BP()
        self.init_HR()
        self.init_Consc()

        self.mainLayout.addWidget(QSplitter(Qt.Horizontal), 7, 0, 1, 3)
        self.mainLayout.addWidget(QSplitter(Qt.Horizontal), 8, 0, 1, 3)
        self.mainLayout.addWidget(QLabel("EWS Score:"), 9, 0, 1, 1)
        self.score_l = QLabel("0")
        self.mainLayout.addWidget(self.score_l, 9, 1, 1, 2)

        self.setLayout(self.mainLayout)

    def init_RR(self):
        self.ui_rr = QSlider(Qt.Horizontal)
        self.ui_rr.setMinimum(8)
        self.ui_rr.setMaximum(25)
        self.ui_rr.setValue(self.param_rr)
        self.ui_rr.setFixedWidth(150)
        self.ui_rr_l = QLabel(str(self.param_rr))
        self.ui_rr_l.setFixedWidth(30)
        self.ui_rr.valueChanged.connect(self.cb_RR)
        self.mainLayout.addWidget(self.ui_rr, 0, 1, 1, 1)
        self.mainLayout.addWidget(self.ui_rr_l, 0, 2, 1, 1)

    def cb_RR(self, val):
        self.param_rr = val
        self.ui_rr_l.setText(str(self.param_rr))
        self.ScoreChanged()

    def init_SpO2(self):
        self.ui_spo2 = QSlider(Qt.Horizontal)
        self.ui_spo2.setMinimum(91)
        self.ui_spo2.setMaximum(100)
        self.ui_spo2.setValue(self.param_spo2)
        self.ui_spo2.setFixedWidth(150)
        self.ui_spo2_l = QLabel(str(self.param_spo2))
        self.ui_spo2_l.setFixedWidth(30)
        self.ui_spo2.valueChanged.connect(self.cb_SpO2)
        self.mainLayout.addWidget(self.ui_spo2, 1, 1, 1, 1)
        self.mainLayout.addWidget(self.ui_spo2_l, 1, 2, 1, 1)

    def cb_SpO2(self, val):
        self.param_spo2 = val
        self.ui_spo2_l.setText(str(self.param_spo2))
        self.ScoreChanged()

    def init_Supp(self):
        self.ui_supp_gb = QGroupBox()
        self.ui_supp_gb_layout = QHBoxLayout()

        self.ui_supp_gb_layout.addWidget(QLabel("No"))
        self.ui_supp_no = QRadioButton()
        self.ui_supp_no.setChecked(True)
        self.ui_supp_gb_layout.addWidget(self.ui_supp_no)

        self.ui_supp_gb_layout.addWidget(QLabel("Yes"))
        self.ui_supp_yes = QRadioButton()
        self.ui_supp_yes.toggled.connect(self.cb_Supp)
        self.ui_supp_gb_layout.addWidget(self.ui_supp_yes)

        self.ui_supp_gb.setLayout(self.ui_supp_gb_layout)
        self.mainLayout.addWidget(self.ui_supp_gb, 2, 1, 1, 2)

    def cb_Supp(self, val):
        self.param_supp = val
        self.ScoreChanged()

    def init_Temp(self):
        self.ui_temp = QSlider(Qt.Horizontal)
        self.ui_temp.setMinimum(350)
        self.ui_temp.setMaximum(391)
        self.ui_temp.setValue(int(10*self.param_temp))
        self.ui_temp.setFixedWidth(150)
        self.ui_temp_l = QLabel(str(self.param_temp))
        self.ui_temp_l.setFixedWidth(30)
        self.ui_temp.valueChanged.connect(self.cb_Temp)
        self.mainLayout.addWidget(self.ui_temp, 3, 1, 1, 1)
        self.mainLayout.addWidget(self.ui_temp_l, 3, 2, 1, 1)

    def cb_Temp(self, val):
        self.param_temp = float(val)/10
        self.ui_temp_l.setText(str(self.param_temp))
        self.ScoreChanged()

    def init_BP(self):
        self.ui_bp = QSlider(Qt.Horizontal)
        self.ui_bp.setMinimum(90)
        self.ui_bp.setMaximum(220)
        self.ui_bp.setValue(self.param_bp)
        self.ui_bp.setFixedWidth(150)
        self.ui_bp_l = QLabel(str(self.param_bp))
        self.ui_bp_l.setFixedWidth(30)
        self.ui_bp.valueChanged.connect(self.cb_BP)
        self.mainLayout.addWidget(self.ui_bp, 4, 1, 1, 1)
        self.mainLayout.addWidget(self.ui_bp_l, 4, 2, 1, 1)

    def cb_BP(self, val):
        self.param_bp = val
        self.ui_bp_l.setText(str(self.param_bp))
        self.ScoreChanged()

    def init_HR(self):
        self.ui_hr = QSlider(Qt.Horizontal)
        self.ui_hr.setMinimum(40)
        self.ui_hr.setMaximum(131)
        self.ui_hr.setValue(self.param_hr)
        self.ui_hr.setFixedWidth(150)
        self.ui_hr_l = QLabel(str(self.param_hr))
        self.ui_hr_l.setFixedWidth(30)
        self.ui_hr.valueChanged.connect(self.cb_HR)
        self.mainLayout.addWidget(self.ui_hr, 5, 1, 1, 1)
        self.mainLayout.addWidget(self.ui_hr_l, 5, 2, 1, 1)

    def cb_HR(self, val):
        self.param_hr = val
        self.ui_hr_l.setText(str(self.param_hr))
        self.ScoreChanged()

    def init_Consc(self):
        self.ui_consc_gb = QGroupBox()
        self.ui_consc_gb_layout = QHBoxLayout()

        self.ui_consc_gb_layout.addWidget(QLabel("A"))
        self.ui_consc_a = QRadioButton()
        self.ui_consc_a.setChecked(True)
        self.ui_consc_a.toggled.connect(self.cb_Consc)
        self.ui_consc_gb_layout.addWidget(self.ui_consc_a)

        self.ui_consc_gb_layout.addWidget(QLabel("V"))
        self.ui_consc_gb_layout.addWidget(QRadioButton()) 
        self.ui_consc_gb_layout.addWidget(QLabel("P"))
        self.ui_consc_gb_layout.addWidget(QRadioButton())
        self.ui_consc_gb_layout.addWidget(QLabel("U"))
        self.ui_consc_gb_layout.addWidget(QRadioButton())
        self.ui_consc_gb.setLayout(self.ui_consc_gb_layout)
        self.mainLayout.addWidget(self.ui_consc_gb, 6, 1, 1, 2)

    def cb_Consc(self, val):
        self.param_consc = val
        self.ScoreChanged()

    def ScoreChanged(self):
        self.score = 0
        self.score += self.getScore_RR()
        self.score += self.getScore_SpO2()
        self.score += self.getScore_Supp()
        self.score += self.getScore_Temp()
        self.score += self.getScore_BP()
        self.score += self.getScore_HR()
        self.score += self.getScore_Consc()

        self.score_l.setText(str(self.score))

        # print("----------")
        # print("RR       : {}".format(self.param_rr))
        # print("SpO2     : {}".format(self.param_spo2))
        # print("Supp O2  : {}".format(self.param_supp))
        # print("Temp     : {}".format(self.param_temp))
        # print("BP       : {}".format(self.param_bp))
        # print("HR       : {}".format(self.param_hr))
        # print("Conscious: {}".format(self.param_consc))
        # print("Score: {}".format(self.score))

    def getScore_RR(self):
        if self.param_rr > 24:
            return 3
        elif self.param_rr > 20:
            return 2
        elif self.param_rr > 11:
            return 0
        elif self.param_rr > 8:
            return 1
        else:
            return 3

    def getScore_SpO2(self):
        if self.param_spo2 > 95:
            return 0
        elif self.param_spo2 > 93:
            return 1
        elif self.param_spo2 > 91:
            return 2
        else:
            return 3

    def getScore_Supp(self):
        if self.param_supp:
            return 2
        else:
            return 0

    def getScore_Temp(self):
        if self.param_temp >= 39.1:
            return 2
        elif self.param_temp >= 38.1:
            return 1
        elif self.param_temp >= 36.1:
            return 0
        elif self.param_temp >= 35.1:
            return 1
        else:
            return 3

    def getScore_BP(self):
        if self.param_bp > 219:
            return 3
        elif self.param_bp > 110:
            return 0
        elif self.param_bp > 100:
            return 1
        elif self.param_bp > 90:
            return 2
        else:
            return 3

    def getScore_HR(self):
        if self.param_hr > 130:
            return 3
        elif self.param_hr > 110:
            return 2
        elif self.param_hr > 90:
            return 1
        elif self.param_hr > 50:
            return 0
        elif self.param_hr > 40:
            return 1
        else:
            return 3

    def getScore_Consc(self):
        if self.param_consc:
            return 0
        else:
            return 3


suppOx = False
consc = True

def test():
    global suppOx, consc
    print(suppOx)
    print(consc)
    print("--------")

def createWidgets(window):

    def setSuppOx(val):
        global suppOx
        suppOx = val
        # test()

    def setConsc(val):
        global consc
        consc = val
        test()

    mainLayout = QGridLayout()

    mainLayout.addWidget(QLabel("Respiratory Rate"), 0, 0, 1, 1)
    # param_rr = QSlider(Qt.Horizontal)
    # param_rr.setMinimum(8)
    # param_rr.setMaximum(25)
    # param_rr.setValue(16)
    # param_rr.setFixedWidth(150)
    # val_rr = QLabel(str(param_rr.value()))
    # val_rr.setFixedWidth(30)
    # param_rr.valueChanged.connect(lambda x: val_rr.setText(str(x)) )
    # mainLayout.addWidget(param_rr, 0, 1, 1, 1)
    # mainLayout.addWidget(val_rr, 0, 2, 1, 1)

    mainLayout.addWidget(QLabel("Oxygen Saturation"),  1, 0, 1, 1)
    

    mainLayout.addWidget(QLabel("Supplemental Oxygen"), 2, 0, 1, 1)
    

    mainLayout.addWidget(QLabel("Temperature"), 3, 0, 1, 1)
    


    mainLayout.addWidget(QLabel("Systolic Blood Pressure"), 4, 0, 1, 1)
    

    mainLayout.addWidget(QLabel("Heart Rate"), 5, 0, 1, 1)
    

    mainLayout.addWidget(QLabel("Level of Consciousness"), 6, 0, 1, 1)
    

    # mainLayout.addWidget(QPushButton("Test"))

    window.setLayout(mainLayout)

### Main
if __name__ == '__main__':
    app = QApplication([])
    window = MainWindow()

    # createWidgets(window)

    window.show()

    app.exec()
