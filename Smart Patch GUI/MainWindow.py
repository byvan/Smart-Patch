#! python3

from ble import *
from enum import Enum
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtTest import *
import types
from UpdateDialog import *

class WindowState(Enum):
    STATE_INIT          = 0,
    STATE_CONNECTING    = 1,
    STATE_CONNECTED     = 2,
    STATE_SCANNING      = 3

class Window(QWidget):

    ##
    ## Flag to indicate if Sensor DFU is supported by the BLE module.
    ##
    _sensorDFU_enabled = False

    ##
    ## Constructor for the main Smart Patch application window.
    ##
    def __init__(self):
        super(Window, self).__init__()
        
        self.setMinimumWidth(750)
        self.mainLayout = QGridLayout()
        self._init_TopLayout()
        self._init_MidLayout()
        self._init_BotLayout()
        self.setLayout(self.mainLayout)

        self.state = WindowState.STATE_INIT

    def SensorDFU_RegisterCallbacks(self,
        ble_DFUInit: Callable[[None], None], ble_DFUInit_cbsig: pyqtSignal,
        ble_DFUData: Callable[[bytearray], None],
        ble_DFURead: Callable[[None], None], ble_DFURead_cbsig: pyqtSignal):
        """
        Registers all necessary callbacks for the DFU of the sensor suite.
        
        :param      ble_DFUInit:        Callback of the BLE module to initiate
                                        the sensor DFU.
        :type       ble_DFUInit:        Callable(None) -> None
        :param      ble_DFUInit_cbsig:  The callback signal once DFU
                                        initialisation is complete.
        :type       ble_DFUInit_cbsig:  pyqtSignal(bytearray)
        :param      ble_DFUData:        Callback of the BLE module to trigger
                                        data transmission to the DFU register.
        :type       ble_DFUData:        Callable(bytearray) -> None
        :param      ble_DFURead:        Callback of the BLE module to trigger a
                                        read operation of the DFU ACK register.
        :type       ble_DFURead:        Callable(None) -> None
        :param      ble_DFURead_cbsig:  The callback signal once the DFU ACK
                                        register has been read.
        :type       ble_DFURead_cbsig:  pyqtSignal(bytearray)
        """

        self._sensorDFU_ble_Init = ble_DFUInit
        self._sensorDFU_ble_Init_cbsig = ble_DFUInit_cbsig
        self._sensorDFU_ble_Data = ble_DFUData
        self._sensorDFU_ble_Read = ble_DFURead
        self._sensorDFU_ble_Read_cbsig = ble_DFURead_cbsig

        self._sensorDFU_enabled = True

    """
    #################################
            Button Callbacks
    #################################
    """

    def _btn_sensorDFU_pressed(self):
        """
        Callback that is called, when the sensor DFU button is pressed.
        """

        # if self.state != WindowState.STATE_CONNECTED:
        if False:
            # The device is not connected
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setText("Device not connected!")
            msg.exec()
            return

        elif not self._sensorDFU_enabled:
            # The application does not support sensor DFU
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setText("Sensor DFU not supported!")
            msg.exec()
            return

        dial = UpdateDialog(self._sensorDFU_ble_Init, self._sensorDFU_ble_Init_cbsig,
            self._sensorDFU_ble_Data,
            self._sensorDFU_ble_Read, self._sensorDFU_ble_Read_cbsig)

        # let the dialog check the validity of the msbl file
        if not dial.Parse_File(self._sensorDFU_filename):
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Could not parse update file!")
            msg.exec()
            return

        dial.Run()
        print("TODO Clean up")




    # def _btnMAXUp_pressed(self):
    #     # if self.state != WindowState.STATE_CONNECTED:
    #     if False:
    #         msg = QMessageBox()
    #         msg.setIcon(QMessageBox.Warning)
    #         msg.setText("Device not connected!")
    #         msg.exec()
    #     else:
    #         # dial = UpdateDialog(self.MAXfileName, self.DFUInit, self.cb_DFUInit, self.DFUData, self.cb_DFUData, self.DFURead)

    #         if not dial.parseFile():
    #             msg = QMessageBox()
    #             msg.setIcon(QMessageBox.Critical)
    #             msg.setText("Bad msbl file!")
    #             msg.exec()
    #         else:
    #             if dial.run():
    #                 print("TODO Clean up")
    










    sig_connect = pyqtSignal(int)
    sig_disconnect = pyqtSignal()
    sig_scan = pyqtSignal()

    # sig_Conn = pyqtSignal(int)
    # sig_Disc = pyqtSignal()
    sig_Enable = pyqtSignal()


    

 




    def ConnectSignal(self, callback, backsignal):
        self.sig_connect.connect(callback)
        backsignal.connect(self._cb_Connected)

    def ConnectionLost(self):
        self._setupState_Init()
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)
        msg.setText("Connection lost!")
        msg.exec()

    def DFUSignal(self, init, cb_init, data, cb_data, read_data):
        self.DFUInit = init
        self.cb_DFUInit = cb_init
        self.DFUData = data
        self.cb_DFUData = cb_data
        self.DFURead = read_data

    def DisconnectSignal(self, callback):
        self.sig_disconnect.connect(callback)

    def ScanningSignal(self, callback, backsignal):
        self.sig_scan.connect(callback)
        backsignal.connect(self._cb_ScanComplete)

    def setDeviceName(self, name):
        self.status_Name.setText(name)

    def _btn_ConnPressed(self):
        if self.state == WindowState.STATE_INIT:
            self._trig_connect()
        else:
            self._trig_disconnect()

        # if self.isConnected():
        #     self.btnConn.setEnabled(True)
        #     self.btnEnable.setEnabled(False)
        #     self.btnScan.setEnabled(True)
        #     self.deviceBox.setEnabled(True)
        #     self.status_Bat.setText("-")
        #     self.sig_Disc.emit()
        #     self.btnConn.setText("Connect")
        #     self.btnEnable.setText("Start Sensors");
        # else:
        #     self.btnConn.setEnabled(False)
        #     self.btnScan.setEnabled(False)
        #     self.deviceBox.setEnabled(False)
        #     self.btnEnable.setEnabled(True)
        #     self.btnEnable.setText("Start Sensors");
        #     self.sig_Conn.emit(self.deviceBox.currentIndex())
    
    def _btnMAX_pressed(self):
        options = QFileDialog.Options()
        options |= QFileDialog.ReadOnly
        self._sensorDFU_filename, _ = QFileDialog.getOpenFileName(self,
            "Chose application firmware for MAX32664", "","Sensor Hub (*.msbl);;All Files (*)", options=options)
        
        if self._sensorDFU_filename:
            if len(self._sensorDFU_filename) < 35:
                self.MAXFile.setText(self._sensorDFU_filename)
            else:
                self.MAXFile.setText(self._sensorDFU_filename[-35:])

            self.btnMAXUp.setEnabled(True)

        else:
            self.MAXFile.setText("No file selected")
            self.btnMAXUp.setEnabled(False)

    
            

    def _btn_ScanPressed(self):
        if not self.state == WindowState.STATE_INIT:
            return

        self._setupState_Scanning()
        self.sig_scan.emit()
        

    def _cb_Connected(self, status):
        if status:
            self._cb_ConnSuccess()
        else:
            self._cb_ConnFailed()

    def _cb_ConnSuccess(self):
        self._setupState_Connected()

    def _cb_ConnFailed(self):
        self._setupState_Init()
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Connection failed!")
        msg.exec()

    def _cb_MAXfailed(self):
        print("Max failed")

    def _cb_MAXsuccess(self):
        print("Max success")

    def _cb_ScanComplete(self, devices):
        self.deviceBox.clear()
        for d in devices:
            # if d.name == "Smart Patch":
            self.deviceBox.addItems({"{0} ({1})".format(d.name, d.address)})

        self._setupState_Init()
    
    def _trig_connect(self):
        self._setupState_Connecting()
        self.sig_connect.emit(self.deviceBox.currentIndex())

    def _trig_disconnect(self):
        self.sig_disconnect.emit()
        self._setupState_Init()

    def _setupState_Init(self):

        # Switch off everything that is not usable anymore
        self.btnEnable.setEnabled(False)

        # Switch on everything we need again
        self.btnScan.setEnabled(True)
        self.btnConn.setText("Connect")
        self.setDeviceName("Unknown")
        self._setBatteryLevel(-1)

        if self.deviceBox.count() > 0:
            self.btnConn.setEnabled(True)
            self.deviceBox.setEnabled(True)

        self.state = WindowState.STATE_INIT

    def _setupState_Connecting(self):
        self.btnScan.setEnabled(False)
        self.btnConn.setEnabled(False)
        self.deviceBox.setEnabled(False)

        self.state = WindowState.STATE_CONNECTING

    def _setupState_Connected(self):
        self.btnConn.setText("Disconnect")
        self.btnConn.setEnabled(True)

        self.state = WindowState.STATE_CONNECTED

    def _setupState_Scanning(self):
        self.btnConn.setEnabled(False)
        self.btnScan.setEnabled(False)
        self.deviceBox.setEnabled(False)

        self.state = WindowState.STATE_SCANNING

    def _setBatteryLevel(self, val):
        if val == -1:
            self.status_Bat.setText("-")
        else:
            self.status_Bat.setText(str(val))

    

    def _init_TopLayout(self):
        self.topLayout = QHBoxLayout()
        self.topLayout.addWidget(QLabel("Device: "))

        self.deviceBox = QComboBox()
        self.deviceBox.setEnabled(False)
        self.deviceBox.setMaximumWidth(150)

        self.btnScan = QPushButton("Scan")
        self.btnScan.setEnabled(False)
        self.btnScan.clicked.connect(self._btn_ScanPressed)

        self.btnConn = QPushButton("Connect")
        self.btnConn.setEnabled(False)
        self.btnConn.clicked.connect(self._btn_ConnPressed)

        self.topLayout.addWidget(self.deviceBox)
        self.topLayout.addWidget(self.btnScan)
        self.topLayout.addWidget(self.btnConn)

        self.mainLayout.addLayout(self.topLayout, 0, 0, 1, 2)

    def _init_MidLayout(self):
        # Control Layout
        self.ctrlLayout = QGridLayout()

        # Sensor data
        self.btnEnable = QPushButton("Start Sensors")
        self.btnEnable.setEnabled(False)
        self.btnEnable.clicked.connect(self._btnEnable_pressed)

        self.SPData_table = QTableWidget(4, 2)
        self.SPData_HR = QTableWidgetItem("0")
        self.SPData_SpO2 = QTableWidgetItem("0")
        self.SPData_BP = QTableWidgetItem("0")
        self.SPData_Temp = QTableWidgetItem("0")
        self.SPData_table.setItem(0, 0, QTableWidgetItem("HR"))
        self.SPData_table.setItem(0, 1, self.SPData_HR)
        self.SPData_table.setItem(1, 0, QTableWidgetItem("SpO2"))
        self.SPData_table.setItem(1, 1, self.SPData_SpO2)
        self.SPData_table.setItem(2, 0, QTableWidgetItem("BP"))
        self.SPData_table.setItem(2, 1, self.SPData_BP)
        self.SPData_table.setItem(3, 0, QTableWidgetItem("Temp"))
        self.SPData_table.setItem(3, 1, self.SPData_Temp)

        self.ctrlLayout.addWidget(self.btnEnable, 0, 0, 1, 1)
        self.ctrlLayout.addWidget(self.SPData_table, 0, 1, 1, 1)

        self.mainLayout.addLayout(self.ctrlLayout, 1, 0, 1, 1)

        # MAX32664 DFU
        line = QFrame()
        line.setFrameShape(QFrame.HLine)
        line.setFrameShadow(QFrame.Sunken)

        self.btnMAX = QPushButton("Select File")
        self.btnMAX.setEnabled(True)
        self.btnMAX.clicked.connect(self._btnMAX_pressed)

        self.MAXFile = QLabel("No file selected")
        self.MAXFile.setStyleSheet("font-weight: Italic")

        self.btnMAXUp = QPushButton("Update")
        self.btnMAXUp.setEnabled(False)
        self.btnMAXUp.clicked.connect(self._btn_sensorDFU_pressed)

        line2 = QFrame()
        line2.setFrameShape(QFrame.HLine)
        line2.setFrameShadow(QFrame.Sunken)

        self.ctrlLayout.addWidget(line, 2, 0, 1, 2)
        self.ctrlLayout.addWidget(QLabel("MAX32664 Firmware Upgrade"), 3, 0, 1, 1)        
        self.ctrlLayout.addWidget(self.btnMAX, 4, 0, 1, 1)
        self.ctrlLayout.addWidget(self.MAXFile, 4, 1, 1, 1)
        self.ctrlLayout.addWidget(self.btnMAXUp, 5, 0, 1, 1)
        self.ctrlLayout.addWidget(line2, 6, 0, 1, 2)

    def _init_BotLayout(self):
        # Status and Battery section
        self.statusLayout = QHBoxLayout()

        self.status_Name = QLabel("Unknown")
        self.status_Name.setFixedWidth(100)
        self.statusLayout.addWidget(self.status_Name)
        
        self.status_Bat = QLabel("-")
        self.status_Bat.setAlignment(Qt.AlignRight)
        self.status_Bat.setFixedWidth(22)
        self.statusLayout.addWidget(QLabel("Battery:"))
        self.statusLayout.addWidget(self.status_Bat)
        self.statusLayout.addWidget(QLabel("%"))

        self.mainLayout.addLayout(self.statusLayout, 2, 0, 1, 1)












    

    

    

    def _btnEnable_pressed(self):
        self.sig_Enable.emit()

    

    def _cb_ACQEnabled(self, state):
        if state:
            self.btnEnable.setText("Stop Sensors")
        else:
            self.btnEnable.setText("Start Sensors")


    def _cb_BatteryLevel(self, level):
        self.status_Bat.setText(str(level))


# class UpdateDialog(QDialog):

#     sig_DFUInit = pyqtSignal()
#     sig_sendData = pyqtSignal(bytearray)

#     """docstring for UpdateDialog"""
#     def __init__(self, file, DFUInit, cb_DFUInit, DFUData, cb_DFUData, DFURead):
#         super(UpdateDialog, self).__init__()

#         self.filename = file
#         self.sig_DFUInit.connect(DFUInit)
#         self.sig_sendData.connect(DFUData)
#         self.DFURead = DFURead

#         cb_DFUInit.connect(self.cb_DFUInit)
#         cb_DFUData.connect(self.cb_DFUData)

#         layout = QVBoxLayout()
#         self.pgBar = QProgressBar()
#         self.pgBar.setMinimum(0)
#         self.pgBar.setMaximum(100)
#         layout.addWidget(self.pgBar)

#         self.log = QTextEdit("Initiating Update!")
#         p = self.log.palette()
#         p.setColor(QPalette.Base, Qt.black)
#         p.setColor(QPalette.Text, Qt.green)
#         self.log.setPalette(p)

#         # self.log.setAutoFillBackground(True)
#         self.log.setReadOnly(True)
#         # self.log.setTextBackgroundColor(QColor(Qt.black))
#         # self.log.setTextColor(QColor(Qt.green))
#         layout.addWidget(self.log)

#         self.btn_run = QPushButton("Run")
#         self.btn_run.clicked.connect(self.btn_run_pressed)
#         layout.addWidget(self.btn_run)

#         self.btn_cancel = QPushButton("Cancel")
#         self.btn_cancel.clicked.connect(self.btn_cancel_pressed)
#         layout.addWidget(self.btn_cancel)

#         self.setLayout(layout)

#     def closeDialog(self, code):
#         if code:
#             msg = QMessageBox()
#             msg.setIcon(QMessageBox.Critical)
#             msg.setText("Failed to update MAX32664 ({})!".format(code))
#             msg.exec()

#         self.done(code)

#     def initDFU(self):
#         self.sig_DFUInit.emit()

#     def parseFile(self):
#         file = open(self.filename, "rb")
#         self.file_data = file.read()
#         file.close()

#         if self.file_data[:4] != b'msbl':
#             return False

#         self.data_IV = self.file_data[0x28:0x33]
#         self.data_AB = self.file_data[0x34:0x44]
#         self.data_PS = self.file_data[0x44]

#         self.log.append("Reading msbl file... {} pages to flash.".format(self.data_PS))
#         self.log.append("Press \"Run\" to continue")

#         return True

#     def run(self):
#         return self.exec_()

#     def sendInitData(self):
#         self.sequence = 0;  # sequence identifier

#         data = [ self.sequence ]
#         data.append(self.data_PS)
#         data = bytearray(data) + self.data_IV + self.data_AB

#         self.log.append("Transmitting initialisation data.")
#         self.sig_sendData.emit(data)
        
#     def btn_cancel_pressed(self):
#         self.closeDialog(0)

#     def btn_run_pressed(self):
#         self.initDFU()

#     def cb_DFUInit(self, val):
#         if val:
#             self.log.append("Failed to set MAX32664 in bootloader mode! ({})".format(val))
#             self.closeDialog(-1)
#             return

#         counter = 4
#         val = self.DFURead()
#         while val and counter:
#             self.log.append("Waiting...")
#             QTest.qWait(500)
#             counter -= 1
#             val = self.DFURead()

#         if not counter:
#             self.log.append("Could not read ACK")
#             self.closeDialog(-1)
#             return

#         self.log.append("MAX32664 setup in bootloader mode.")

#         self.sendInitData()

#     def cb_DFUData(self, ack):
#         print("DFU Data CB: {}".format(ack))

#         if ack == 0:
#             self.sendInitData()


