/**
 * @file main.c
 * @brief      Smart Patch nRF52833 board definitions.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       October 2020
 */

#ifndef __SMARTPATCH_BOARD_H
#define __SMARTPATCH_BOARD_H

#ifdef __cplusplus
extern "C" {
#endif

/*! TWI interface */
#define TWI_MASTER_INST         0
/*! TWI Master SCL pin */
#define TWI_MASTER_SCL          41
/*! TWI Master SDA pin */
#define TWI_MASTER_SDA          11

/*! UART RX Pin */
#define RX_PIN_NUMBER           8
/*! UART TX Pin */
#define TX_PIN_NUMBER           6
/*! UART CTS Pin */
// #define CTS_PIN_NUMBER          
/*! UART RTS Pin */
// #define RTS_PIN_NUMBER          

#ifdef __cplusplus
}
#endif

#endif /* __SMARTPATCH_BOARD_H included */