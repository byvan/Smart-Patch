#ifndef __NEWCFG_H_
#define __NEWCFG_H_

// <h> nRF_BLE_Services

//==========================================================
// <e> BLE_BAS_ENABLED - ble_bas - Battery Service
//==========================================================
#define BLE_BAS_ENABLED 1

// </h> 
//==========================================================


// <h> nRF_Log 

//==========================================================
// <e> NRF_LOG_ENABLED - nrf_log - Logger
//==========================================================
#define NRF_LOG_ENABLED 1

//==========================================================
// <e> NRF_LOG_BACKEND_RTT_ENABLED - nrf_log_backend_rtt - Log RTT backend
//==========================================================
#define NRF_LOG_BACKEND_RTT_ENABLED 0

// </e>

// <e> NRF_LOG_BACKEND_UART_ENABLED - nrf_log_backend_uart - Log UART backend
//==========================================================
#define NRF_LOG_BACKEND_UART_ENABLED 1

// <o> NRF_LOG_BACKEND_UART_TX_PIN - UART TX pin
#define NRF_LOG_BACKEND_UART_TX_PIN 6

// <o> NRF_LOG_BACKEND_UART_BAUDRATE  - Default Baudrate
// <323584=> 1200 baud 
// <643072=> 2400 baud 
// <1290240=> 4800 baud 
// <2576384=> 9600 baud 
// <3862528=> 14400 baud 
// <5152768=> 19200 baud 
// <7716864=> 28800 baud 
// <10289152=> 38400 baud 
// <15400960=> 57600 baud 
// <20615168=> 76800 baud 
// <30801920=> 115200 baud 
// <61865984=> 230400 baud 
// <67108864=> 250000 baud 
// <121634816=> 460800 baud 
// <251658240=> 921600 baud 
// <268435456=> 1000000 baud 
#define NRF_LOG_BACKEND_UART_BAUDRATE 30801920

// <o> NRF_LOG_BACKEND_UART_TEMP_BUFFER_SIZE - Size of buffer for partially processed strings. 
// <i> Size of the buffer is a trade-off between RAM usage and processing.
// <i> if buffer is smaller then strings will often be fragmented.
// <i> It is recommended to use size which will fit typical log and only the
// <i> longer one will be fragmented.
#define NRF_LOG_BACKEND_UART_TEMP_BUFFER_SIZE 64

// </e>

// <h> Log message pool - Configuration of log message pool
//==========================================================

// <o> NRF_LOG_MSGPOOL_ELEMENT_SIZE - Size of a single element in the pool of memory objects. 
// <i> If a small value is set, then performance of logs processing
// <i> is degraded because data is fragmented. Bigger value impacts
// <i> RAM memory utilization. The size is set to fit a message with
// <i> a timestamp and up to 2 arguments in a single memory object.
#define NRF_LOG_MSGPOOL_ELEMENT_SIZE 20

// <o> NRF_LOG_MSGPOOL_ELEMENT_COUNT - Number of elements in the pool of memory objects 
// <i> If a small value is set, then it may lead to a deadlock
// <i> in certain cases if backend has high latency and holds
// <i> multiple messages for long time. Bigger value impacts
// <i> RAM memory usage.
#define NRF_LOG_MSGPOOL_ELEMENT_COUNT 8

// </h> 
//==========================================================


// <o> NRF_LOG_ALLOW_OVERFLOW  - Configures behavior when circular buffer is full.
// <i> If set then oldest logs are overwritten. Otherwise a 
// <i> marker is injected informing about overflow.
#define NRF_LOG_ALLOW_OVERFLOW 1

// <o> NRF_LOG_BUFSIZE  - Size of the buffer for storing logs (in bytes). 
// <i> Must be power of 2 and multiple of 4.
// <i> If NRF_LOG_DEFERRED = 0 then buffer size can be reduced to minimum.
// <128=> 128 
// <256=> 256 
// <512=> 512 
// <1024=> 1024 
// <2048=> 2048 
// <4096=> 4096 
// <8192=> 8192 
// <16384=> 16384 
#define NRF_LOG_BUFSIZE 1024

// <o> NRF_LOG_CLI_CMDS  - Enable CLI commands for the module.
#define NRF_LOG_CLI_CMDS 0

// <o> NRF_LOG_DEFAULT_LEVEL  - Default Severity level
// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 
#define NRF_LOG_DEFAULT_LEVEL 4

// <o> NRF_LOG_DEFERRED  - Enable deffered logger.
// <i> Log data is buffered and can be processed in idle.
#define NRF_LOG_DEFERRED 0

// <o> NRF_LOG_FILTERS_ENABLED  - Enable dynamic filtering of logs.
#define NRF_LOG_FILTERS_ENABLED 0

// <o> NRF_LOG_NON_DEFFERED_CRITICAL_REGION_ENABLED  - Enable use of critical region for non deffered mode when flushing logs.
// <i> When enabled NRF_LOG_FLUSH is called from critical section when non deffered mode is used.
// <i> Log output will never be corrupted as access to the log backend is exclusive
// <i> but system will spend significant amount of time in critical section
#define NRF_LOG_NON_DEFFERED_CRITICAL_REGION_ENABLED 0

// <o> NRF_LOG_STR_PUSH_BUFFER_SIZE  - Size of the buffer dedicated for strings stored using @ref NRF_LOG_PUSH.
// <16=> 16 
// <32=> 32 
// <64=> 64 
// <128=> 128 
// <256=> 256 
// <512=> 512 
// <1024=> 1024 
#define NRF_LOG_STR_PUSH_BUFFER_SIZE 128

// <o> NRF_LOG_STR_PUSH_BUFFER_SIZE  - Size of the buffer dedicated for strings stored using @ref NRF_LOG_PUSH.
// <16=> 16 
// <32=> 32 
// <64=> 64 
// <128=> 128 
// <256=> 256 
// <512=> 512 
// <1024=> 1024 
#define NRF_LOG_STR_PUSH_BUFFER_SIZE 128

// <e> NRF_LOG_USES_COLORS - If enabled then ANSI escape code for colors is prefixed to every string
//==========================================================
#define NRF_LOG_USES_COLORS 1

// <o> NRF_LOG_COLOR_DEFAULT  - ANSI escape code prefix. 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 
#define NRF_LOG_COLOR_DEFAULT 0

// <o> NRF_LOG_ERROR_COLOR  - ANSI escape code prefix.
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 
#define NRF_LOG_ERROR_COLOR 2

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 
#define NRF_LOG_WARNING_COLOR 4

// </e>

// <e> NRF_LOG_USES_TIMESTAMP - Enable timestamping
// <i> Function for getting the timestamp is provided by the user
//==========================================================
#define NRF_LOG_USES_TIMESTAMP 1

// <o> NRF_LOG_TIMESTAMP_DEFAULT_FREQUENCY - Default frequency of the timestamp (in Hz) or 0 to use app_timer frequency. 
#define NRF_LOG_TIMESTAMP_DEFAULT_FREQUENCY 0

// </e>


// <e> NRFX_GPIOTE_ENABLED - nrfx_gpiote - GPIOTE peripheral driver
//==========================================================
#define NRFX_GPIOTE_ENABLED 1

// <o> NRFX_GPIOTE_CONFIG_NUM_OF_LOW_POWER_EVENTS - Number of lower power input pins 
#define NRFX_GPIOTE_CONFIG_NUM_OF_LOW_POWER_EVENTS 2

// <o> NRFX_GPIOTE_CONFIG_IRQ_PRIORITY  - Interrupt priority
// <0=> 0 (highest) 
// <1=> 1 
// <2=> 2 
// <3=> 3 
// <4=> 4 
// <5=> 5 
// <6=> 6 
// <7=> 7 
#define NRFX_GPIOTE_CONFIG_IRQ_PRIORITY 6

// <e> NRFX_GPIOTE_CONFIG_LOG_ENABLED - Enables logging in the module.
//==========================================================
#define NRFX_GPIOTE_CONFIG_LOG_ENABLED 0

// <o> NRFX_GPIOTE_CONFIG_LOG_LEVEL  - Default Severity level 
// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 
#define NRFX_GPIOTE_CONFIG_LOG_LEVEL 3

// <o> NRFX_GPIOTE_CONFIG_INFO_COLOR  - ANSI escape code prefix. 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White
#define NRFX_GPIOTE_CONFIG_INFO_COLOR 0

// <o> NRFX_GPIOTE_CONFIG_DEBUG_COLOR  - ANSI escape code prefix. 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 
#define NRFX_GPIOTE_CONFIG_DEBUG_COLOR 0

// </e>

// </e>








#define NRFX_TWI_ENABLED 1

#define TWI_ENABLED 1
#define TWI0_ENABLED 1
#define TWI0_USE_EASY_DMA 0
#define TWI1_ENABLED 0


// <e> FDS_ENABLED - fds - Flash data storage module
//==========================================================
#define FDS_ENABLED 1


// <h> Pages - Virtual page settings
// <i> Configure the number of virtual pages to use and their size.
//==========================================================

// <o> FDS_VIRTUAL_PAGES - Number of virtual flash pages to use. 
// <i> One of the virtual pages is reserved by the system for garbage collection.
// <i> Therefore, the minimum is two virtual pages: one page to store data and one page to be used by the system for garbage collection.
// <i> The total amount of flash memory that is used by FDS amounts to @ref FDS_VIRTUAL_PAGES * @ref FDS_VIRTUAL_PAGE_SIZE * 4 bytes.
#define FDS_VIRTUAL_PAGES 3

// <o> FDS_VIRTUAL_PAGE_SIZE  - The size of a virtual flash page. 
// <i> Expressed in number of 4-byte words.
// <i> By default, a virtual page is the same size as a physical page.
// <i> The size of a virtual page must be a multiple of the size of a physical page.
// <1024=> 1024 
// <2048=> 2048 
#define FDS_VIRTUAL_PAGE_SIZE 1024

// <o> FDS_VIRTUAL_PAGES_RESERVED - The number of virtual flash pages that are used by other modules. 
// <i> FDS module stores its data in the last pages of the flash memory.
// <i> By setting this value, you can move flash end address used by the FDS.
// <i> As a result the reserved space can be used by other modules.
#define FDS_VIRTUAL_PAGES_RESERVED 0

// </h> 
//==========================================================


// <h> Backend - Backend configuration
// <i> Configure which nrf_fstorage backend is used by FDS to write to flash.
//==========================================================

// <o> FDS_BACKEND  - FDS flash backend.
// <i> NRF_FSTORAGE_SD uses the nrf_fstorage_sd backend implementation using the SoftDevice API. Use this if you have a SoftDevice present.
// <i> NRF_FSTORAGE_NVMC uses the nrf_fstorage_nvmc implementation. Use this setting if you don't use the SoftDevice.
// <1=> NRF_FSTORAGE_NVMC 
// <2=> NRF_FSTORAGE_SD
#define FDS_BACKEND 2

// </h>
//==========================================================


// <h> Queue - Queue settings
//==========================================================

// <o> FDS_OP_QUEUE_SIZE - Size of the internal queue. 
// <i> Increase this value if you frequently get synchronous FDS_ERR_NO_SPACE_IN_QUEUES errors.
#define FDS_OP_QUEUE_SIZE 4

// </h> 
//==========================================================


// <h> CRC - CRC functionality
//==========================================================
// <e> FDS_CRC_CHECK_ON_READ - Enable CRC checks.

// <i> Save a record's CRC when it is written to flash and check it when the record is opened.
// <i> Records with an incorrect CRC can still be 'seen' by the user using FDS functions, but they cannot be opened.
// <i> Additionally, they will not be garbage collected until they are deleted.
#define FDS_CRC_CHECK_ON_READ 0

// <o> FDS_CRC_CHECK_ON_WRITE  - Perform a CRC check on newly written records. 
// <i> Perform a CRC check on newly written records.
// <i> This setting can be used to make sure that the record data was not altered while being written to flash.
// <1=> Enabled 
// <0=> Disabled 
#define FDS_CRC_CHECK_ON_WRITE 0

// </e>
// </h> 
//==========================================================


// <h> Users - Number of users
//==========================================================

// <o> FDS_MAX_USERS - Maximum number of callbacks that can be registered. 
#define FDS_MAX_USERS 4

// </h> 
//==========================================================


// </e>


// <e> NRF_FSTORAGE_ENABLED - nrf_fstorage - Flash abstraction library
//==========================================================
#define NRF_FSTORAGE_ENABLED 1


// <h> nrf_fstorage - Common settings
// <i> Common settings to all fstorage implementations
//==========================================================

// <o> NRF_FSTORAGE_PARAM_CHECK_DISABLED  - Disable user input validation
// <i> If selected, use ASSERT to validate user input.
// <i> This effectively removes user input validation in production code.
// <i> Recommended setting: OFF, only enable this setting if size is a major concern.
#define NRF_FSTORAGE_PARAM_CHECK_DISABLED 0

// </h> 
//==========================================================


// <h> nrf_fstorage_sd - Implementation using the SoftDevice
// <i> Configuration options for the fstorage implementation using the SoftDevice
//==========================================================

// <o> NRF_FSTORAGE_SD_QUEUE_SIZE - Size of the internal queue of operations 
// <i> Increase this value if API calls frequently return the error @ref NRF_ERROR_NO_MEM.
#define NRF_FSTORAGE_SD_QUEUE_SIZE 4

// <o> NRF_FSTORAGE_SD_MAX_RETRIES - Maximum number of attempts at executing an operation when the SoftDevice is busy 
// <i> Increase this value if events frequently return the @ref NRF_ERROR_TIMEOUT error.
// <i> The SoftDevice might fail to schedule flash access due to high BLE activity.
#define NRF_FSTORAGE_SD_MAX_RETRIES 8

// <o> NRF_FSTORAGE_SD_MAX_WRITE_SIZE - Maximum number of bytes to be written to flash in a single operation 
// <i> This value must be a multiple of four.
// <i> Lowering this value can increase the chances of the SoftDevice being able to execute flash operations in between radio activity.
// <i> This value is bound by the maximum number of bytes that can be written to flash in a single call to @ref sd_flash_write.
// <i> That is 1024 bytes for nRF51 ICs and 4096 bytes for nRF52 ICs.
#define NRF_FSTORAGE_SD_MAX_WRITE_SIZE 4096

// </h> 
//==========================================================


// </e>


#define PEER_MANAGER_ENABLED 1


#endif /* __NEWCFG_H_ included */
