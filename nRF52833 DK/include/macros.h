#ifndef __MACROS_H
#define __MACROS_H

#define RET_ON_ERROR( err )                                 \
    do {                                                    \
        if( err ) {                                         \
            return err;                                     \
        }                                                   \
    } while (0)

#endif /* __MACROS_H included */
