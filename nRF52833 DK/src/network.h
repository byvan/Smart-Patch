/**
 * @file network.h
 * @brief      API for the TF network.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       December 2020
 */

#ifndef __NETWORK_H
#define __NETWORK_H

/**
 * @brief      Initialises the TF Lite Micro framework.
 *
 * @return     { description_of_the_return_value }
 */
uint32_t Net_Init(void);

#endif /* __NETWORK_H included */
