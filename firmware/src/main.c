/**
 * @file main.c
 * @brief      Smart Patch nRF52833 application firmware.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       October 2020
 */

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

#include "app_error.h"
#include "app_timer.h"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_twi.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_pwr_mgmt.h"

#include "macros.h"
#include "sp_board.h"
#include "sp_sensors.h"
#include "sp_system.h"

/**
 * @defgroup   SP_FW Smart Patch Firmware
 * @{
 */

/**
 * @defgroup   main_ic Main Application internal components
 * @{
 */

/* Private defines -----------------------------------------------------------*/

/* Private macros ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/**
 * @brief      Wrapper function for the timer callback.
 *
 * @param      pContext  The context to be passed to the callback.
 */
static void cb_tim_wrapper(void* pContext);

/**
 * @brief      Initialises the peripherals of the Smart Patch.
 *
 * @return     any error code the peripherals might return.
 */
static uint32_t peripherals_init(void);

/**
 * @brief      Wrapper to start the system timer.
 *
 * @param[in]  interval  The timer interval in ticks.
 *
 * @return     the error from #app_timer_start.
 */
static uint32_t timer_start(const uint32_t interval);

/**
 * @brief      Wrapper to stop the system timer.
 *
 * @return     the error from #app_timer_stop.
 */
static uint32_t timer_stop(void);

/* Private variables ---------------------------------------------------------*/

/*! I2C handler */
static const nrf_drv_twi_t hi2c = NRF_DRV_TWI_INSTANCE(I2C_MASTER_INST);
/*! I2C initialisation config */
static const nrf_drv_twi_config_t i2c_cfg = {
    .scl                = I2C_MASTER_SCL,
    .sda                = I2C_MASTER_SDA,
    .frequency          = NRF_DRV_TWI_FREQ_400K,
    // .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
    .interrupt_priority = APP_IRQ_PRIORITY_LOWEST,
    .clear_bus_init     = false
};

/*! Timer handler */
APP_TIMER_DEF(htim);
/*! Timer callback pointer to be set during config */
static app_timer_timeout_handler_t cb_tim = NULL;
/*! Timer interval in ticks */
// static uint32_t tim_ticks = (uint32_t) -1;

/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/

int main(void)
{
    APP_ERROR_CHECK( peripherals_init() );

    // APP_ERROR_CHECK( SP_System_Init(&hi2c, &tim_ticks, &cb_tim) );
    APP_ERROR_CHECK( SP_System_Init(&hi2c, &cb_tim, timer_start, timer_stop) );
    NRF_LOG_INFO("Smart Patch System initialised!");

    // APP_ERROR_CHECK( app_timer_start(htim, tim_ticks, NULL) );

    bool actionPending;
    while( 1 ) {
        actionPending = false;

        APP_ERROR_CHECK( SP_System_Process(&actionPending) );
        APP_ERROR_CHECK( SP_Sensors_Process(&actionPending) );

        // actionPending |= SP_Sensors_Process(status_counter, progress);
        actionPending |= NRF_LOG_PROCESS();

        if( !actionPending ) {
            nrf_pwr_mgmt_run();
        }
    }



#if 0

    bool actionPending;

    while(1) {

        actionPending = false;

        actionPending |= SP_System_Process();

        actionPending |= SP_Sensors_Process(status_counter, progress);

        actionPending |= NRF_LOG_PROCESS();

        if( !actionPending ) {
            nrf_pwr_mgmt_run();
        }
    }

#endif

    return 0;
}

/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/

static void cb_tim_wrapper(void* pContext)
{
    if( cb_tim ) {
        cb_tim(pContext);
    }
}

static uint32_t peripherals_init(void)
{
    /** Initialise GPIO peripheral */
    RET_ON_ERROR( nrf_drv_gpiote_init() );

    /** Initialise log */
    RET_ON_ERROR( NRF_LOG_INIT(app_timer_cnt_get) );
    NRF_LOG_DEFAULT_BACKENDS_INIT();
    /** @todo enable NRF_LOG_DEFERRED */
    NRF_LOG_DEBUG("LOG initialised!");

    /** Initialise the I2C module */
    RET_ON_ERROR( nrf_drv_twi_init(&hi2c, &i2c_cfg, NULL, NULL) );
    nrf_drv_twi_enable(&hi2c);
    NRF_LOG_DEBUG("I2C initialised!");

    /** Initialise timer */
    RET_ON_ERROR( app_timer_init() );
    RET_ON_ERROR( app_timer_create(&htim, APP_TIMER_MODE_REPEATED, cb_tim_wrapper) );
    NRF_LOG_DEBUG("Timer initialised!");

    NRF_LOG_INFO("Peripherals initialised!");

    return 0;
}

static uint32_t timer_start(const uint32_t interval)
{
    return app_timer_start(htim, interval, NULL);
}

static uint32_t timer_stop(void)
{
    return app_timer_stop(htim);
}


#if 0

uint32_t sd_temp_get(int32_t * p_temp);

static void timer_cb_handler(void* p_context)
{
    

    // int32_t temperature = 0;   
    // sd_temp_get(&temperature);
    // int8_t temp = temperature/4;
    // NRF_LOG_INFO("Temperature: %u", temp);
    ble_update((uint8_t*) &EWS_Data.HR, 4, 1);
    
    // our_temperature_characteristic_update(&m_our_service, &temperature);

    // ble_updateBAS(cnt++);

}
#endif
