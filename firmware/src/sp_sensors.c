/**
 * @file sp_sensors.c
 * @brief      Implementation of the sensor suite of the Smart Patch.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       October 2020
 * 
 * @todo disable pin reset for this to work
 * @todo enable everything with pin 18 for this to work
 */

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdlib.h>

#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "nrf_log.h"

#include "MAX32664.h"
#include "macros.h"
#include "sp_board.h"
#include "sp_sensors.h"

/**
 * @addtogroup   SP_Sensors
 * @{
 */

/**
 * @defgroup   SP_Sensors_ic Internal Components
 * @{
 */

/* Private defines -----------------------------------------------------------*/

/*! Number of samples to store in internal buffer */
#define BUFFER_SAMPLE_SIZE          128

/* Private macros ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/**
 * @brief      Struct to handle the sensor suite.
 */
typedef struct {
    const nrf_drv_twi_t*    hi2c;           /*!< Pointer to the I2C handler */

    bool                    MAX_Configured; /*!< Flag indicating if the MAX32664 has been configured */
    bool                    MAX_Upgrade;    /*!< Flag, if a DFU is required */
} SensorSuite_t;

/* Private function prototypes -----------------------------------------------*/

/**
 * @brief      Initialises the peripherals for the MAX32664 device.
 *
 * @return     Any error that comes up during initialisation.
 */
static uint32_t max_init(void);

/**
 * @brief      Sets the mode of the MFIO pin.
 *
 * @param[in]  isInput  Indicates if the pin should be used as input.
 *
 * @return     Any error that comes up during initialisation.
 */
static inline uint32_t max_mfio_mode(bool isInput);

/**
 * @brief      Event handler for handling interrupts of the MFIO pin.
 *
 * @param[in]  pin     The pin that caused the interrupt.
 * @param[in]  action  The action that caused the interrupt.
 */
static void on_mfio_evt(nrfx_gpiote_pin_t pin, nrf_gpiote_polarity_t action);

/* Private variables ---------------------------------------------------------*/

/*! Internal device handler */
static SensorSuite_t Sensors = {0};





/*! Internal I2C handler */
// static const nrf_drv_twi_t* hi2c;
/*! Internal data buffer */
static uint8_t dataBuffer[1 + BUFFER_SAMPLE_SIZE*23] = {0};

static uint8_t* pDFUBuffer = 0;
static uint8_t DFUPageCount = 0;
static uint16_t DFUPageSize = 0;
static uint16_t DFUCounter = 0;


static uint8_t pageCounter = 0;

/**
 * @}
 */

/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/

uint32_t SP_Sensors_Configure(void)
{
    // RET_ON_ERROR(SP_Sensors_Start());
    /* @todo remove */
    // RET_ON_ERROR( MAX32664_SetAGC(MAX32664_DISABLED) );
    RET_ON_ERROR( MAX32664_SetAGC(MAX32664_ENABLED) );

    // nrfx_gpiote_out_clear(SP_PIN_BOOST_EN);

    uint8_t wData[2] = {0x0C, 0x1F};
    RET_ON_ERROR( MAX32664_WriteSensorRegister(wData) );

    wData[0] = 0x0D;
    RET_ON_ERROR( MAX32664_WriteSensorRegister(wData) );

    return 0;
}

uint32_t SP_Sensors_DFU_FlashPage(const uint8_t* pData, const uint16_t size)
{
    uint32_t err_code = 0;


    // if( pData == NULL ) {
    //     NRF_LOG_WARNING("Flashing page %u of %u", pageCounter, DFUPageCount);
    //     err_code = MAX32664_DFU_FlashPage(pDFUBuffer);
    //     if( err_code ) {
    //         free(pDFUBuffer);
    //         return err_code;
    //     }

    //     return err_code;
    // }



    if( (DFUCounter + size) > DFUPageSize ) {
        NRF_LOG_ERROR("Too much data! (tot: %u, size: %u)", (DFUCounter + size), DFUPageSize);
        free(pDFUBuffer);
        return -1;
    }

    memcpy(&pDFUBuffer[DFUCounter], pData, size);
    DFUCounter += size;

    if( DFUCounter == DFUPageSize ) {
        pageCounter++;
        NRF_LOG_WARNING("Flashing page %u of %u", pageCounter, DFUPageCount);
        err_code = MAX32664_DFU_FlashPage(pDFUBuffer);
        if( err_code ) {
            free(pDFUBuffer);
            return err_code;
        }

        DFUCounter = 0;
    }

    if( pageCounter == DFUPageCount ) {
        free(pDFUBuffer);
        NRF_LOG_WARNING("Firmware flashed: Rebooting...");

        err_code = MAX32664_SetDeviceMode(MAX32664_APPLICATION_MODE);
        if( err_code )  return err_code;

        uint8_t fw_version[3] = {0};
        err_code = MAX32664_GetFWVersion(fw_version, true);
        if( err_code )  return err_code;

        NRF_LOG_DEBUG("FW version: %u.%u.%u", fw_version[0], fw_version[1], fw_version[2]);
    }



    // NRF_LOG_WARNING("Flashing segment %u/%u", pageCounter, DFUPageCount);
    // pageCounter++;
    // RET_ON_ERROR( MAX32664_DFU_FlashPage(pData) );

    // if( pageCounter == 2*DFUPageCount ) {
    //     NRF_LOG_WARNING("Firmware flashed (%u): Rebooting...", pageCounter);
    //     RET_ON_ERROR( MAX32664_SetDeviceMode(MAX32664_APPLICATION_MODE) );

    //     uint8_t fw_version[3] = {0};
    //     RET_ON_ERROR( MAX32664_GetFWVersion(fw_version, true) );

    //     NRF_LOG_DEBUG("FW version: %u.%u.%u", fw_version[0], fw_version[1], fw_version[2]);
    // }

    return 0;
}

uint32_t SP_Sensors_DFU_Init(uint16_t* const pSize)
{
    NRF_LOG_INFO("Setting MAX32664 into Bootloader mode!");

    /*RET_ON_ERROR(*/ MAX32664_SetDeviceMode(MAX32664_BOOTLOADER_MODE) /*)*/;

    MAX32664_DeviceMode_t mode = MAX32664_GetDeviceMode();
    NRF_LOG_INFO("MAX2 in Mode %u", mode);

    uint8_t bl_version[3] = {0};
    RET_ON_ERROR( MAX32664_GetBLVersion(bl_version) );
    RET_ON_ERROR( MAX32664_GetPageSize(&DFUPageSize) );

    DFUPageSize = 8208;

    NRF_LOG_DEBUG("BL version: %u.%u.%u", bl_version[0], bl_version[1], bl_version[2]);
    NRF_LOG_DEBUG("Page Size: %u", DFUPageSize);

    pDFUBuffer = malloc(DFUPageSize);
    RET_ON_ERROR( (pDFUBuffer ? 0 : 23) );
    DFUCounter = 0;
    *pSize = DFUPageSize;
    pageCounter = 0;

    return 0;
}

uint32_t SP_Sensors_DFU_Prepare(const uint8_t* pData)
{
    /**
     * We expect the data to contain the following data:
     * data[0]      :   Number of pages to flash
     * data[1:11]   :   Initialisation vector bytes
     * data[12:27]  :   Authentication bytes
     */

    NRF_LOG_INFO("Preparing MAX32664 for DFU!");

    /* Set the number of pages to flash */
    RET_ON_ERROR( MAX32664_SetFlashPages(pData[0]) );
    DFUPageCount = pData[0];

    /* Set the initialisation vector bytes */
    RET_ON_ERROR( MAX32664_SetIVBytes(&pData[1]) );

    /* Set the authentication bytes */
    RET_ON_ERROR( MAX32664_SetAuthBytes(&pData[12]) );

    /* Erase memory */
    NRF_LOG_WARNING("Erasing MAX32664 application memory!");
    RET_ON_ERROR( MAX32664_EraseMemory() );

    return 0;
}

uint32_t SP_Sensors_Init(const nrf_drv_twi_t* pInterface)
{
    NRF_LOG_DEBUG("Initialising sensors.");

    Sensors.hi2c = pInterface;
    
    MAX32664_Config_t cfg = {
        .outputMode         = MAX32664_OUT_SENSORALG,
        .fifoThreshold      = 0x0F                      // for whatever reason, this cannot be changed??
    };

    RET_ON_ERROR( max_init() );

    if( !Sensors.MAX_Upgrade ) {
        RET_ON_ERROR( SP_Sensors_Stop() );
        RET_ON_ERROR( MAX32664_Configure(&cfg) );
        Sensors.MAX_Configured = true;
    }

    RET_ON_ERROR( max_mfio_mode(true) );

    return 0;
}

void SP_Sensors_ISR_Disable(void)
{
    nrf_drv_gpiote_in_event_disable(SP_PIN_MAX32664_MFIO);
}

void SP_Sensors_ISR_Enable(void)
{
    nrf_drv_gpiote_in_event_enable(SP_PIN_MAX32664_MFIO, true);
}

uint32_t SP_Sensors_Process(bool* const pending)
{
    // if( ! MAX32664_InterruptPending(true) ) {
    //     return 0;
    // }

    uint8_t nPending = 0;
    uint8_t nProcessing = 0;
    uint32_t err_code = 0;

    /* Determine number of samples to pull from the sensor */
    MAX32664_GetFifoCount(&nPending);
    nProcessing = ((nPending > BUFFER_SAMPLE_SIZE) ? BUFFER_SAMPLE_SIZE : nPending );
    
    if( nProcessing )
        NRF_LOG_INFO("%u/%u pending samples", nProcessing, nPending);

    // SP_Sensors_ISR_Disable();

    /* Transfer data */
    err_code = MAX32664_GetSensorData(dataBuffer, nProcessing);
    if( err_code ) {
        NRF_LOG_ERROR("Failed to read sensor data! (0x%08X)", err_code);
        NRF_LOG_ERROR("Repeated: %lu", MAX32664_GetStatus());

        SP_Sensors_Stop();
        SP_Sensors_Start();
        SP_Sensors_Configure();
        // APP_ERROR_CHECK(err_code);
    }

    // SP_Sensors_ISR_Enable();

    /* @todo Do something with data */

    /* Clear interrupt depending on remaining samples */
    nPending -= nProcessing;
    // MAX32664_InterruptPending(!nPending);

    if( nPending ) {
        NRF_LOG_WARNING("%u still pending", nPending);
        *pending = true;
    }

    // progress[0] += nProcessing;
    // for(uint32_t i = 0; i < nProcessing; ++i) {
    //     uint8_t status = pSensorData[1 + 23*i + 12];
    //     pCounter[status]++;
    //     uint16_t pHR = (pSensorData[1 + 23*i + 14] << 8) | pSensorData[1 + 23*i + 15];
    //     progress[1] += pHR;
    // }

    // sprintf(msg, "%lu, %lu, %lu, %lu, %lu, %lu, %lu", status_count[0], status_count[1], status_count[2], status_count[3], status_count[4], status_count[5], status_count[6]);
    // NRF_LOG_INFO("%s", msg);

    return err_code;//MAX32664_InterruptPending(false);

#if 0
    if( ! MAX32664_InterruptPending(false) ) {
        return 0;
    }

    uint8_t nPending = 0;
    uint8_t nProcessing = 0;
    uint32_t err_code = 0;

    /* Determine number of samples to pull from the sensor */
    MAX32664_GetFifoCount(&nPending);
    nProcessing = ((nPending > SENSOR_DATA_BUFFER_SIZE) ? SENSOR_DATA_BUFFER_SIZE : nPending );
    // NRF_LOG_INFO("%u/%u pending samples", nProcessing, nPending);

    // nrf_delay_ms(20);

    SP_Sensors_ISR_Disable();

    /* Transfer data */
    err_code = MAX32664_GetSensorData(pSensorData, nProcessing);
    if( err_code ) {
        NRF_LOG_ERROR("Failed to read sensor data! (0x%08X)", err_code);
        NRF_LOG_ERROR("Repeated: %lu", MAX32664_GetStatus());

        SP_Sensors_Stop();
        SP_Sensors_Start();
        SP_Sensors_Configure();
        // APP_ERROR_CHECK(err_code);
    }

    SP_Sensors_ISR_Enable();

    /* @todo Do something with data */


    /* Clear interrupt depending on remaining samples */
    nPending -= nProcessing;
    // MAX32664_InterruptPending(!nPending);

    if( nPending ) {
        NRF_LOG_WARNING("%u still pending", nPending);
    }

    progress[0] += nProcessing;
    for(uint32_t i = 0; i < nProcessing; ++i) {
        uint8_t status = pSensorData[1 + 23*i + 12];
        pCounter[status]++;
        uint16_t pHR = (pSensorData[1 + 23*i + 14] << 8) | pSensorData[1 + 23*i + 15];
        progress[1] += pHR;
    }

    // sprintf(msg, "%lu, %lu, %lu, %lu, %lu, %lu, %lu", status_count[0], status_count[1], status_count[2], status_count[3], status_count[4], status_count[5], status_count[6]);
    // NRF_LOG_INFO("%s", msg);

    return nPending;//MAX32664_InterruptPending(false);

#endif
}

uint32_t SP_Sensors_Start(void)
{
    RET_ON_ERROR( MAX32664_SetSensor(MAX32664_ENABLED) );
    RET_ON_ERROR( MAX32664_SetAlgorithm(MAX32664_ALGO_ESTIMATION) );

    return 0;
}

uint32_t SP_Sensors_Stop(void)
{
    RET_ON_ERROR( MAX32664_SetSensor(MAX32664_DISABLED) );
    RET_ON_ERROR( MAX32664_SetAlgorithm(MAX32664_ALGO_DISABLED) );

    return 0;
}

/* Private functions ---------------------------------------------------------*/

static uint32_t max_init(void)
{
    /* Configure nRST and MFIO pins. */
    nrf_drv_gpiote_out_config_t pin_cfg = GPIOTE_CONFIG_OUT_SIMPLE(true);

    RET_ON_ERROR( nrf_drv_gpiote_out_init(SP_PIN_MAX32664_nRST, &pin_cfg) );
    RET_ON_ERROR( nrf_drv_gpiote_out_init(SP_PIN_MAX32664_MFIO, &pin_cfg) );
    // RET_ON_ERROR( nrf_drv_gpiote_out_init(SP_PIN_BOOST_EN, &pin_cfg) );

    /* Initialise the MAX32664 and check if a DFU is necessary */
    uint32_t err_code = MAX32664_Init(&Sensors.MAX_Upgrade);

    if( err_code == 0xF001 ) {
        Sensors.MAX_Upgrade = true;
    }

    /* Read the bootloader version */
    uint8_t bl_version[3] = {0};
    RET_ON_ERROR( MAX32664_GetBLVersion(bl_version) );
    NRF_LOG_DEBUG("\tBL version: %u.%u.%u", bl_version[0], bl_version[1], bl_version[2]);

    if( Sensors.MAX_Upgrade ) {
        NRF_LOG_WARNING("\tDevice needs to be upgraded!");
    }
    else {
        uint8_t fw_version[3] = {0};
        RET_ON_ERROR( MAX32664_GetFWVersion(fw_version, true) );
        NRF_LOG_DEBUG("\tFW version: %u.%u.%u", fw_version[0], fw_version[1], fw_version[2]);
    }

    return 0;
}

static inline uint32_t max_mfio_mode(bool isInput)
{
    if( isInput ) {
        if( nrf_drv_gpiote_in_is_set(SP_PIN_MAX32664_MFIO) ) {
            return 0;
        }
        else {
            nrf_drv_gpiote_out_uninit(SP_PIN_MAX32664_MFIO);
            nrf_drv_gpiote_in_config_t pin_cfg = GPIOTE_CONFIG_IN_SENSE_LOTOHI(true);
            // pin_cfg.pull = NRF_GPIO_PIN_PULLUP;
            return nrf_drv_gpiote_in_init(SP_PIN_MAX32664_MFIO, &pin_cfg, on_mfio_evt);
        }
    }
    else {
        if( nrf_drv_gpiote_in_is_set(SP_PIN_MAX32664_MFIO) ) {
            SP_Sensors_ISR_Disable();
            nrf_drv_gpiote_in_uninit(SP_PIN_MAX32664_MFIO);
            nrf_drv_gpiote_out_config_t pin_cfg = GPIOTE_CONFIG_OUT_SIMPLE(true);
            return nrf_drv_gpiote_out_init(SP_PIN_MAX32664_MFIO, &pin_cfg);
        }
        else {
            return 0;
        }
    }
}

static void on_mfio_evt(nrfx_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    /* De-mux event source */
    if( (pin == SP_PIN_MAX32664_MFIO) && (action == NRF_GPIOTE_POLARITY_LOTOHI) ) {
        MAX32664_InterruptReceived();
        NRF_LOG_ERROR("MAX ISR");
    }
}

/**
 * @addtogroup   SP_Sensors
 * @{
 */

/**
 * @defgroup   SP_Sensors_ec External Components
 * @{
 */

uint32_t MAX32664_Reset(const MAX32664_DeviceMode_t mode)
{
    uint32_t err_code;

    /* Boot in application mode */
    if( mode == MAX32664_APPLICATION_MODE ) {
        /* Set MFIO pin in to output mode */
        RET_ON_ERROR( max_mfio_mode(false) );

        /* Reset the device */
        nrfx_gpiote_out_clear(SP_PIN_MAX32664_nRST);
        nrf_delay_ms(1);
        nrfx_gpiote_out_set(SP_PIN_MAX32664_MFIO);
        nrf_delay_ms(9);
        nrfx_gpiote_out_set(SP_PIN_MAX32664_nRST);
        nrf_delay_ms(50);

        /* Device is now in application mode */
        RET_ON_ERROR( max_mfio_mode(true) );
        nrf_delay_ms(1000);
        err_code = 0;
    }
    /* Boot in bootloader mode */
    else if( mode == MAX32664_BOOTLOADER_MODE ) {
        /* Set MFIO pin in to output mode */
        RET_ON_ERROR( max_mfio_mode(false) );

        /* Reset the device */
        nrfx_gpiote_out_clear(SP_PIN_MAX32664_nRST);
        nrf_delay_ms(1);
        nrfx_gpiote_out_clear(SP_PIN_MAX32664_MFIO);
        nrf_delay_ms(9);
        nrfx_gpiote_out_set(SP_PIN_MAX32664_nRST);
        nrf_delay_ms(50);

        /* Device is now in bootloader mode */
        err_code = 0;
    }
    else {
        err_code = -1;
    }

    return err_code;
}

uint32_t MAX32664_ReadData(const uint32_t size, uint8_t* pData)
{
    return nrf_drv_twi_rx(Sensors.hi2c, MAX32664_ADDRESS, pData, size);
}

void MAX32664_Wait(uint32_t ms)
{
    nrf_delay_ms(ms);
}

uint32_t MAX32664_WriteData(const uint8_t* pData, const uint32_t size, const bool noStop)
{
    return nrf_drv_twi_tx(Sensors.hi2c, MAX32664_ADDRESS, pData, size, noStop);
}

/**
 * @}
 */

/**
 * @}
 */
