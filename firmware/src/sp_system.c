/**
 * @file sp_system.h
 * @brief      Smart Patch general system implementation.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       November 2020
 */

/* Includes ------------------------------------------------------------------*/
#include "app_timer.h"
#include "nrf_log.h"

#include "macros.h"
#include "MAX32664.h"
#include "sp_ble.h"
#include "sp_board.h"
#include "sp_sensors.h"
#include "sp_system.h"

/**
 * @addtogroup   SP_System
 * @{
 */

/**
 * @defgroup   SP_System_ic Internal Components
 * @{
 */

/* Private defines -----------------------------------------------------------*/

/*! Length of the queue for scheduling operations */
#define OPERATION_QUEUE_LENGTH          4
/*! Timer interval in ticks */
#define TIMER_INTERVAL_TICKS            APP_TIMER_TICKS(DATA_INTERVAL_MS)

/*! Length of the sensor DFU data buffer */
#define SENSORDFU_BUFFER_LEN            (240)

/*! Macro to beautify access to the sequence number of the sensor DFU process */
#define SENSORDFU_SEQUENCE              SystemState.rSensorDFU[0]
/*! Macro to beautify access to the segment number of the sensor DFU process */
#define SENSORDFU_SEGMENT               SystemState.rSensorDFU[1]
/*! Macro to beautify access to the total segment number of the sensor DFU process */
#define SENSORDFU_SEGMENTLEN            SystemState.rSensorDFU[2]
/*! Macro to beautify access to the data part of the sensor DFU process */
#define SENSORDFU_DATA                  SystemState.rSensorDFU[3]

/* Private macros ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/**
 * @brief      System operation enumerators.
 */
typedef enum {
    SP_OPERATION_NONE       = 0,    /*!< No operation */
    SP_OPERATION_SWITCH_OFF,        /*!< Sensor suite shoud switch off */
    SP_OPERATION_SWITCH_ON,         /*!< Sensor suite shoud switch on */
    SP_OPERATION_ACQ_OFF,           /*!< Acquisition system shoud switch off */
    SP_OPERATION_ACQ_ON,            /*!< Acquisition system shoud switch on */
    SP_OPERATION_FWUPG_INIT,        /*!< Initiate firmware upgrade */
    SP_OPERATION_DFU_PROCESS,       /*!< Process the DFU */
} SP_Operation_t;

/**
 * @brief      Acquistion state enumerators.
 */
typedef enum {
    SP_ACQ_OFF  = 0,    /*!< Acquisition system in off state */
    SP_ACQ_ON   = 1     /*!< Acquisition system in on state */
} SP_AcqState_t;

/**
 * @brief      DFU state enumerator.
 */
typedef enum {
    SP_DFU_NONE = 0,    /*!< No DFU in progress */
    SP_DFU_INIT,        /*!< DFU initialised */
    SP_DFU_AUTH,        /*!< DFU authenticated */
} SP_DFUState_t;

/**
 * @brief      Internal sensor state enumerators.
 */
typedef enum {
    SENSOR_STATE_OFF,
    SENSOR_STATE_IDLE,
    SENSOR_STATE_ON,
    SENSOR_STATE_DFU
} SP_SensorState_t;

/**
 * @brief      Struct to manage the SP System state.
 *
 * @todo       Use generatic queue.
 */
typedef struct {
    SP_SensorState_t    SensorState;                            /*!< Sensor state */
    SP_AcqState_t       AcqState;                               /*!< Acquisition system state */
    SP_DFUState_t       DFUState;                               /*!< DFU state */
    SP_Operation_t      OpQueue[OPERATION_QUEUE_LENGTH];        /*!< Queue for scheduled operations */
    SP_Operation_t*     pOpQueueRead;                           /*!< Queue read pointer */
    SP_Operation_t*     pOpQueueWrite;                          /*!< Queue write pointer */

    uint8_t             DeviceConfig;                           /*!< Device configuration to be set via BLE */
    uint8_t             rSensorDFU[5];                          /*!< Sensor DFU register to be read via BLE */

    uint32_t            (*pTimStart)(uint32_t);                 /*!< Handler to start the system timer */
    uint32_t            (*pTimStop)(void);                      /*!< Handler to stop the system timer */
} SP_System_t;

/* Private function prototypes -----------------------------------------------*/

/**
 * @brief      Switches the acquisition system off.
 *
 * @return     any error that comes up during the operation.
 */
static uint32_t acquisition_off(void);

/**
 * @brief      Switches the acquisition system on.
 *
 * @return     any error that comes up during the operation.
 */
static uint32_t acquisition_on(void);

/**
 * @brief      Schedules state transitions of the acquisition system.
 *
 * @param[in]  state  The enabling state of the acquisition system.
 *
 * @return     any error that comes up during the operation.
 */
static uint32_t acquisition_state_schedule(uint8_t state);

/**
 * @brief      Callback function that is called, when the device register is
 *             changed by the SD.
 */
static void cb_deviceregister_chaged(void);

/**
 * @brief      Callback function that is called, when DFU data is changed by the
 *             SD.
 *
 * @param[in]  len   The length of the received data.
 */
static void cb_dfu(uint16_t len);

/**
 * @brief      Callback called by #cb_dfu when device is in state #SP_DFU_AUTH.
 *
 * @param[in]  len   The length of the received data.
 */
static void cb_dfu_auth(uint16_t len);

/**
 * @brief      Callback called by #cb_dfu when device is in state #SP_DFU_INIT.
 *
 * @param[in]  len   The length of the received data.
 */
static void cb_dfu_init(uint16_t len);

/**
 * @brief      Function for handling the system timer timeout.
 *
 * @param[in]  pContext  Pointer used for passing some arbitrary information
 *                       (context) from the app_start_timer() call to the
 *                       timeout handler.
 */
static void cb_timer(void* pContext);

/**
 * @brief      Process the DFU.
 *
 * @return     Any error that comes up during the process.
 */
static uint32_t dfu_process(void);

/**
 * @brief      Executes the firmware upgrade.
 *
 * @return     Any error code that comes up during the process.
 */
static uint32_t fw_upgrade(void);

/**
 * @brief      Schedules a FW Upgrade for the MAX32664 over bluetooth.
 *
 * @return     Any error code that comes up during the process.
 */
static uint32_t fw_upgrade_schedule(void);

/**
 * @brief      Switches the sensor suite off.
 *
 * @return     any error that comes up during the operation.
 */
static uint32_t sensors_off(void);

/**
 * @brief      Switches the sensor suite on.
 *
 * @return     any error that comes up during the operation.
 */
static uint32_t sensors_on(void);

/**
 * @brief      Schedules state transitions of the sensor suite.
 *
 * @param[in]  state  The enabling state of the sensor suite.
 *
 * @return     any error that comes up during the operation.
 */
static uint32_t sensor_state_schedule(uint8_t state);

/**
 * @brief      Clears the operation queue.
 */
static void queue_clear(void);

/**
 * @brief      Queue an operation in the system queue.
 *
 * @param[in]  op    The operation to be queued.
 *
 * @warning    if the queue is full, the system should assert an error.
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t queue_operation(const SP_Operation_t op);

/* Private variables ---------------------------------------------------------*/

/*! Internal system variables */
static SP_System_t SystemState = {0};
/*! BLE Buffer for DFU of the MAX32664 */
static uint8_t bDFU[1 + SENSORDFU_BUFFER_LEN] = {0};
static uint16_t DFUSize = 0;

/**
 * @}
 */

/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/

uint32_t SP_System_Init(const nrf_drv_twi_t* pInterface, app_timer_timeout_handler_t* const pCallback, uint32_t (*pTimStart)(uint32_t), uint32_t (*pTimStop)(void))
{
    NRF_LOG_DEBUG("Initialising Smart Patch System");

    /* Initialise queue buffers */
    queue_clear();

    /* Initialise the BMS. */
    // RET_ON_ERROR( SP_BMS_Init(pInterface) );

    /* Initialise the sensor suite. */
    RET_ON_ERROR( SP_Sensors_Init(pInterface) );

    SystemState.SensorState = SENSOR_STATE_IDLE;

    /* Initialise the BLE interface */
    SP_BLEcfg_t cfg = {
        .pDevice_cfg = &SystemState.DeviceConfig,
        .cb_device = cb_deviceregister_chaged,
        .pDFU = bDFU,
        .sDFU = SENSORDFU_BUFFER_LEN,
        .cb_dfu = cb_dfu,
        .pDFUAck = SystemState.rSensorDFU
    };
    RET_ON_ERROR( SP_BLE_Init(&cfg) );

    /* Configure timer settings */
    SystemState.pTimStart = pTimStart;
    SystemState.pTimStop = pTimStop;
    *pCallback = cb_timer;

    /* reset Sensor DFU register */
    SENSORDFU_SEQUENCE = -1;
    SENSORDFU_SEGMENT = -1;
    SENSORDFU_SEGMENTLEN = -1;
    SENSORDFU_DATA = -1;

    return 0;
}

uint32_t SP_System_Process(bool* const pending)
{
    if( *SystemState.pOpQueueRead == SP_OPERATION_NONE ) {
        return 0;
    }

    uint32_t err_code = 0;

    switch( *SystemState.pOpQueueRead ) {
        case SP_OPERATION_SWITCH_OFF:
            err_code = sensors_off();
            break;

        case SP_OPERATION_SWITCH_ON:
            err_code = sensors_on();
            break;

        case SP_OPERATION_ACQ_OFF:
            err_code = acquisition_off();
            break;

        case SP_OPERATION_ACQ_ON:
            err_code = acquisition_on();
            break;

        case SP_OPERATION_FWUPG_INIT:
            err_code = fw_upgrade();
            break;

        case SP_OPERATION_DFU_PROCESS:
            err_code = dfu_process();
            break;

        default:
            // No implementation needed.
            break;
    }

    RET_ON_ERROR( err_code );

    /* advance pointer */
    *SystemState.pOpQueueRead = SP_OPERATION_NONE;
    SystemState.pOpQueueRead++;
    if( SystemState.pOpQueueRead == &SystemState.OpQueue[OPERATION_QUEUE_LENGTH] ) {
        SystemState.pOpQueueRead = SystemState.OpQueue;
    }

    *pending = (*pending | ( *SystemState.pOpQueueRead != SP_OPERATION_NONE ));
    return err_code;
}

/* Private functions ---------------------------------------------------------*/

static uint32_t acquisition_off(void)
{
    NRF_LOG_DEBUG("Disabling acquisition");

    RET_ON_ERROR( SystemState.pTimStop() );

    SystemState.AcqState = SP_ACQ_OFF;
    return 0;
}

static uint32_t acquisition_on(void)
{
    NRF_LOG_DEBUG("Enabling acquisition");

    RET_ON_ERROR( SystemState.pTimStart(TIMER_INTERVAL_TICKS) );

    SystemState.AcqState = SP_ACQ_ON;
    return 0;
}

static uint32_t acquisition_state_schedule(uint8_t state)
{
    state = (state >> 1);

    /* Acquisition should be off */
    if( !state && SystemState.AcqState ) {
        return queue_operation(SP_OPERATION_ACQ_OFF);
    }
    /* Acquisition should be on */
    else if( state && !SystemState.AcqState ) {
        return queue_operation(SP_OPERATION_ACQ_ON);
    }

    return state != SystemState.AcqState;
}

static void cb_deviceregister_chaged(void)
{
    uint8_t reg = SystemState.DeviceConfig;

    if( reg & cfgUPGRADE_MASK ) {
        SystemState.DeviceConfig = 0;
        APP_ERROR_CHECK( fw_upgrade_schedule() );
        return;
    }

    APP_ERROR_CHECK( sensor_state_schedule(reg & cfgSENSORS_ON_MASK) );
    APP_ERROR_CHECK( acquisition_state_schedule(reg & cfgACQUISITION_ON_MASK) );
}

static void cb_dfu(uint16_t len)
{
    switch( SystemState.DFUState ) {
        case SP_DFU_NONE:
            // Do nothing, device is not in DFU mode!
            break;

        case SP_DFU_INIT:
            cb_dfu_init(len);
            break;

        case SP_DFU_AUTH:
            cb_dfu_auth(len);
            break;

        default:
            NRF_LOG_WARNING("I don't know what to do with this information!");
    }

    // if( SystemState.DFUState == SP_DFU_INIT ) {
    //     cb_dfu_init(len);
    // }

    // NRF_LOG_WARNING("Received %u bytes!", len);
}

static void cb_dfu_auth(uint16_t len)
{
    /**
     * We expect 137 bytes of data:
     * data[0]      :   Sequence number (0x01 ... )
     * data[1:136]  :   Application firmware
     */
    if( bDFU[0] != SENSORDFU_SEQUENCE) {
        NRF_LOG_ERROR("Wrong data! %u, %u (%u:%u)", len, bDFU[0], SENSORDFU_SEQUENCE, SENSORDFU_SEGMENT);
        return;
    }

    DFUSize = len;

    if( queue_operation(SP_OPERATION_DFU_PROCESS) ) {
        NRF_LOG_ERROR("Could not process DFU!");
    }
}

static void cb_dfu_init(uint16_t len)
{
    /**
     * We expect the data to contain the following data:
     * data[0]      :   Sequence identifier (0x00)
     * data[1]      :   Number of pages to flash
     * data[2:12]   :   Initialisation vector bytes
     * data[13:28]  :   Authentication bytes
     */
    if( (len != 29) || (bDFU[0] != SENSORDFU_SEQUENCE) ) {
        NRF_LOG_ERROR("Wrong data!");
        return;
    }

    if( queue_operation(SP_OPERATION_DFU_PROCESS) ) {
        NRF_LOG_ERROR("Could not process DFU!");
    }
}

static void cb_timer(void* pContext)
{
    NRF_LOG_DEBUG("Hello from Callback %u", SystemState.DeviceConfig);
}

static uint32_t dfu_process(void)
{
    /* @todo another safeguard? don't think so.... */
    if( SystemState.DFUState == SP_DFU_INIT ) {
        /* Data contains initialisation bytes */
        RET_ON_ERROR( SP_Sensors_DFU_Prepare(&bDFU[1]) );

        SystemState.DFUState = SP_DFU_AUTH;
        SENSORDFU_SEQUENCE = 1;
    }
    else if( SystemState.DFUState == SP_DFU_AUTH ) {
        NRF_LOG_DEBUG("Received %u bytes. (%u:%u)", DFUSize - 1, SENSORDFU_SEQUENCE, SENSORDFU_SEGMENT);

        /* Data contains a page to be flashed */
        RET_ON_ERROR( SP_Sensors_DFU_FlashPage(&bDFU[1], DFUSize - 1) );
        SENSORDFU_SEGMENT++;

        if( SENSORDFU_SEGMENT == SENSORDFU_SEGMENTLEN ) {
            SENSORDFU_SEGMENT = 0;
            SENSORDFU_SEQUENCE++;

            // RET_ON_ERROR( SP_Sensors_DFU_FlashPage(NULL, 8192) );
        }
    }
    else {
        NRF_LOG_ERROR("Unhandled state! %u-%u", SENSORDFU_SEQUENCE, SENSORDFU_SEGMENT);
        return -1;
    }

    return 0;
}

static uint32_t fw_upgrade(void)
{
    if( SystemState.DFUState != SP_DFU_NONE ) {
        /* User error */
        NRF_LOG_WARNING("DFU already in progress!");
        return 0;
    }

    NRF_LOG_WARNING("Initiating sensor DFU...");

    SystemState.SensorState = SENSOR_STATE_DFU;

    /* Reset sequence and segment numbers */
    SENSORDFU_SEQUENCE = 0;
    SENSORDFU_SEGMENT = 0;

    /* Retrieve page size to communicate to the host application */
    uint16_t pageSize = 0;
    RET_ON_ERROR( SP_Sensors_DFU_Init(&pageSize) );
    memcpy(&SENSORDFU_DATA, (uint8_t*) &pageSize, 2);

    // pageSize += 16;
    uint16_t nSegments = (pageSize / SENSORDFU_BUFFER_LEN) + ( (pageSize % SENSORDFU_BUFFER_LEN) ? 1 : 0 );
    SENSORDFU_SEGMENTLEN = nSegments;
    // pageSize -= 16;

    NRF_LOG_DEBUG("\tPage size:     %u bytes", pageSize);
    NRF_LOG_DEBUG("\tBuffer size:   %u bytes", SENSORDFU_BUFFER_LEN);
    NRF_LOG_DEBUG("\tSegments/page: %u", SENSORDFU_SEGMENTLEN);
    NRF_LOG_INFO( "\tOk.");

    SystemState.DFUState = SP_DFU_INIT;
    
    return 0;
}

static uint32_t fw_upgrade_schedule(void)
{
    queue_clear();

    RET_ON_ERROR( acquisition_state_schedule(0) );
    RET_ON_ERROR( sensor_state_schedule(0) );
    RET_ON_ERROR( queue_operation(SP_OPERATION_FWUPG_INIT) );

    return 0;
}

static uint32_t sensors_off(void)
{
    if( SystemState.SensorState != SENSOR_STATE_ON ) {
        /* User error */
        NRF_LOG_WARNING("Sensor is not in ON state!");
        return 0;
    }

    NRF_LOG_DEBUG("Disabling sensor suite");

    SP_Sensors_ISR_Disable();
    RET_ON_ERROR( SP_Sensors_Stop() );

    SystemState.SensorState = SENSOR_STATE_IDLE;
    return 0;
}

static uint32_t sensors_on(void)
{
    NRF_LOG_DEBUG("Enabling sensor suite");

    if( SystemState.SensorState != SENSOR_STATE_ON ) {
        /* User error */
        NRF_LOG_WARNING("Sensor is not in IDLE state!");
        return 0;
    }

    SP_Sensors_ISR_Enable();
    RET_ON_ERROR( SP_Sensors_Start() );
    RET_ON_ERROR( SP_Sensors_Configure() );

    /* trigger first transfer manually */
    /* @todo move to SP Sensors (Start/configure) */
    MAX32664_InterruptReceived();

    SystemState.SensorState = SENSOR_STATE_ON;
    return 0;
}

static uint32_t sensor_state_schedule(uint8_t state)
{
    /* @todo make clean for all the state transitions */

    /* sensors should be off */
    if( !state && (SystemState.SensorState == SENSOR_STATE_ON) ) {
        return queue_operation(SP_OPERATION_SWITCH_OFF);
    }
    /* sensors should be on */
    else if( state && (SystemState.SensorState == SENSOR_STATE_IDLE) ) {
        return queue_operation(SP_OPERATION_SWITCH_ON);
    }
    /* sensors already off */
    else if( !state && (SystemState.SensorState == SENSOR_STATE_IDLE) ) {
        return 0;
    }
    /* sensors already on */
    else if( state && (SystemState.SensorState == SENSOR_STATE_ON) ) {
        return 0;
    }

    return 1;
}

static void queue_clear(void)
{
    for(uint32_t i = 0; i < OPERATION_QUEUE_LENGTH; ++i) {
        SystemState.OpQueue[i] = SP_OPERATION_NONE;
    }

    SystemState.pOpQueueRead = SystemState.OpQueue;
    SystemState.pOpQueueWrite = SystemState.OpQueue;
}

static uint32_t queue_operation(const SP_Operation_t op)
{
    if( *SystemState.pOpQueueWrite != SP_OPERATION_NONE ) {
        NRF_LOG_ERROR("Operation queue full!");
        return -1;
    }

    *SystemState.pOpQueueWrite = op;

    SystemState.pOpQueueWrite++;
    if( SystemState.pOpQueueWrite == &SystemState.OpQueue[OPERATION_QUEUE_LENGTH] ) {
        SystemState.pOpQueueWrite = SystemState.OpQueue;
    }

    return 0;
}


#if 0

#include "bms.h"
#include "MAX32664.h"
#include "macros.h"

#include "sp_ble.h"
#include "sp_system.h"

typedef struct {
    uint8_t     ACQ_Enabled;
} SP_BLEVars_t;

typedef struct {
    
    SP_BLEVars_t        BLEVars;
    SP_Operation_t      OpQueue[OPERATION_QUEUE_LENGTH];
    SP_Operation_t*     pOpQueueRead;
    SP_Operation_t*     pOpQueueWrite;

    bool                sACQNotifications;

} SP_System_t;

static EWS_Data_t EWS_Data = {
    .RR = 15,
    .SpO2 = 99,
    .O2 = 0,
    .sBP = 120,
    .HR = 76,
    .ACVPU = ACVPU_ALERT,
    .Temp = 368
};

uint32_t SP_CalculateEWS(EWS_Data_t* const pData)
{
    uint32_t err_code = 0;
    uint8_t score = 0;

    /* Calculate RR score */
    if( pData->RR > 24 )                    score += 3;
    else if( pData->RR > 20 )               score += 2;
    else if( pData->RR < 9 )                score += 3;
    else if( pData->RR < 12 )               score += 1;

    /* Calculate SpO2 score */
    if( pData->SpO2 < 92 )                  score += 3;
    else if( pData->SpO2 < 94 )             score += 2;
    else if( pData->SpO2 < 96 )             score += 1;

    /* Calculate oxygenation score */
    if( pData->O2 )                         score += 2;

    /* Calculate systolic BP score */
    if( pData->sBP > 219 )                  score += 3;
    else if( pData->sBP < 91 )              score += 3;
    else if( pData->sBP < 101 )             score += 2;
    else if( pData->sBP < 111 )             score += 1;

    /* Calculate HR score */
    if( pData->HR > 130 )                   score += 3;
    else if( pData->HR > 110 )              score += 2;
    else if( pData->HR > 90 )               score += 1;
    else if( pData->HR < 41 )               score += 3;
    else if( pData->HR < 51 )               score += 1;

    /* Calculate consciousness score */
    if( pData->ACVPU == ACVPU_UNRESP )      score += 3;
    else if( pData->ACVPU & ACVPU_UNRESP )  err_code = -1;
    else if( pData->ACVPU )                 score += 3;

    /* Calculate temperature score */
    if( pData->Temp > 390 )                 score += 2;
    else if( pData->Temp > 380 )            score += 1;
    else if( pData->Temp < 351 )            score += 3;
    else if( pData->Temp < 361 )            score += 1;

    if( !err_code ) {
        pData->Score = score;
    }

    return err_code;
}

#endif
