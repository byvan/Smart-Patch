/**
 * @file bms.h
 * @brief      Implementation of the BMS of the Smart Patch.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       October 2020
 */

#include "nrf_log.h"
#include "macros.h"
#include "BQ25125.h"
#include "bms.h"

/**
 * @brief      Read function for the BQ25125.
 *
 * @param[out] pData  Pointer to the data buffer.
 * @param[in]  size   The number of bytes to be read.
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t _BMS_Read(uint8_t* pData, const uint32_t size);

/**
 * @brief      Write function for the BQ25125.
 *
 * @param[in]  pData   Pointer to the data to be written.
 * @param[in]  size    The number of bytes to be written.
 * @param[in]  noStop  If set, no stop condition will be generated.
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t _BMS_Write(const uint8_t* pData, const uint32_t size, bool noStop);

static const nrf_drv_twi_t* _BMS_Interface;

uint32_t SP_BMS_Init(const nrf_drv_twi_t* pInterface)
{
    NRF_LOG_INFO("Initialising BMS.");
    _BMS_Interface = pInterface;

    // RET_ON_ERROR(BQ25125_Init(_BMS_Read, _BMS_Write));
    BQ25125_Init(_BMS_Read, _BMS_Write);
    // RET_ON_ERROR(BQ25125_SYSVOUT_Set(BQ25125_SYSVOUT_1V8));
    BQ25125_SYSVOUT_Set(BQ25125_SYSVOUT_1V8);
    // RET_ON_ERROR(BQ25125_BVCR_Set(BQ25125_BVCR_4V2));
    BQ25125_BVCR_Set(BQ25125_BVCR_4V2);

    return 0;
}

static uint32_t _BMS_Read(uint8_t* pData, const uint32_t size)
{
    return nrf_drv_twi_rx(_BMS_Interface, BQ25125_ADDRESS_READ, pData, size);
}

static uint32_t _BMS_Write(const uint8_t* pData, const uint32_t size, bool noStop)
{
    return nrf_drv_twi_tx(_BMS_Interface, BQ25125_ADDRESS_WRITE, pData, size, noStop);
}
