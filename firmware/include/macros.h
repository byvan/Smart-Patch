#ifndef __MACROS_H
#define __MACROS_H

#define RET_ON_ERROR( err )                 \
    do {                                    \
        uint32_t LOCAL_ERR = err;           \
        if( LOCAL_ERR ) {                   \
            return LOCAL_ERR;               \
        }                                   \
    } while (0)

#endif /* __MACROS_H included */
