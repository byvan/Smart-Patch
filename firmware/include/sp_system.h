/**
 * @file sp_system.h
 * @brief      Smart Patch general system definitions.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       November 2020
 */

#ifndef __SP_SYSTEM_H
#define __SP_SYSTEM_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "app_timer.h"
#include "nrf_drv_twi.h"

/**
 * @addtogroup   SP_FW
 * @{
 */

/**
 * @defgroup   SP_System Smart Patch Functionality Module
 * @{
 */

/* Exported constants --------------------------------------------------------*/

/*! Mask for the device register to set the sensor state. */
#define cfgSENSORS_ON_MASK      0x01
/*! Mask for the device register to toggle acquisition procedures. */
#define cfgACQUISITION_ON_MASK  0x02
/*! Mask to schedule a FW upgrade of the MAX32664 */
#define cfgUPGRADE_MASK         0x08

/* Exported types ------------------------------------------------------------*/

/* Exported macros -----------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/

/**
 * @brief      Initialises the Smart Patch System
 *
 * @param[in]  pInterface  The I2C interface to use.
 * @param[out] interval    The interval in ticks for the timer to use.
 * @param[out] pCallback   Pointer to set the callback to.
 *
 * @return     any error that comes up during initialisation.
 */
uint32_t SP_System_Init(const nrf_drv_twi_t* pInterface, app_timer_timeout_handler_t* const pCallback, uint32_t (*pTimStart)(uint32_t), uint32_t (*pTimStop)(void));

/**
 * @brief      Process any pending operations.
 *
 * @param[out] pending  true if it was true before, or if an action in this
 *                      module is still pending.
 *
 * @return     any error that comes up during processing.
 */
uint32_t SP_System_Process(bool* const pending);

/**
 * @}
 */

/**
 * @}
 */

#if 0

typedef enum {
    ACVPU_ALERT     = 0x00,
    ACVPU_CONFUSED  = 0x01,
    ACVPU_VOICE     = 0x02,
    ACVPU_PAIN      = 0x04,
    ACVPU_UNRESP    = 0x08
} ACVPU_t;

/**
 * @brief      Data struct to collect all EWS relevant data.
 */
typedef struct __attribute__((__packed__, aligned(1))) {
    uint8_t     RR;             /*!< Respiratory Rate [breaths per minute] */
    uint8_t     SpO2;           /*!< Percent of oxygenated peripheral blood [%] */
    uint8_t     O2;             /*!< Supplementary oxygen administered {0,1} */
    uint8_t     sBP;            /*!< Systolic blood pressure [mmHg] */
    uint8_t     HR;             /*!< Heart Rate [beats per minute] */
    uint8_t     ACVPU;          /*!< Consciousness, see @ref ACVPU_t */
    uint16_t    Temp;           /*!< Temperature [0.1 ˚C] */
    uint8_t     _reserved[3];   /*!< Reserved fields */
    uint8_t     Score;          /*!< The final score of the parameters. */
} EWS_Data_t;

/**
 * @brief      Callback that enables the data acquisition from the sensor units.
 */
void SP_Acquisition_Enable();

/**
 * @brief      Calculates the Early Warning Score from the given parameters.
 *
 * @param      pData  Pointer to the data struct, containing all relevant
 *                    parameter.
 *
 * @return     0 if the calculation was successful.
 *
 *             The resulting score will be written into the Score attribute of
 *             @p pData.
 */
uint32_t SP_CalculateEWS(EWS_Data_t* const pData);

/**
 * @brief      Sets the notification status of the ACQ service.
 *
 * @param[in]  enabled  Indicates if enabled
 */
void SP_ACQ_Notifications_Enabled(bool enabled);

#endif

#endif /* __SP_SYSTEM_H included */
