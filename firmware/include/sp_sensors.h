/**
 * @file sp_sensors.h
 * @brief      Definitions for the sensor suite of the Smart Patch.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       October 2020
 */

#ifndef __SP_SENSORS_H
#define __SP_SENSORS_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "nrf_drv_twi.h"

/**
 * @addtogroup   SP_FW
 * @{
 */

/**
 * @defgroup   SP_Sensors Smart Patch Sensor Module
 * @{
 */

/* Exported constants --------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported macros -----------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/

/**
 * @brief      Configures the MAX32664.
 *
 * @return     Any error that comes up during the operation.
 */
uint32_t SP_Sensors_Configure(void);

/**
 * @brief      Flashes a page to the MAX32664.
 *
 * @param[in]  pData  Pointer to a (partial) page (max page size).
 * @param[in]  size   The number of bytes that were written.
 *
 * @return     Any error that comes up during processing.
 */
uint32_t SP_Sensors_DFU_FlashPage(const uint8_t* pData, const uint16_t size);

/**
 * @brief      Initiates the device firmware upgrade of the MAX32664.
 *
 * @param      pSize  Pointer to the variable to store the page size.
 *
 * @return     Any error that comes up during processing.
 */
uint32_t SP_Sensors_DFU_Init(uint16_t* const pSize);

/**
 * @brief      Writes the necessary configuration data to the MAX32664.
 *
 * @param[in]  pData  Pointer to the 28 byte config data array.
 *
 * @return     Any error that comes up during processing.
 */
uint32_t SP_Sensors_DFU_Prepare(const uint8_t* pData);

/**
 * @brief      Initialises the sensor suite of the Smart Patch device.
 *
 * @param[in]  pInterface  Pointer to the I2C interface.
 *
 * @return     Any error that comes up during initialisation.
 */
uint32_t SP_Sensors_Init(const nrf_drv_twi_t* pInterface);

/**
 * @brief      Disable the MFIO ISR.
 */
void SP_Sensors_ISR_Disable(void);

/**
 * @brief      Enables the MFIO ISR.
 */
void SP_Sensors_ISR_Enable(void);

/**
 * @brief      Processes any sensor related events.
 *
 * @param[out] pending  true if it was true before, or if an action in this
 *                      module is still pending.
 *
 * @return     Any error that comes up during execution.
 */
uint32_t SP_Sensors_Process(bool* const pending);

/**
 * @brief      Starts the sensor device and algorithms.
 *
 * @return     Any error that comes up during the operation.
 */
uint32_t SP_Sensors_Start(void);

/**
 * @brief      Stops the sensor device and algorithms.
 *
 * @return     Any error that comes up during the operation.
 */
uint32_t SP_Sensors_Stop(void);

/**
 * @}
 */

/**
 * @}
 */

#endif /* __SP_SENSORS_H included */
