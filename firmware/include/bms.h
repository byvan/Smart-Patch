/**
 * @file bms.h
 * @brief      Definitions for the BMS of the Smart Patch.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       October 2020
 */

#ifndef __BMS_H
#define __BMS_H

#include "nrf_drv_twi.h"

/**
 * @brief      Initialises the BMS of the Smart Patch device.
 *
 * @param[in]  pInterface  Pointer to the I2C interface.
 *
 * @return     { description_of_the_return_value }
 */
uint32_t SP_BMS_Init(const nrf_drv_twi_t* pInterface);

#endif /* __BMS_H included */
