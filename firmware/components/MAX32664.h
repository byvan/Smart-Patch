/**
 * @file MAX32664.h
 * @brief      Definitions for the interface to the Maxim MAX32664 device.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       October 2020
 */

#ifndef __MAX32664_H
#define __MAX32664_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "nrf_drv_twi.h"

/**
 * @addtogroup   SP_FW
 * @{
 */

/**
 * @defgroup   MAX32664 MAX32664 Sensor Hub Module
 * @{
 */

/* Exported constants --------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/**
 * @brief      MAX32664D BPT Algorithm mode.
 */
typedef enum {
    MAX32664_ALGO_DISABLED      = 0x00,
    MAX32664_ALGO_CALIBRATION   = 0x01,
    MAX32664_ALGO_ESTIMATION    = 0x02
} MAX32664_AlgoMode_t;

/**
 * @brief      Enumeration of the output mode.
 */
typedef enum {
    MAX32664_OUT_PAUSE          = 0x00,
    MAX32664_OUT_SENSOR         = 0x01,
    MAX32664_OUT_ALG            = 0x02,
    MAX32664_OUT_SENSORALG      = 0x03,
    MAX32664_OUT_PAUSE2         = 0x04,
    MAX32664_OUT_CNTSENSOR      = 0x05,
    MAX32664_OUT_CNTALG         = 0x06,
    MAX32664_OUT_CNTSENSORALG   = 0x07
} MAX32664_OutputMode_t;

/**
 * @brief      Helper struct to configure the device.
 */
typedef struct {
    MAX32664_OutputMode_t       outputMode;     /*!< The output mode to use */
    uint8_t                     fifoThreshold;  /*!< FIFO threshold before interrupt generation */
} MAX32664_Config_t;

/**
 * @brief      Enumeration of the MAX32664 boot modes.
 */
typedef enum {
    MAX32664_APPLICATION_MODE   = 0x00,     /*!< Normal application mode */
    MAX32664_RESET_MODE         = 0x02,     /*!< Reset mode, performs a soft reset of the device */
    MAX32664_BOOTLOADER_MODE    = 0x08      /*!< Bootloader mode, used for DFU */
} MAX32664_DeviceMode_t;

/**
 * @brief      MAX30102 Sensor state.
 */
typedef enum {
    MAX32664_DISABLED           = 0x00,     /*!< Sensor is disabled */
    MAX32664_ENABLED            = 0x01      /*!< Sensor is enabled */
} MAX32664_EnabledState_t;

/* Exported macros -----------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/

/**
 * @brief      Set the configuration parameters of the MAX32664.
 *
 * @param[in]  cfg   The configuration to be applied.
 *
 * @return     Any error that comes up.
 */
uint32_t MAX32664_Configure(const MAX32664_Config_t* const cfg);

/**
 * @brief      Erases the application memory.
 *
 * @return     Any error that comes up.
 */
uint32_t MAX32664_EraseMemory(void);

/**
 * @brief      Initialises the MAX32664 sensor hub.
 *
 * @param[in]  upgradeRequired  Pointer to a buffer, indicating if and upgrade
 *                              is required.
 *
 * @return     Any error that comes up.
 *
 *             If the device is not in application mode, the host controller
 *             tries to set it to application mode. If this failes and it is
 *             detected, that the device is in bootloader mode, the firmware
 *             must be upgraded. This is signalled through the @p
 *             upgradeRequired parameter.
 */
uint32_t MAX32664_Init(bool* const upgradeRequired);

/**
 * @brief      Gets the bootloader version of the device.
 *
 * @param[out] version  Pointer to a 3 byte array.
 *
 * @return     Any error that comes up.
 *
 *             On success, the output array will contain the bootloader version
 *             in the following format MAJOR - MINOR - REVISION.
 */
uint32_t MAX32664_GetBLVersion(uint8_t* version);

/**
 * @brief      Reads the device mode of the MAX32664.
 *
 * @return     The current device mode.
 */
MAX32664_DeviceMode_t MAX32664_GetDeviceMode(void);

/**
 * @brief      Gets the Firmware version of the device.
 *
 * @param[out] version  Pointer to a 3 byte array.
 * @param[in]  reload   flag if the info should be reread or taken from buffer.
 *
 * @return     Any error that comes up.
 *
 *             On success, the output array will contain the firmware version in
 *             the following format MAJOR - MINOR - REVISION.
 */
uint32_t MAX32664_GetFWVersion(uint8_t* version, bool reload);

/**
 * @brief      Enables the algorithm of the MAX32664.
 *
 * @param[in]  mode  The mode of the algorithm.
 *
 * @return     Any error that comes up.
 */
uint32_t MAX32664_SetAlgorithm(const MAX32664_AlgoMode_t mode);

/**
 * @brief      Sets the authentication bytes for the DFU.
 *
 * @param[in]  pData  Pointer to the 16 authentication bytes.
 *
 * @return     Any error that comes up.
 */
uint32_t MAX32664_SetAuthBytes(const uint8_t* pData);

/**
 * @brief      Sets the device mode of the MAX32664.
 *
 * @param[in]  mode  The mode to set the device into (see
 *                   #MAX32664_DeviceMode_t).
 *
 * @return     Any error that comes up.
 */
uint32_t MAX32664_SetDeviceMode(const MAX32664_DeviceMode_t mode);

/**
 * @brief      Set the number of pages to flash.
 *
 * @param[in]  pages  The number of pages to flash.
 *
 * @return     Any error that comes up.
 */
uint32_t MAX32664_SetFlashPages(uint8_t pages);

/**
 * @brief      Sets the initialisation vector for the DFU.
 *
 * @param[in]  pData  Pointer to the 11 IV.
 *
 * @return     Any error that comes up.
 */
uint32_t MAX32664_SetIVBytes(const uint8_t* pData);

/**
 * @brief      Sets the enabling state of the MAX30102 sensor.
 *
 * @param[in]  state  The state of the sensor.
 *
 * @return     Any error that comes up.
 */
uint32_t MAX32664_SetSensor(const MAX32664_EnabledState_t state);

/**
 * @}
 */

/**
 * @}
 */


















/*! I2C slave address */
#define MAX32664_ADDRESS                0x55
/*! I2C read command delay [ms] */
#define MAX32664_CMD_DELAY_MS           2

#define MAX32664_ERR_NONE               0x0000
#define MAX32664_ERR_UNAVAIL_CMD        0xF001
#define MAX32664_ERR_UNAVAIL_FUNC       0xF002
#define MAX32664_ERR_DATA_FORMAT        0xF003
#define MAX32664_ERR_INPUT_VALUE        0xF004
#define MAX32664_ERR_INVALID_MODE       0xF005
#define MAX32664_ERR_BTLDR_TRY_AGAIN    0xF005
#define MAX32664_ERR_BTLDR_GENERAL      0xF080
#define MAX32664_ERR_BTLDR_CHECKSUM     0xF081
#define MAX32664_ERR_BTLDR_AUTH         0xF082
#define MAX32664_ERR_BTLDR_INVALID_APP  0xF083
#define MAX32664_ERR_TRY_AGAIN          0xF0FE
#define MAX32664_ERR_UNKNOWN            0xF0FF
#define MAX32664_ERR_COM                0xF101

#define MAX32664_CMD_STATUS_READ        { 0x00, 0x00 }
#define MAX32664_CMD_DEVMODE_SET        { 0x01, 0x00 }
#define MAX32664_CMD_DEVMODE_READ       { 0x02, 0x00 }
#define MAX32664_CMD_OUTMODE_SET        { 0x10, 0x00 }
#define MAX32664_CMD_FIFOTH_SET         { 0x10, 0x01 }
// #define MAX32664_CMD_SAMPLEP_SET        { 0x10, 0x02 }  // Only B & C variants
// #define MAX32664_CMD_ADDRESS_SET        { 0x10, 0x03 }  // Only B & C variants
// #define MAX32664_CMD_COUNTER_SET        { 0x10, 0x04 }  // Only B & C variants
#define MAX32664_CMD_OUTMODE_READ       { 0x11, 0x00 }
#define MAX32664_CMD_FIFOTH_READ        { 0x11, 0x01 }
// #define MAX32664_CMD_SAMPLEP_READ       { 0x11, 0x02 }  // Only B & C variants
// #define MAX32664_CMD_ADDRESS_READ       { 0x11, 0x03 }  // Only B & C variants
// #define MAX32664_CMD_COUNTER_READ       { 0x11, 0x04 }  // Only B & C variants
#define MAX32664_CMD_FIFOLEVEL_READ     { 0x12, 0x00 }
#define MAX32664_CMD_FIFO_READ          { 0x12, 0x01 }
// #define MAX32664_CMD_SAMPLESIZE_READ    { 0x13, 0x00 }  // Only A, B & C variants
#define MAX32664_CMD_FIFOINSIZE_READ    { 0x13, 0x01 }
// #define MAX32664_CMD_FIFOSENSSIZE_READ  { 0x13, 0x02 }  // Only A, B & C variants
// #define MAX32664_CMD_FIFOINLEVEL_READ   { 0x13, 0x03 }  // Only A, B & C variants
#define MAX32664_CMD_FIFOSENSLEVEL_READ { 0x13, 0x04 }
#define MAX32664_CMD_FIFOIN_WRITE       { 0x14, 0x00 }
// #define MAX32664_CMD_SENS1_WRITE        { 0x40, 0x00 }  // Only B & C variants
// #define MAX32664_CMD_SENS2_WRITE        { 0x40, 0x01 }  // Only B variant
// #define MAX32664_CMD_SENS3_WRITE        { 0x40, 0x02 }  // Only B variant
#define MAX32664_CMD_SENSAD_WRITE       { 0x40, 0x03 }  // Only A & D variants
// #define MAX32664_CMD_ACC_WRITE          { 0x40, 0x04 }  // Only A, B & C variants
// #define MAX32664_CMD_SENS1REG_READ      { 0x41, 0x00 }  // Only B & C variants
// #define MAX32664_CMD_SENS2REG_READ      { 0x41, 0x01 }  // Only B variant
// #define MAX32664_CMD_SENS3REG_READ      { 0x41, 0x02 }  // Only B variant
#define MAX32664_CMD_SENSADREG_READ     { 0x41, 0x03 }  // Only A & D variants
#define MAX32664_CMD_ACC_WRITE          { 0x41, 0x04 }
                                        // { 0x42, 0x00 }
                                        // { 0x42, 0x01 }
                                        // { 0x42, 0x02 }
#define MAX32664_CMD_AFE_SENS_READ      { 0x42, 0x03 }  // Only A & D variants
#define MAX32664_CMD_AFE_ACC_READ       { 0x42, 0x04 }
                                        // { 0x43, 0x00 }
                                        // { 0x43, 0x01 }
                                        // { 0x43, 0x02 }
#define MAX32664_CMD_SENS_DUMP          { 0x43, 0x03 }  // Only A & D variants
#define MAX32664_CMD_ACC_DUMP           { 0x43, 0x04 }
                                        // { 0x44, 0x00 }
                                        // { 0x44, 0x01 }
                                        // { 0x44, 0x02 }
#define MAX32664_CMD_SENS_ENABLE        { 0x44, 0x03 }  // Only A & D variants
// #define MAX32664_CMG_ACC_ENABLE         { 0x44, 0x04 }  // Only A, B & C variants
                                        // { 0x44, 0xFF }
                                        // { 0x45, 0x00 }
                                        // { 0x45, 0x01 }
                                        // { 0x45, 0x02 }
#define MAX32664_CMD_SENS_READ          { 0x45, 0x03 }  // Only A & D variants
                                        // { 0x45, 0x04 }
                                        // { 0x46, 0x00 }
                                        // { 0x47, 0x00 }
#define MAX32664_CMD_AGC_SET            { 0x50, 0x00 }  // Only A & D variants
#define MAX32664_CMD_BPT_SET            { 0x50, 0x04 }  // Only D variant
                                        // { 0x50, 0x07 }
#define MAX32664_CMD_AGC_READ           { 0x51, 0x00 }  // Only A & D variants
#define MAX32664_CMD_BPT_READ           { 0x50, 0x04 }  // Only D variant
                                        // { 0x51, 0x07 }
#define MAX32664_CMD_AGC_ENABLE         { 0x52, 0x00 }  // Only A variant (apparently not?)
#define MAX32664_CMD_AEC_ENABLE         { 0x52, 0x01 }
                                        // { 0x52, 0x02 }
#define MAX32664_CMD_ECG_ENABLE         { 0x52, 0x03 }
#define MAX32664_CMD_BPT_ENABLE         { 0x52, 0x04 }  // Only D variant
                                        // { 0x52, 0x07 }
#define MAX32664_BOOTCMD_INIT           { 0x80, 0x00 }
#define MAX32664_BOOTCMD_AUTH           { 0x80, 0x01 }
#define MAX32664_BOOTCMD_PAGE           { 0x80, 0x02 }
#define MAX32664_BOOTCMD_ERASE          { 0x80, 0x03 }
#define MAX32664_BOOTCMD_SEND           { 0x80, 0x04 }
#define MAX32664_BOOTCMD_INFO           { 0x81, 0x00 }
#define MAX32664_BOOTCMD_PAGESIZE       { 0x81, 0x01 }
#define MAX32664_CMD_MCUTYPE_READ       { 0xFF, 0x00 }
#define MAX32664_CMD_HUBVERSION_READ    { 0xFF, 0x03 }
// #define MAX32664_CMD_ALGVERSION_READ    { 0xFF, 0x07 }  // Deprecated









typedef enum {
    MAX32664_STATE_UNINIT = 0,
    MAX32664_STATE_READY = 1
} MAX32664_State_t;

// typedef struct {
//     const nrf_drv_twi_t*    interface;      /*!< Pointer to the I2C instance to use. */
//     uint8_t                 pin_nRST;       /*!< Pin number of the nRST pin. */
//     uint8_t                 pin_MFIO;       /*!< Pin number of the MFIO pin. */
// } MAX32664_config_t;






/**
 * @brief      Flashes a page to the application memory.
 *
 * @param[in]  pData  The data
 *
 * @return     { description_of_the_return_value }
 */
uint32_t MAX32664_DFU_FlashPage(const uint8_t* pData);







/**
 * @brief      Gets the number of samples in the fifo.
 *
 * @param[out] count  The number of samples.
 *
 * @return     { description_of_the_return_value }
 */
uint32_t MAX32664_GetFifoCount(uint8_t* const count);



/**
 * @brief      Reads the page size from the bootloader of the MAX32664.
 *
 * @param[out] size  The page size of the bootloader.
 *
 * @return     { description_of_the_return_value }
 */
uint32_t MAX32664_GetPageSize(uint16_t* const size);

/**
 * @brief      { function_description }
 *
 * @param[out] pData   Pointer to the data structure to store the results.
 * @param[in]  nBytes  The number of bytes to read.
 *
 * @return     { description_of_the_return_value }
 * 
 * @note       @p pData must have one more byte allocated than needs to be read.
 */
uint32_t MAX32664_GetSensorData(uint8_t* pData, const uint8_t nBytes);

/**
 * @brief      Reads the status register from the MAX32664.
 *
 * @return     The status or an error code.
 */
uint32_t MAX32664_GetStatus(void);



/**
 * @brief      Call this function from the MFIO ISR.
 */
void MAX32664_InterruptReceived(void);

/**
 * @brief      Check if an interrupt is pending to be processed.
 *
 * @param[in]  clear  If the interrupt should be cleared.
 *
 * @return     whether an interrupt is pending processing.
 */
bool MAX32664_InterruptPending(bool clear);

/**
 * @brief      Function implementing a generic read process to the MAX32664.
 *
 * @param[in]  size   The number of bytes to be read.
 * @param[out] pData  Pointer to the data buffer.
 *
 * @return     0 if the process was completed.
 * @return     non-zero value if the process failed.
 *
 * @note       This function has to be implemented by the user.
 */
uint32_t MAX32664_ReadData(const uint32_t size, uint8_t* pData);

/**
 * @brief      Reads a register from the sMAX30102 sensor.
 *
 * @param[in]  addr   The address to read from.
 * @param[out] pData  The register.
 *
 * @return     { description_of_the_return_value }
 */
uint32_t MAX32664_ReadSensorRegister(const uint8_t addr, uint8_t* const pData);

/**
 * @brief      Function implementing reset and boot procedures for the MAX32664.
 *
 * @param[in]  mode  The mode the device should boot into.
 *
 * @return     0 if the process was completed.
 * @return     non-zero value if the process failed.
 *
 * @note       This function has to be implemented by the user.
 */
uint32_t MAX32664_Reset(const MAX32664_DeviceMode_t mode);



/**
 * @brief      Sets the AGC state.
 *
 * @param[in]  state  The state to be used.
 *
 * @return     { description_of_the_return_value }
 */
uint32_t MAX32664_SetAGC(const MAX32664_EnabledState_t state);











/**
 * @brief      Generic wait function.
 *
 * @param[in]  ms    The time to wait in milliseconds.
 *
 * @note       This function has to be implemented by the user.
 */
void MAX32664_Wait(uint32_t ms);

/**
 * @brief      Function implementing a generic write process to the MAX32664.
 *
 * @param[in]  pData   Pointer to the data to be written.
 * @param[in]  size    The number of bytes to be read.
 * @param[in]  noStop  If set, the transmission continues and more data has to
 *                     be provided.
 *
 * @return     0 if the process was completed.
 * @return     non-zero value if the process failed.
 *
 * @note       This function has to be implemented by the user.
 */
uint32_t MAX32664_WriteData(const uint8_t* pData, const uint32_t size, const bool noStop);

/**
 * @brief      Writes data to a sensor register.
 *
 * @param[in]  pData  The data to be sent [register, data]
 *
 * @return     { description_of_the_return_value }
 */
uint32_t MAX32664_WriteSensorRegister(const uint8_t* pData);

#endif /* __MAX32664_H included */
