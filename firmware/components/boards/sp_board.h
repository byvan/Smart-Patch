/**
 * @file sp_board.h
 * @brief      Smart Patch nRF52833 board definitions.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       October 2020
 */

#ifndef __SP_BOARD_H
#define __SP_BOARD_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @addtogroup   SP_FW
 * @{
 */

/**
 * @defgroup   SP_Config Smart Patch Firmware Configuration
 * @{
 */

/*! Data acquisition interval [ms] */
#define DATA_INTERVAL_MS        1000


/*! I2C interface */
#define I2C_MASTER_INST         0
/*! I2C Master SCL pin */
#define I2C_MASTER_SCL          41
/*! I2C Master SDA pin */
#define I2C_MASTER_SDA          11

/*! UART RX Pin */
#define RX_PIN_NUMBER           10
/*! UART TX Pin */
#define TX_PIN_NUMBER           9
/*! UART CTS Pin */
#define CTS_PIN_NUMBER          
/*! UART RTS Pin */
#define RTS_PIN_NUMBER          

/*! EN pin for the boost converter */
#define SP_PIN_BOOST_EN         15
/*! MFIO pin of the MAX32664 */
#define SP_PIN_MAX32664_MFIO    17
/*! Reset pin of the MAX32664 */
#define SP_PIN_MAX32664_nRST    18

/*! Name of device. Will be included in the advertising data. */
#define DEVICE_NAME             "Smart Patch v1"
/*! Device base UUID. */
#define DEVICE_UUID             {{ 0x02, 0x1E, 0x5A, 0xAF, 0x2E, 0xDF, 0x48, 0xB6, 0x0C, 0x4E, 0xAC, 0xA7, 0x6D, 0xBC, 0x9A, 0x9C }}

/**
 * @}
 */

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __SP_BOARD_H included */