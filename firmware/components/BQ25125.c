/**
 * @file BQ25125.c
 * @brief      Implementation of the interface to the TI BQ25125 device.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       October 2020
 */

#include "BQ25125.h"

/*! Start address of the register map */
#define BQ25125_REG_BASE            0x00

#define BQ25125_REG_SSMCR           ( 0x00 + BQ25125_REG_BASE )
#define BQ25125_REG_BVCR            ( 0x05 + BQ25125_REG_BASE )
#define BQ25125_REG_SYSVOCR         ( 0x06 + BQ25125_REG_BASE )
#define BQ25125_REG_LSLDOCR         ( 0x07 + BQ25125_REG_BASE )

typedef struct __attribute__((__packed__, aligned(1))) {
    uint8_t     SSMCR;      /*!< Status and Ship Mode Control Register */
    uint8_t     FFMR;       /*!< Faults and Faults Mask Register */
    uint8_t     TSCFMR;     /*!< TS Control and Faults Mask Register */
    uint8_t     FCCR;       /*!< Fast Charge Control Register */
    uint8_t     TPCAR;      /*!< Termination/Pre-Charge and I2C Address Register */
    uint8_t     BVCR;       /*!< Battery Voltage Control Register */
    uint8_t     SYSVOCR;    /*!< SYS VOUT Control Register */
    uint8_t     LSLDOCR;    /*!< Load Switch and LDO Control Register */
    uint8_t     PBCR;       /*!< Push-button Control Register */
    uint8_t     ILUVCR;     /*!< ILIM and Battery UVLO Control Register */
    uint8_t     VBBMR;      /*!< Voltage Based Battery Monitor Register */
    uint8_t     VITR;       /*!< VIN_DPM and Timers Register */
} BQ25125_Registers_t;

typedef struct {
    BQ25125_Registers_t     Registers;

    BQ25125_Read_t          read;
    BQ25125_Write_t         write;
} BQ25125_Handler_t;


/**
 * @brief      Reads a register from the device.
 *
 * @param[in]  addr  The register address to read from.
 * @param[out] pReg  Pointer to a buffer to store it.
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t _ReadRegister(const uint8_t addr, uint8_t* const pReg);

/**
 * @brief      Reads the control registers of the device.
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t _ReadRegisters(void);

static BQ25125_Handler_t hDevice = {0};


void BQ25125_BVCR_Get(BQ25125_BVCR_t* const bvcr)
{
    *bvcr = (BQ25125_BVCR_t) ( hDevice.Registers.BVCR >> 1 );
}

uint32_t BQ25125_BVCR_Set(const BQ25125_BVCR_t bvcr)
{
    uint8_t wData[2] = {0};
    wData[0] = BQ25125_REG_BVCR;
    wData[1] = (((uint8_t) bvcr) << 1);

    uint32_t err_code = hDevice.write(wData, 2, false);
    if( err_code ) return err_code;

    err_code = _ReadRegister(BQ25125_REG_BVCR, &hDevice.Registers.BVCR);
    if( err_code ) return err_code;

    BQ25125_BVCR_t resBvcr;
    BQ25125_BVCR_Get(&resBvcr);
    return !(bvcr == resBvcr);
}

void BQ25125_LSLDO_Get(BQ25125_LSLDO_t* const lsldo)
{
    *lsldo = (BQ25125_LSLDO_t) ( (0x80 & hDevice.Registers.LSLDOCR) >> 7);
}

uint32_t BQ25125_LSLDO_Set(const BQ25125_LSLDO_t lsldo)
{
    uint8_t wData[2] = {0};
    wData[0] = BQ25125_REG_LSLDOCR;
    wData[1] = ((uint8_t) lsldo) << 7;

    uint32_t err_code = hDevice.write(wData, 2, false);
    if( err_code ) return err_code;

    err_code = _ReadRegister(BQ25125_REG_LSLDOCR, &hDevice.Registers.LSLDOCR);
    if( err_code ) return err_code;

    BQ25125_LSLDO_t resLsldo;
    BQ25125_LSLDO_Get(&resLsldo);
    return !(lsldo == resLsldo);
}

uint32_t BQ25125_Init(BQ25125_Read_t rFunc, BQ25125_Write_t wFunc)
{
    hDevice.read = rFunc;
    hDevice.write = wFunc;

    /** Read status register after reset */
    uint32_t err_code = _ReadRegisters();
    if( err_code ) return err_code;

    /** @todo check contents */

    return 0;
}

uint32_t BQ25125_Status_Get(uint8_t* const status)
{
    uint32_t err_code = _ReadRegister(BQ25125_REG_SSMCR, &hDevice.Registers.SSMCR);
    if( err_code ) return err_code;

    if( status ) *status = hDevice.Registers.SSMCR;

    return err_code;
}

void BQ25125_SYSVOUT_Get(BQ25125_SYSVOUT_t* const vout)
{
    *vout = (BQ25125_SYSVOUT_t) ( 0x3F & (hDevice.Registers.SYSVOCR >> 1));
}

uint32_t BQ25125_SYSVOUT_Set(const BQ25125_SYSVOUT_t vout)
{
    uint8_t wData[2] = {0};
    wData[0] = BQ25125_REG_SYSVOCR;
    wData[1] = (hDevice.Registers.SYSVOCR & 0x81) | (((uint8_t) vout) << 1);

    uint32_t err_code = hDevice.write(wData, 2, false);
    if( err_code ) return err_code;

    err_code = _ReadRegister(BQ25125_REG_SYSVOCR, &hDevice.Registers.SYSVOCR);
    if( err_code ) return err_code;

    BQ25125_SYSVOUT_t resVout;
    BQ25125_SYSVOUT_Get(&resVout);
    return !(vout == resVout);
}

static uint32_t _ReadRegister(const uint8_t addr, uint8_t* const pReg)
{
    uint32_t err_code = hDevice.write(&addr, 1, false);
    if( err_code ) return err_code;

    err_code = hDevice.read(pReg, 1);
    return err_code;
}

static uint32_t _ReadRegisters(void)
{
    uint8_t addr = BQ25125_REG_BASE;
    uint32_t err_code = hDevice.write(&addr, 1, false);
    if( err_code ) return err_code;

    err_code = hDevice.read((uint8_t*) &hDevice.Registers, sizeof(BQ25125_Registers_t));
    return err_code;
}
