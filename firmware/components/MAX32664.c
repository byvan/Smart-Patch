/**
 * @file MAX32664.c
 * @brief      Implementation of the interface to the Maxim MAX32664 device.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       October 2020
 */

/* Includes ------------------------------------------------------------------*/
#include <stdarg.h>
#include <stdlib.h>
#include "nordic_common.h"
#include "MAX32664.h"

#include "nrf_log.h"

/**
 * @addtogroup   MAX32664
 * @{
 */

/**
 * @defgroup   MAX32664_ic Internal Components
 * @{
 */

/* Private defines -----------------------------------------------------------*/

/*! Converts the MAX32664 command array into the helper struct */
#define CMD_CONVERT( x )            ((MAX32664_Command_t) x)
/*! Assigns a given command helper struct new values */
#define CMD_ASSIGN( cmd, ncmd )                                     \
                                do {                                \
                                    MAX32664_Command_t _cmd = ncmd; \
                                    cmd.famByte = _cmd.famByte;     \
                                    cmd.idxByte = _cmd.idxByte;     \
                                } while(0)


/* Private macros ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/**
 * @brief      Helper struct to collect device information.
 */
typedef struct {
    bool        blValid;        /*!< Flag indicating, if the bootloader information is valid */
    uint8_t     blVersion[3];   /*!< Bootloader version number in the form MAJ.MIN.VER */
    uint16_t    pagesize;       /*!< Size of a page when performing a DFU */
    uint8_t     pfType;         /*!< Platform type */

    bool        fwValid;        /*!< Flag indicating, if the firmware information is valid */
    uint8_t     fwVersion[3];   /*!< Firmware version number in the form MAJ.MIN.VER */
} MAX32664_DeviceInfo_t;

/**
 * @brief      Helper struct to assemble commands to the MAX32664.
 */
typedef struct {
    uint8_t famByte;        /*!< The command family byte */
    uint8_t idxByte;        /*!< The command index byte */
    uint8_t addr;           /*!< The command address byte */
    bool    useAddr;        /*!< Whether to use the command address byte */
} MAX32664_Command_t;

/**
 * @brief      Internal handler struct.
 */
typedef struct {
    uint8_t                 Status;     /*!< The Status byte of the MAX32664 */
    MAX32664_DeviceMode_t   Mode;       /*!< The device operating mode */
    MAX32664_DeviceInfo_t   Info;       /*!< Collection of device information */
} MAX32664_DeviceState_t;

/* Private function prototypes -----------------------------------------------*/

/**
 * @brief      Reads the boot mode of the device.
 *
 *             The current mode is written into the device state.
 *
 * @return     Any error that comes up.
 */
static uint32_t get_mode(void);

/**
 * @brief      Reads the bootloader information from the device.
 *
 * @return     Any error that comes up.
 */
static uint32_t read_bl_info(void);

/**
 * @brief      Performs a generic read process with the given command as argument.
 *
 * @param[in]  cmd     The command to be used.
 * @param[in]  nBytes  The number of bytes to be read in @p pData.
 * @param      pData   Pointer to the data buffer to write into.
 *
 * @return     Any error that comes up.
 */
static uint32_t read_data(const MAX32664_Command_t* cmd, const uint32_t nBytes, uint8_t* pData);

/**
 * @brief      Reads the device mode and stores it in the device handler.
 *
 * @return     Any error that comes up.
 */
static uint32_t read_device_mode(void);

static uint32_t read_fw_info(void);

/**
 * @brief      Read just the status byte.
 *
 * @param[in]  issue  if set, the command to read is issued.
 *
 * @return     Any error that comes up.
 */
static uint32_t read_status(bool issue);

/**
 * @brief      Performs a generic write process using the provided command.
 *
 * @param[in]  cmd     The command to be used.
 * @param[in]  wData   The data to be written.
 * @param[in]  nBytes  The number of bytes to be written.
 * @param[in]  wait    The time to wait after the write process in ms.
 *
 * @return     Any error that comes up.
 */
static uint32_t write_data(const MAX32664_Command_t* cmd, const uint8_t* wData, const uint32_t nBytes, const uint32_t wait);

/* Private variables ---------------------------------------------------------*/

/*! Internal device handler */
static MAX32664_DeviceState_t Hub = { .Status = 0x00, .Mode = MAX32664_RESET_MODE };

/**
 * @}
 */

/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/

uint32_t MAX32664_Configure(const MAX32664_Config_t* const cfg)
{
    ((void)cfg);
    return -1;

    // uint32_t err_code = 0;

    // /** Set output mode */
    // err_code = _setOutputMode(cfg->outputMode);
    // if( err_code ) {
    //     NRF_LOG_WARNING("ERROR here %u", err_code);
    //     return err_code;
    // }

    // /** Set FIFO threshold */
    // if( cfg->fifoThreshold ) {
    //     err_code = _setFifoThreshold(cfg->fifoThreshold);
    //     if( err_code ) return err_code; 
    // }

    // return err_code;
}

uint32_t MAX32664_EraseMemory(void)
{
    uint32_t err_code = 0;

    MAX32664_Command_t cmd = MAX32664_BOOTCMD_ERASE;
    err_code = write_data(&cmd, 0, 0, 1400);
    if( err_code )  return err_code;

    return read_status(false);
}

uint32_t MAX32664_Init(bool* const upgradeRequired)
{
    uint32_t err_code;

    /* Hard-reset the MAX32664 */
    NRF_LOG_DEBUG("\tBooting MAX32664...");
    MAX32664_Reset(MAX32664_APPLICATION_MODE);
    NRF_LOG_DEBUG("\t\tOk.");

    /** Read mode. */
    err_code = get_mode();
    if( err_code )  return err_code;
    NRF_LOG_DEBUG("\tDevice in mode %u", Hub.Mode);

    /* Device is in bootloader mode, likely needs a DFU. */
    if( Hub.Mode != MAX32664_APPLICATION_MODE ) {
        NRF_LOG_DEBUG("\tSetting device into application mode...");

        err_code = MAX32664_SetDeviceMode(MAX32664_APPLICATION_MODE);

        if( err_code ) {
            NRF_LOG_DEBUG("\t\tfailed!");

            if( err_code != -1 )    return err_code;

            /* Device needs a DFU */
            *upgradeRequired = true;
            NRF_LOG_DEBUG("\tDevice needs to be upgraded!");
        }
        else {
            *upgradeRequired = false;
            NRF_LOG_DEBUG("\t\tOk.");
        }
    }

    return read_status(true);
}

uint32_t MAX32664_GetBLVersion(uint8_t* version)
{
    uint32_t err_code = 0;

    /* BL version not yet read */
    if( !Hub.Info.blValid ) {
        err_code = read_bl_info();
    }

    memcpy(version, Hub.Info.blVersion, 3);
    return err_code;
}

MAX32664_DeviceMode_t MAX32664_GetDeviceMode(void)
{
    return Hub.Mode;
}

uint32_t MAX32664_GetFWVersion(uint8_t* version, bool reload)
{
    uint32_t err_code = 0;

    /* Firmware version not read yet */
    if( !Hub.Info.fwValid || reload ) {
        err_code = read_fw_info();
        if( err_code )  return err_code;
    }

    memcpy(version, Hub.Info.fwVersion, 3);
    return err_code;
}

uint32_t MAX32664_SetAlgorithm(const MAX32664_AlgoMode_t mode)
{
    MAX32664_Command_t cmd = MAX32664_CMD_BPT_ENABLE;
    cmd.addr = (uint8_t) mode;
    cmd.useAddr = true;

    uint32_t err_code = write_data(&cmd, 0, 0, 20);
    if( err_code )  return err_code;

    return read_status(false);
}

uint32_t MAX32664_SetAuthBytes(const uint8_t* pData)
{
    MAX32664_Command_t cmd = MAX32664_BOOTCMD_AUTH;
    uint32_t err_code = write_data(&cmd, pData, 16, 0);
    if( err_code )  return err_code;

    return read_status(false);
}

uint32_t MAX32664_SetDeviceMode(const MAX32664_DeviceMode_t mode)
{
    uint32_t err_code = 0;

    /* Device already in the desired mode */
    if( Hub.Mode == mode )  return err_code;


    /* Otherwise perform write operation */
    NRF_LOG_DEBUG("\tSetting device mode %u.", (uint8_t) mode);

    MAX32664_Command_t cmd = MAX32664_CMD_DEVMODE_SET;
    cmd.addr = (uint8_t) mode;
    cmd.useAddr = true;

    err_code = write_data(&cmd, 0, 0, ((mode == MAX32664_APPLICATION_MODE) ? 1000 : 0));
    if( err_code ) {
        NRF_LOG_DEBUG("\t\tfailed!");
        return err_code;
    }

    err_code = read_status(false);
    if( err_code )  return err_code;

    err_code = read_device_mode();
    if( err_code )  return err_code;

    return (Hub.Mode == mode) ? 0 : -1;
}

uint32_t MAX32664_SetFlashPages(uint8_t pages)
{
    MAX32664_Command_t cmd = MAX32664_BOOTCMD_PAGE;
    cmd.addr = 0x00;
    cmd.useAddr = true;

    uint32_t err_code = write_data(&cmd, &pages, 1, 0);
    if( err_code ) return err_code;

    return read_status(false);
}

uint32_t MAX32664_SetIVBytes(const uint8_t* pData)
{
    MAX32664_Command_t cmd = MAX32664_BOOTCMD_INIT;
    uint32_t err_code = write_data(&cmd, pData, 11, 0);
    if( err_code )  return err_code;

    return read_status(false);
}

uint32_t MAX32664_SetSensor(const MAX32664_EnabledState_t state)
{
    MAX32664_Command_t cmd = MAX32664_CMD_SENS_ENABLE;
    cmd.addr = (uint8_t) state;
    cmd.useAddr = true;

    uint32_t err_code = write_data(&cmd, 0, 0, 40);
    if( err_code )  return err_code;

    return read_status(false);
}

/* Private functions ---------------------------------------------------------*/

static uint32_t get_mode(void)
{
    MAX32664_Command_t cmd = MAX32664_CMD_DEVMODE_READ;
    uint8_t rData[2] = {0};
    uint32_t err_code = read_data(&cmd, 2, rData);
    if( err_code ) return err_code;

    Hub.Mode = (MAX32664_DeviceMode_t) rData[1];
    return err_code;
}

static uint32_t read_bl_info(void)
{
    uint32_t err_code = 0;
    uint8_t rData[4] = {0};
    MAX32664_Command_t cmd;

    /* Read platform type */
    CMD_ASSIGN(cmd, MAX32664_CMD_MCUTYPE_READ);
    err_code = read_data(&cmd, 2, rData);
    if( err_code )  return err_code;

    Hub.Info.pfType = rData[1];
    NRF_LOG_DEBUG("\tRead platform type: %u", Hub.Info.pfType);

    /* Read BL Version */
    CMD_ASSIGN(cmd, MAX32664_BOOTCMD_INFO);
    err_code = read_data(&cmd, 4, rData);
    if( err_code )  return err_code;

    memcpy(Hub.Info.blVersion, &rData[1], 3);
    NRF_LOG_DEBUG("\tRead BL version:    %u.%u.%u", Hub.Info.blVersion[0], Hub.Info.blVersion[1], Hub.Info.blVersion[2]);

    /* Read Page Size */
    CMD_ASSIGN(cmd, MAX32664_BOOTCMD_PAGESIZE);
    err_code = read_data(&cmd, 3, rData);
    if( err_code )  return err_code;

    uint16_t ps = rData[1];
    ps = (ps << 8) | rData[2];
    Hub.Info.pagesize = ps;
    NRF_LOG_DEBUG("\tRead page size:     %u", Hub.Info.pagesize);

    Hub.Info.blValid = true;

    return err_code;
}

static uint32_t read_data(const MAX32664_Command_t* cmd, const uint32_t nBytes, uint8_t* pData)
{
    uint32_t err_code = write_data(cmd, 0, 0, 0);
    if( err_code ) return err_code;

    return read_status(false);
}

static uint32_t read_device_mode(void)
{
    uint32_t err_code = 0;
    uint8_t rData[2] = {0};
    MAX32664_Command_t cmd = MAX32664_CMD_DEVMODE_READ;
    
    err_code = read_data(&cmd, 2, rData);
    if( err_code ) return err_code;

    Hub.Mode = (MAX32664_DeviceMode_t) rData[1];
    return err_code;
}

static uint32_t read_fw_info(void)
{
    MAX32664_Command_t cmd = MAX32664_CMD_HUBVERSION_READ;
    uint8_t rData[4] = {0};
    uint32_t err_code = read_data(&cmd, 4, rData);
    if( err_code )  return err_code;

    memcpy(&Hub.Info.fwVersion, &rData[1], 3);
    Hub.Info.fwValid = true;

    return err_code;
}

static uint32_t read_status(bool issue)
{
    uint32_t err_code = 0;

    if( issue ) {
        MAX32664_Command_t cmd = MAX32664_CMD_STATUS_READ;
        err_code = write_data(&cmd, 0, 0, 0);
        if( err_code )  return err_code;
    }

    err_code = MAX32664_ReadData(1, &Hub.Status);
    if( err_code )  return err_code;

    return (Hub.Status ? (0xF000 | Hub.Status) : Hub.Status );
}

static uint32_t write_data(const MAX32664_Command_t* cmd, const uint8_t* wData, const uint32_t nBytes, const uint32_t wait)
{
    uint8_t* wHeader;

    if( cmd->useAddr ) {
        wHeader = (uint8_t*) malloc(3 + nBytes);
        wHeader[2] = cmd->addr;
        memcpy(wHeader+3, wData, nBytes);
    }
    else {
        wHeader = (uint8_t*) malloc(2 + nBytes);
        memcpy(wHeader+2, wData, nBytes);
    }

    wHeader[0] = cmd->famByte;
    wHeader[1] = cmd->idxByte;

    uint32_t err_code = MAX32664_WriteData(wHeader, (cmd->useAddr ? 3 : 2) + nBytes, false);
    free(wHeader);
    if( err_code ) return err_code;

    MAX32664_Wait( ((wait > MAX32664_CMD_DELAY_MS) ? wait : MAX32664_CMD_DELAY_MS ));
    return err_code;
}













#include "firstpage.h"

uint32_t MAX32664_DFU_FlashPage(const uint8_t* pData)
{
    int32_t err_code = 0;
    // memcpy(hDevice.pDFUBuffer, pData, 128 + 8);

    // // if( hDevice.pDFUBuffer == hDevice.DFUBuffer ) {
    // //     /* Buffer was empty */
    // //     hDevice.pDFUBuffer = &hDevice.DFUBuffer[128 + 8];
    // //     NRF_LOG_WARNING("Copied 1st segment");
    // //     return err_code;
    // // }
    // // else {
    // //     /* First half-page already copied */
    // //     hDevice.pDFUBuffer = hDevice.DFUBuffer;
    // //     NRF_LOG_WARNING("Copied 2nd segment");
    // // }

    MAX32664_Command_t cmd = MAX32664_BOOTCMD_SEND;
    // err_code = _writeGeneric(&cmd, pData, hDevice.BLInfo.PageSize + 16, 680);
    
    /* @todo calculate CRC */
    // err_code = write_data(&cmd, pData, 8192 + 16, 680);
    err_code = write_data(&cmd, __page_data__, 8192 + 16, 680);
    if( err_code )  return err_code;

    return read_status(false);
}







uint32_t MAX32664_GetFifoCount(uint8_t* const count)
{
    ((void)count);
    return 0;

    // uint32_t err_code = 0;
    // MAX32664_Command_t cmd = MAX32664_CMD_FIFOLEVEL_READ;

    // uint8_t rData[2] = {0};
    // err_code = _readGeneric(&cmd, 2, rData);

    // *count = rData[1];
    // return err_code;
}

uint32_t MAX32664_GetPageSize(uint16_t* const size)
{
    ((void)size);
    return 0;

    // uint32_t err_code = 0;

    // /* Firmware version not read yet */
    // if( !hDevice.BLInfo.Valid ) {
    //     err_code = _readFWInfo();
    //     if( err_code )  return err_code;
    // }

    // *size = hDevice.BLInfo.PageSize;
    // return err_code;
}

uint32_t MAX32664_GetSensorData(uint8_t* pData, const uint8_t nSamples)
{
    ((void)pData);
    ((void)nSamples);
    return 0;
    // MAX32664_Command_t cmd = MAX32664_CMD_FIFO_READ;
    // return _readGeneric(&cmd, (nSamples * 23) + 1, pData);
}

uint32_t MAX32664_GetStatus(void)
{
    return 0;
    // return MAX32664_ReadData(1, &hDevice.DeviceStatus);
}



void MAX32664_InterruptReceived(void)
{
    // hDevice.InterruptPending++;// = true;
}

bool MAX32664_InterruptPending(bool clear)
{
    ((void)clear);
    return 0;

    // bool val = hDevice.InterruptPending;

    // if( clear && val ) {
    //     hDevice.InterruptPending--;// = false;
    // }

    // return val;
}

uint32_t MAX32664_ReadSensorRegister(const uint8_t addr, uint8_t* const pData)
{
    ((void)pData);
    ((void)addr);

    return 0;

    // MAX32664_Command_t cmd = MAX32664_CMD_SENSADREG_READ;
    // cmd.addr = addr;
    // cmd.useAddr = true;

    // uint8_t rData[2] = {0};
    // uint32_t err_code = _readGeneric(&cmd, 2, rData);
    // if( err_code ) return err_code;

    // *pData = rData[1];
    // return err_code;
}



uint32_t MAX32664_SetAGC(const MAX32664_EnabledState_t state)
{
    ((void)state);
    return 0;

    // MAX32664_Command_t cmd = MAX32664_CMD_AGC_ENABLE;
    // cmd.addr = (uint8_t) state;
    // cmd.useAddr = true;

    // return _writeGeneric(&cmd, 0, 0, 100);
}











uint32_t MAX32664_WriteSensorRegister(const uint8_t* pData)
{
    ((void)pData);
    return 0;

    // MAX32664_Command_t cmd = MAX32664_CMD_SENSAD_WRITE;
    // return _writeGeneric(&cmd, pData, 2, 100);
}



#if 0










typedef struct {
    bool        isValid;
    uint8_t     FWVersion[3];
} MAX32664_FWInfo_t;

typedef struct {
    uint8_t                 DeviceStatus;
    MAX32664_DeviceMode_t   DeviceMode;
    uint8_t                 InterruptPending;
    MAX32664_BLInfo_t       BLInfo;
    MAX32664_FWInfo_t       FWInfo;

    uint8_t                 DFUBuffer[256+16];
    uint8_t*                pDFUBuffer;
} MAX32664_DeviceState_t;















uint32_t MAX32664_Init()
{
    uint32_t err_code;

    hDevice.InterruptPending = 0;
    hDevice.pDFUBuffer = hDevice.DFUBuffer;

    /* Hard-reset the MAX32664 */
    NRF_LOG_DEBUG("\tBooting MAX32664...");
    MAX32664_Reset(MAX32664_APPLICATION_MODE);
    NRF_LOG_DEBUG("\t\tOk.");

    /** Read mode. */
    err_code = get_mode();










    

    if( err_code ) {
        /* If communication failed, try hard-resetting the device into application mode. */
        MAX32664_Reset(MAX32664_APPLICATION_MODE);
        err_code = _readMode();
        if( err_code ) return err_code;
    }
    
    /** Set device in application mode. */
    if( hDevice.DeviceMode != MAX32664_APPLICATION_MODE ) {

        /** Take device out of firmware update mode */
        err_code = _writeMode(MAX32664_APPLICATION_MODE);
        if( err_code ) return err_code;

        err_code = _readMode();
        if( err_code ) return err_code;

        err_code = ((hDevice.DeviceMode == MAX32664_APPLICATION_MODE) ? 0 : MAX32664_ERR_UNKNOWN );
        return err_code;

        #ifdef MAX_FLASH_ENABLED
        /** @todo implement firmware flashing */
            /** Read platform type */
            MAX32664_Command_t cmd = MAX32664_CMD_MCUTYPE_READ;
            uint8_t rData[4] = {0};
            err_code = _readGeneric(&cmd, 2, rData);
            if( err_code ) return err_code;
            err_code = ((rData[1] == 0x01) ? 0 : MAX32664_ERR_UNKNOWN );

            NRF_LOG_WARNING("PF: %u", rData[1]);

            /** Read bootloader version */
            cmd = (MAX32664_Command_t) MAX32664_BOOTCMD_INFO;
            err_code = _readGeneric(&cmd, 4, rData);
            if( err_code ) return err_code;

            NRF_LOG_WARNING("BL Version: %u.%u.%u", rData[1], rData[2], rData[3]);
        #endif        
    }

    /** Read device status */
    err_code = _readStatus();
    NRF_LOG_WARNING("\tStatus: 0x%02X", hDevice.DeviceStatus);

    return hDevice.DeviceStatus;
}









/**
 * @brief      Sets the fifo threshold.
 *
 * @param[in]  th    The new value
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t _setFifoThreshold(const uint8_t th);

/**
 * @brief      Sets the output mode.
 *
 * @param[in]  mode  The output mode to use.
 *
 * @return     { description_of_the_return_value }
 */
static uint32_t _setOutputMode(const uint8_t mode);






uint32_t MAX32664_Configure(const MAX32664_Config_t* const cfg)
{
    uint32_t err_code = 0;

    /** Set output mode */
    err_code = _setOutputMode(cfg->outputMode);
    if( err_code ) {
        NRF_LOG_WARNING("ERROR here %u", err_code);
        return err_code;
    }

    /** Set FIFO threshold */
    if( cfg->fifoThreshold ) {
        err_code = _setFifoThreshold(cfg->fifoThreshold);
        if( err_code ) return err_code; 
    }

    return err_code;
}

uint32_t MAX32664_DFU_FlashPage(const uint8_t* pData)
{
    uint32_t err_code = 0;
    // memcpy(hDevice.pDFUBuffer, pData, 128 + 8);

    // if( hDevice.pDFUBuffer == hDevice.DFUBuffer ) {
    //     /* Buffer was empty */
    //     hDevice.pDFUBuffer = &hDevice.DFUBuffer[128 + 8];
    //     NRF_LOG_WARNING("Copied 1st segment");
    //     return err_code;
    // }
    // else {
    //     /* First half-page already copied */
    //     hDevice.pDFUBuffer = hDevice.DFUBuffer;
    //     NRF_LOG_WARNING("Copied 2nd segment");
    // }

    MAX32664_Command_t cmd = MAX32664_BOOTCMD_SEND;
    // err_code = _writeGeneric(&cmd, pData, hDevice.BLInfo.PageSize + 16, 680);
    err_code = _writeGeneric(&cmd, pData, 8192 + 16, 680);
    if( err_code )  return err_code;

    err_code = MAX32664_ReadData(1, &hDevice.DeviceStatus);
    if( err_code )  return err_code;

    return hDevice.DeviceStatus;
}

uint32_t MAX32664_EraseMemory(void)
{
    uint32_t err_code = 0;

    MAX32664_Command_t cmd = MAX32664_BOOTCMD_ERASE;
    err_code = _writeGeneric(&cmd, 0, 0, 1400);
    if( err_code )  return err_code;

    err_code = MAX32664_ReadData(1, &hDevice.DeviceStatus);
    if( err_code )  return err_code;

    return hDevice.DeviceStatus;
}

uint32_t MAX32664_GetBLVersion(uint8_t* version)
{
    uint32_t err_code = 0;

    NRF_LOG_WARNING("Here ok");

    /* BL version not read yet */
    if( !hDevice.BLInfo.Valid ) {
        err_code = _readBLInfo();
        NRF_LOG_WARNING("Here ok %u", err_code);
        if( err_code )  return err_code;
    }

    memcpy(version, hDevice.BLInfo.Version, 3);
    return err_code;



    // if( !hDevice.FWversion[0] ) {
    //     err_code = _readFWVersion();
    //     if( err_code )  return err_code;
    // }

    // memcpy(version, hDevice.FWversion, 3);
    // return err_code;
}


uint32_t MAX32664_GetFifoCount(uint8_t* const count)
{
    uint32_t err_code = 0;
    MAX32664_Command_t cmd = MAX32664_CMD_FIFOLEVEL_READ;

    uint8_t rData[2] = {0};
    err_code = _readGeneric(&cmd, 2, rData);

    *count = rData[1];
    return err_code;
}

uint32_t MAX32664_GetFWVersion(uint8_t* version, bool reload)
{
    uint32_t err_code = 0;

    /* Firmware version not read yet */
    if( !hDevice.FWInfo.isValid || reload ) {
        err_code = _readFWInfo();
        if( err_code )  return err_code;
    }

    memcpy(version, hDevice.FWInfo.FWVersion, 3);
    return err_code;
}

uint32_t MAX32664_GetPageSize(uint16_t* const size)
{
    uint32_t err_code = 0;

    /* Firmware version not read yet */
    if( !hDevice.BLInfo.Valid ) {
        err_code = _readFWInfo();
        if( err_code )  return err_code;
    }

    *size = hDevice.BLInfo.PageSize;
    return err_code;
}

uint32_t MAX32664_GetSensorData(uint8_t* pData, const uint8_t nSamples)
{
    MAX32664_Command_t cmd = MAX32664_CMD_FIFO_READ;
    return _readGeneric(&cmd, (nSamples * 23) + 1, pData);
}

uint32_t MAX32664_GetStatus(void)
{
    return MAX32664_ReadData(1, &hDevice.DeviceStatus);
}



void MAX32664_InterruptReceived(void)
{
    hDevice.InterruptPending++;// = true;
}

bool MAX32664_InterruptPending(bool clear)
{
    bool val = hDevice.InterruptPending;

    if( clear && val ) {
        hDevice.InterruptPending--;// = false;
    }

    return val;
}

uint32_t MAX32664_ReadSensorRegister(const uint8_t addr, uint8_t* const pData)
{
    MAX32664_Command_t cmd = MAX32664_CMD_SENSADREG_READ;
    cmd.addr = addr;
    cmd.useAddr = true;

    uint8_t rData[2] = {0};
    uint32_t err_code = _readGeneric(&cmd, 2, rData);
    if( err_code ) return err_code;

    *pData = rData[1];
    return err_code;
}

uint32_t MAX32664_SetAlgorithm(const MAX32664_AlgoMode_t mode)
{
    MAX32664_Command_t cmd = MAX32664_CMD_BPT_ENABLE;
    cmd.addr = (uint8_t) mode;
    cmd.useAddr = true;

    return _writeGeneric(&cmd, 0, 0, 20);
}

uint32_t MAX32664_SetAGC(const MAX32664_EnabledState_t state)
{
    MAX32664_Command_t cmd = MAX32664_CMD_AGC_ENABLE;
    cmd.addr = (uint8_t) state;
    cmd.useAddr = true;

    return _writeGeneric(&cmd, 0, 0, 100);
}

uint32_t MAX32664_SetAuthBytes(const uint8_t* pData)
{
    MAX32664_Command_t cmd = MAX32664_BOOTCMD_AUTH;
    uint32_t err_code = _writeGeneric(&cmd, pData, 16, 0);
    if( err_code )  return err_code;

    err_code = MAX32664_ReadData(1, &hDevice.DeviceStatus);
    if( err_code )  return err_code;

    return hDevice.DeviceStatus;
}

uint32_t MAX32664_SetFlashPages(uint8_t pages)
{
    MAX32664_Command_t cmd = MAX32664_BOOTCMD_PAGE;
    cmd.addr = 0x00;
    cmd.useAddr = true;

    uint32_t err_code = _writeGeneric(&cmd, &pages, 1, 0);
    if( err_code ) return err_code;

    err_code = MAX32664_ReadData(1, &hDevice.DeviceStatus);
    if( err_code )  return err_code;

    return hDevice.DeviceStatus;
}

uint32_t MAX32664_SetIVBytes(const uint8_t* pData)
{
    MAX32664_Command_t cmd = MAX32664_BOOTCMD_INIT;
    uint32_t err_code = _writeGeneric(&cmd, pData, 11, 0);
    if( err_code )  return err_code;

    err_code = MAX32664_ReadData(1, &hDevice.DeviceStatus);
    if( err_code )  return err_code;

    return hDevice.DeviceStatus;
}

uint32_t MAX32664_SetSensor(const MAX32664_EnabledState_t state)
{
    MAX32664_Command_t cmd = MAX32664_CMD_SENS_ENABLE;
    cmd.addr = (uint8_t) state;
    cmd.useAddr = true;

    return _writeGeneric(&cmd, 0, 0, 40);
}

uint32_t MAX32664_WriteSensorRegister(const uint8_t* pData)
{
    MAX32664_Command_t cmd = MAX32664_CMD_SENSAD_WRITE;
    return _writeGeneric(&cmd, pData, 2, 100);
}

static uint32_t _readBLInfo(void)
{
    
}





static uint32_t _readStatus(void)
{
    MAX32664_Command_t cmd = MAX32664_CMD_STATUS_READ;

    uint32_t err_code = _writeGeneric(&cmd, 0, 0, 0);
    if( err_code ) return err_code;

    err_code = MAX32664_ReadData(1, &hDevice.DeviceStatus);
    return err_code;
}



static uint32_t _setFifoThreshold(const uint8_t th)
{
    /** Set the desired threshold */
    MAX32664_Command_t cmd = MAX32664_CMD_FIFOTH_SET;
    cmd.addr = th;
    cmd.useAddr = true;

    uint32_t err_code = _writeGeneric(&cmd, 0, 0, 0);
    if( err_code ) return err_code;

    /** Read back set value */
    cmd.famByte = CMD_CONVERT(MAX32664_CMD_FIFOTH_READ).famByte;
    cmd.idxByte = CMD_CONVERT(MAX32664_CMD_FIFOTH_READ).idxByte;
    cmd.useAddr = false;

    uint8_t rData[2] = {0};
    err_code = _readGeneric(&cmd, 2, rData);
    if( err_code ) return err_code;

    return (rData[1] == th ? MAX32664_ERR_NONE : MAX32664_ERR_UNKNOWN);
}

static uint32_t _setOutputMode(const uint8_t mode)
{
    /** Set the desired output mode */
    MAX32664_Command_t cmd = MAX32664_CMD_OUTMODE_SET;
    cmd.addr = mode;
    cmd.useAddr = true;
    
    uint32_t err_code = _writeGeneric(&cmd, 0, 0, 0);
    if( err_code ) return err_code;

    /** Read back set output mode */
    cmd.famByte = CMD_CONVERT(MAX32664_CMD_OUTMODE_READ).famByte;
    cmd.idxByte = CMD_CONVERT(MAX32664_CMD_OUTMODE_READ).idxByte;
    cmd.useAddr = false;

    uint8_t rData[2] = {0};
    err_code = _readGeneric(&cmd, 2, rData);
    if( err_code ) return err_code;

    return (rData[1] == mode ? MAX32664_ERR_NONE : MAX32664_ERR_UNKNOWN);
}





#endif
