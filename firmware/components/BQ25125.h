/**
 * @file BQ25125.h
 * @brief      Definitions for the interface to the TI BQ25125 device.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       October 2020
 */

#ifndef __BQ25125_H
#define __BQ25125_H

#include <stdbool.h>
#include <stdint.h>

/*! I2C slave read address */
#define BQ25125_ADDRESS_READ            0xD5
/*! I2C slave write address */
#define BQ25125_ADDRESS_WRITE           0xD4

#define BQ25125_REG_BVCR_3V6            0x00
#define BQ25125_REG_BVCR_3V7            0x0A
#define BQ25125_REG_BVCR_3V8            0x14
#define BQ25125_REG_BVCR_3V9            0x1E
#define BQ25125_REG_BVCR_4V0            0x28
#define BQ25125_REG_BVCR_4V1            0x32
#define BQ25125_REG_BVCR_4V2            0x3C

#define BQ25125_REG_LSLDO_ON            0x01
#define BQ25125_REG_LSLDO_OFF           0x00

#define BQ25125_REG_SYSVOUT_1V1         0x00
#define BQ25125_REG_SYSVOUT_1V2         0x01
#define BQ25125_REG_SYSVOUT_1V3         0x10
#define BQ25125_REG_SYSVOUT_1V4         0x11
#define BQ25125_REG_SYSVOUT_1V5         0x12
#define BQ25125_REG_SYSVOUT_1V6         0x13
#define BQ25125_REG_SYSVOUT_1V7         0x14
#define BQ25125_REG_SYSVOUT_1V8         0x15
#define BQ25125_REG_SYSVOUT_1V9         0x16
#define BQ25125_REG_SYSVOUT_2V0         0x17
#define BQ25125_REG_SYSVOUT_2V1         0x18
#define BQ25125_REG_SYSVOUT_2V2         0x19
#define BQ25125_REG_SYSVOUT_2V3         0x1A
#define BQ25125_REG_SYSVOUT_2V4         0x1B
#define BQ25125_REG_SYSVOUT_2V5         0x1C
#define BQ25125_REG_SYSVOUT_2V6         0x1D
#define BQ25125_REG_SYSVOUT_2V7         0x1E
#define BQ25125_REG_SYSVOUT_2V8         0x1F
#define BQ25125_REG_SYSVOUT_2V9         0x3B
#define BQ25125_REG_SYSVOUT_3V0         0x3C
#define BQ25125_REG_SYSVOUT_3V1         0x3D
#define BQ25125_REG_SYSVOUT_3V2         0x3E
#define BQ25125_REG_SYSVOUT_3V3         0x3F

typedef enum {
    BQ25125_BVCR_3V6 = BQ25125_REG_BVCR_3V6,
    BQ25125_BVCR_3V7 = BQ25125_REG_BVCR_3V7,
    BQ25125_BVCR_3V8 = BQ25125_REG_BVCR_3V8,
    BQ25125_BVCR_3V9 = BQ25125_REG_BVCR_3V9,
    BQ25125_BVCR_4V0 = BQ25125_REG_BVCR_4V0,
    BQ25125_BVCR_4V1 = BQ25125_REG_BVCR_4V1,
    BQ25125_BVCR_4V2 = BQ25125_REG_BVCR_4V2
} BQ25125_BVCR_t;

typedef enum {
    BQ25125_LSLDO_OFF = BQ25125_REG_LSLDO_OFF,
    BQ25125_LSLDO_ON = BQ25125_REG_LSLDO_ON
} BQ25125_LSLDO_t;

typedef enum {
    BQ25125_SYSVOUT_1V1 = BQ25125_REG_SYSVOUT_1V1,
    BQ25125_SYSVOUT_1V2 = BQ25125_REG_SYSVOUT_1V2,
    BQ25125_SYSVOUT_1V3 = BQ25125_REG_SYSVOUT_1V3,
    BQ25125_SYSVOUT_1V4 = BQ25125_REG_SYSVOUT_1V4,
    BQ25125_SYSVOUT_1V5 = BQ25125_REG_SYSVOUT_1V5,
    BQ25125_SYSVOUT_1V6 = BQ25125_REG_SYSVOUT_1V6,
    BQ25125_SYSVOUT_1V7 = BQ25125_REG_SYSVOUT_1V7,
    BQ25125_SYSVOUT_1V8 = BQ25125_REG_SYSVOUT_1V8,
    BQ25125_SYSVOUT_1V9 = BQ25125_REG_SYSVOUT_1V9,
    BQ25125_SYSVOUT_2V0 = BQ25125_REG_SYSVOUT_2V0,
    BQ25125_SYSVOUT_2V1 = BQ25125_REG_SYSVOUT_2V1,
    BQ25125_SYSVOUT_2V2 = BQ25125_REG_SYSVOUT_2V2,
    BQ25125_SYSVOUT_2V3 = BQ25125_REG_SYSVOUT_2V3,
    BQ25125_SYSVOUT_2V4 = BQ25125_REG_SYSVOUT_2V4,
    BQ25125_SYSVOUT_2V5 = BQ25125_REG_SYSVOUT_2V5,
    BQ25125_SYSVOUT_2V6 = BQ25125_REG_SYSVOUT_2V6,
    BQ25125_SYSVOUT_2V7 = BQ25125_REG_SYSVOUT_2V7,
    BQ25125_SYSVOUT_2V8 = BQ25125_REG_SYSVOUT_2V8,
    BQ25125_SYSVOUT_2V9 = BQ25125_REG_SYSVOUT_2V9,
    BQ25125_SYSVOUT_3V0 = BQ25125_REG_SYSVOUT_3V0,
    BQ25125_SYSVOUT_3V1 = BQ25125_REG_SYSVOUT_3V1,
    BQ25125_SYSVOUT_3V2 = BQ25125_REG_SYSVOUT_3V2,
    BQ25125_SYSVOUT_3V3 = BQ25125_REG_SYSVOUT_3V3
} BQ25125_SYSVOUT_t;

/*! Typedef for the read function pointer */
typedef uint32_t (*BQ25125_Read_t)(uint8_t*, const uint32_t);
/*! Typedef for the write function pointer */
typedef uint32_t (*BQ25125_Write_t)(const uint8_t*, const uint32_t, bool);


/**
 * @brief      Retrieves the Battery Voltage Control Register.
 *
 * @param[out] bvcr  The controlled battery voltage.
 */
void BQ25125_BVCR_Get(BQ25125_BVCR_t* const bvcr);

/**
 * @brief      Sets the Battery Voltage Control Register.
 *
 * @param[in]  bvcr  The controlled battery voltage.
 *
 * @return     { description_of_the_return_value }
 */
uint32_t BQ25125_BVCR_Set(const BQ25125_BVCR_t bvcr);

/**
 * @brief      Retrieves the LS/LDO output state.
 *
 * @param[out] lsldo  The output state of the LS/LDO.
 */
void BQ25125_LSLDO_Get(BQ25125_LSLDO_t* const lsldo);

/**
 * @brief      Sets the enabling state of the LS/LDO output.
 *
 * @param[in]  lsldo  The lsldo
 *
 * @return     { description_of_the_return_value }
 */
uint32_t BQ25125_LSLDO_Set(const BQ25125_LSLDO_t lsldo);

/**
 * @brief      Initialises the BQ25125 device.
 *
 * @return     { description_of_the_return_value }
 */
uint32_t BQ25125_Init(BQ25125_Read_t rFunc, BQ25125_Write_t wFunc);

/**
 * @brief      Retrieves the status of the device.
 *
 * @param[out] status  Pointer to the status buffer.
 *
 * @return     { description_of_the_return_value }
 */
uint32_t BQ25125_Status_Get(uint8_t* const status);

/**
 * @brief      Retrieves the SYS VOUT register.
 *
 * @param[out] vout  The voltage set for the SYS output.
 */
void BQ25125_SYSVOUT_Get(BQ25125_SYSVOUT_t* const vout);

/**
 * @brief      Sets the SYS VOUT registers.
 *
 * @param[in]  vout  The output voltage for the SYS output.
 *
 * @return     { description_of_the_return_value }
 */
uint32_t BQ25125_SYSVOUT_Set(const BQ25125_SYSVOUT_t vout);

#endif /* __BQ25125_H included */
