/**
 * @file sp_ble.c
 * @brief      Smart Patch BLE implementation.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       October 2020
 */

/* Includes ------------------------------------------------------------------*/
#include <string.h>

#include "app_error.h"
#include "app_timer.h"
#include "app_util.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "ble_conn_state.h"
#include "ble_gap.h"
#include "fds.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "nrf_log.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "peer_manager.h"

#include "macros.h"
#include "sp_ble.h"
#include "sp_board.h"
#include "sp_services.h"

/**
 * @addtogroup   SP_BLE
 * @{
 */

/**
 * @defgroup   SP_BLE_ic Internal Components
 * @{
 */

/* Private defines -----------------------------------------------------------*/

/*! A tag identifying the SoftDevice BLE configuration. */
#define APP_BLE_CONN_CFG_TAG            1
/*! Minimum acceptable connection interval (0.5 seconds).  */
#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(500, UNIT_1_25_MS)
/*! Maximum acceptable connection interval (1 second). */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(1000, UNIT_1_25_MS)
/*! Slave latency. */
#define SLAVE_LATENCY                   0

/*! Connection supervisory timeout (4 seconds). */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)
/*! The advertising interval (in units of 0.625 ms. This value corresponds to 187.5 ms). */
#define APP_ADV_INTERVAL                300
/*! The advertising duration (180 seconds) in units of 10 milliseconds. */
#define APP_ADV_DURATION                18000

/*! Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)
/*! Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)
/*! Number of attempts before giving up the connection parameter negotiation. */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3

/*! Perform bonding. */
#define SEC_PARAM_BOND                  1
/*! Man In The Middle protection not required. */
#define SEC_PARAM_MITM                  0
/*! LE Secure Connections not enabled. */
#define SEC_PARAM_LESC                  0
/*! Keypress notifications not enabled. */
#define SEC_PARAM_KEYPRESS              0
/*! No I/O capabilities. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE
/*! Out Of Band data not available. */
#define SEC_PARAM_OOB                   0
/*! Minimum encryption key size. */
#define SEC_PARAM_MIN_KEY_SIZE          7
/*! Maximum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16

/*! Application's BLE observer priority. You shouldn't need to modify this value. */
#define APP_BLE_OBSERVER_PRIO           3

/* Private macros ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/**
 * @brief      Function for initializing the Advertising functionality.
 *
 * @return     Any error that comes up.
 */
static uint32_t advertising_init(void);

/**
 * @brief      Function for starting advertising.
 *
 * @param[in]  erase_bonds  The erase bonds
 *
 * @return     Any error that comes up.
 */
static uint32_t advertising_start(bool erase_bonds);

/**
 * @brief      Function for initializing the Connection Parameters module.
 *
 * @return     Any error that comes up.
 */
static uint32_t conn_params_init(void);

/**
 * @brief      Function for the GAP initialization.
 *
 * @details    This function sets up all the necessary GAP (Generic Access
 *             Profile) parameters of the device including the device name,
 *             appearance, and the preferred connection parameters.
 *
 * @return     Any error that comes up.
 */
static uint32_t gap_params_init(void);

/**
 * @brief      Function for initializing the GATT module.
 *
 * @return     Any error that comes up.
 */
static uint32_t gatt_init(void);

/**
 * @brief      Function for handling advertising events.
 *
 * @details    This function will be called for advertising events which are
 *             passed to the application.
 *
 * @param[in]  ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt);

/**
 * @brief      Function for handling BLE events.
 *
 * @param[in]  p_ble_evt  Bluetooth stack event.
 * @param[in]  p_context  Unused.
 */
static void on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context);

/**
 * @brief      Function for handling a Connection Parameters error.
 *
 * @param[in]  nrf_error  Error code containing information about what went
 *                        wrong.
 */
static void on_conn_params_err(uint32_t nrf_error);

/**
 * @brief      Function for handling the Connection Parameters Module.
 *
 * @details    This function will be called for all events in the Connection
 *             Parameters Module which are passed to the application.
 * @note       All this function does is to disconnect. This could have been
 *             done by simply setting the disconnect_on_fail config parameter,
 *             but instead we use the event handler mechanism to demonstrate its
 *             use.
 *
 * @param[in]  p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt);

/**
 * @brief      Function for handling Peer Manager events.
 *
 * @param[in]  p_evt  Peer Manager event.
 */
static void on_pm_evt(pm_evt_t const * p_evt);

/**
 * @brief      Function for handling Queued Write Module errors.
 *
 * @details    A pointer to this function will be passed to each service which
 *             may need to inform the application about an error.
 *
 * @param[in]  nrf_error  Error code containing information about what went
 *                        wrong.
 */
static void on_qwr_err(uint32_t nrf_error);

/**
 * @brief      Function for the Peer Manager initialization.
 *
 * @return     Any error that comes up.
 */
static uint32_t peer_manager_init(void);

/**
 * @brief      Function for initializing services that will be used by the
 *             application.
 *
 * @param[in]  cfg   The configuration to be used.
 *
 * @return     Any error that comes up.
 */
static uint32_t services_init(const SP_BLEcfg_t* const cfg);

/* Private variables ---------------------------------------------------------*/

/*! GATT module instance. */
NRF_BLE_GATT_DEF(hgatt);
/*! Context for the Queued Write module.*/
NRF_BLE_QWR_DEF(hqwr);
/*! Advertising module instance. */
BLE_ADVERTISING_DEF(hadv);
/*! Handle of the current connection. */
static uint16_t hconn = BLE_CONN_HANDLE_INVALID;

/*! Service handler for the configuration service. */
static SP_Service_Handler_t hServ_config = {0};

/*! Register a handler for BLE events. */
NRF_SDH_BLE_OBSERVER(hobs_ble, APP_BLE_OBSERVER_PRIO, on_ble_evt, NULL);
/*! Register a handler for CFG events. */
NRF_SDH_BLE_OBSERVER(hobs_cfg, APP_BLE_OBSERVER_PRIO, on_cfg_evt, (void*) &hServ_config);

/*! Universally unique service identifiers. */
static ble_uuid_t adv_uuids[] = {
    // {BLE_UUID_DEVICE_INFORMATION_SERVICE, BLE_UUID_TYPE_BLE},
    {BLE_UUID_BATTERY_SERVICE, BLE_UUID_TYPE_BLE}
    // {SPS_SP_SERVICE, BLE_UUID_TYPE_VENDOR_BEGIN},
    // {SPS_ACQ_SERVICE, BLE_UUID_TYPE_VENDOR_BEGIN}
    // {BLE_UUID_HEART_RATE_SERVICE, BLE_UUID_TYPE_VENDOR_BEGIN}
    // {BLE_UUID_HEART_RATE_SERVICE, BLE_UUID_TYPE_BLE}
};

/**
 * @}
 */

/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/

uint32_t SP_BLE_Init(const SP_BLEcfg_t* const cfg)
{
    /* Enable power management. */
    RET_ON_ERROR( nrf_pwr_mgmt_init() );

    /* Initialise SD */
    RET_ON_ERROR( nrf_sdh_enable_request() );

    /* Configure the BLE stack using the default settings. Fetch the start
     * address of the application RAM. */
    uint32_t ram_start = 0;
    RET_ON_ERROR( nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start) );

    /* Enable BLE stack */
    RET_ON_ERROR( nrf_sdh_ble_enable(&ram_start) );

    /* Configure SD */
    RET_ON_ERROR( gap_params_init() );
    RET_ON_ERROR( gatt_init() );
    RET_ON_ERROR( services_init(cfg) );
    RET_ON_ERROR( advertising_init() );
    RET_ON_ERROR( conn_params_init() );
    RET_ON_ERROR( peer_manager_init() );
    RET_ON_ERROR( advertising_start(false) );

    return 0;
}

/* Private functions ---------------------------------------------------------*/

static uint32_t advertising_init(void)
{
    ble_advertising_init_t init = {0};

    init.advdata.name_type               = BLE_ADVDATA_SHORT_NAME;  /* @todo eventually change to BLE_ADVDATA_NO_NAME */
    init.advdata.short_name_len          = 11;
    init.advdata.include_appearance      = true;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;

    init.srdata.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    init.srdata.uuids_complete.p_uuids = adv_uuids;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;

    init.evt_handler = on_adv_evt;

    RET_ON_ERROR( ble_advertising_init(&hadv, &init) );

    ble_advertising_conn_cfg_tag_set(&hadv, APP_BLE_CONN_CFG_TAG);

    return 0;
}

static uint32_t advertising_start(bool erase_bonds)
{
    if( erase_bonds ) {
        NRF_LOG_INFO("Erasing bonds!");
        return pm_peers_delete();
    }
    else {
        return ble_advertising_start(&hadv, BLE_ADV_MODE_FAST);
    }
}

static uint32_t conn_params_init(void)
{
    ble_conn_params_init_t cp_init = {0};

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = on_conn_params_err;

    return ble_conn_params_init(&cp_init);
}

static uint32_t gap_params_init(void)
{
    ble_gap_conn_params_t   gap_conn_params = {0};
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    RET_ON_ERROR(
        sd_ble_gap_device_name_set(
            &sec_mode,
            (const uint8_t *)DEVICE_NAME,
            strlen(DEVICE_NAME)
        )
    );

    /* @todo change appearance */
    RET_ON_ERROR(
        sd_ble_gap_appearance_set(BLE_APPEARANCE_GENERIC_HEART_RATE_SENSOR)
    );

    // memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;
    RET_ON_ERROR( sd_ble_gap_ppcp_set(&gap_conn_params) );

    /* Privacy settings */
    // ble_gap_privacy_params_t prvt_conf;
    // memset(&prvt_conf, 0, sizeof(prvt_conf));
    // prvt_conf.privacy_mode = BLE_GAP_PRIVACY_MODE_DEVICE_PRIVACY;
    // prvt_conf.private_addr_type = BLE_GAP_ADDR_TYPE_RANDOM_PRIVATE_RESOLVABLE;
    // prvt_conf.private_addr_cycle_s = 0; // default value
    // err_code = sd_ble_gap_privacy_set(&prvt_conf);
    // APP_ERROR_CHECK(err_code);

    return 0;
}

static uint32_t gatt_init(void)
{
    RET_ON_ERROR( nrf_ble_gatt_att_mtu_periph_set(&hgatt, 247) );
    return nrf_ble_gatt_init(&hgatt, NULL /* = event handler */);
}

static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    /* @todo use correctly! */
    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            // NRF_LOG_INFO("Fast advertising.");
            // err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
            // APP_ERROR_CHECK(err_code);
            break;

        case BLE_ADV_EVT_IDLE:
            // sleep_mode_enter();
            break;

        default:
            break;
    }
}

static void on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t err_code = NRF_SUCCESS;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected.");
            // LED indication will be changed when advertising starts.
            break;

        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected.");
            // err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            // APP_ERROR_CHECK(err_code);
            hconn = p_ble_evt->evt.gap_evt.conn_handle;
            err_code = nrf_ble_qwr_conn_handle_assign(&hqwr, hconn);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_INFO("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_INFO("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_INFO("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            // No implementation needed.
            break;
    }
}

static void on_conn_params_err(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    /* @todo explore possibilities */
    ret_code_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(hconn, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}

static void on_pm_evt(pm_evt_t const * p_evt)
{
    /* @todo explore possibilities */
    ret_code_t err_code;

    switch (p_evt->evt_id)
    {
        case PM_EVT_BONDED_PEER_CONNECTED:
        {
            NRF_LOG_INFO("Connected to a previously bonded device.");
        } break;

        case PM_EVT_CONN_SEC_SUCCEEDED:
        {
            NRF_LOG_INFO("Connection secured: role: %d, conn_handle: 0x%x, procedure: %d.",
                         ble_conn_state_role(p_evt->conn_handle),
                         p_evt->conn_handle,
                         p_evt->params.conn_sec_succeeded.procedure);
        } break;

        case PM_EVT_CONN_SEC_FAILED:
        {
            /* Often, when securing fails, it shouldn't be restarted, for security reasons.
             * Other times, it can be restarted directly.
             * Sometimes it can be restarted, but only after changing some Security Parameters.
             * Sometimes, it cannot be restarted until the link is disconnected and reconnected.
             * Sometimes it is impossible, to secure the link, or the peer device does not support it.
             * How to handle this error is highly application dependent. */
        } break;

        case PM_EVT_CONN_SEC_CONFIG_REQ:
        {
            // Reject pairing request from an already bonded peer.
            pm_conn_sec_config_t conn_sec_config = {.allow_repairing = false};
            pm_conn_sec_config_reply(p_evt->conn_handle, &conn_sec_config);
        } break;

        case PM_EVT_STORAGE_FULL:
        {
            // Run garbage collection on the flash.
            err_code = fds_gc();
            if (err_code == FDS_ERR_NO_SPACE_IN_QUEUES)
            {
                // Retry.
            }
            else
            {
                APP_ERROR_CHECK(err_code);
            }
        } break;

        case PM_EVT_PEERS_DELETE_SUCCEEDED:
        {
            APP_ERROR_CHECK(advertising_start(false));
        } break;

        case PM_EVT_PEER_DATA_UPDATE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peer_data_update_failed.error);
        } break;

        case PM_EVT_PEER_DELETE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peer_delete_failed.error);
        } break;

        case PM_EVT_PEERS_DELETE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peers_delete_failed_evt.error);
        } break;

        case PM_EVT_ERROR_UNEXPECTED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.error_unexpected.error);
        } break;

        case PM_EVT_CONN_SEC_START:
        case PM_EVT_PEER_DATA_UPDATE_SUCCEEDED:
        case PM_EVT_PEER_DELETE_SUCCEEDED:
        case PM_EVT_LOCAL_DB_CACHE_APPLIED:
        case PM_EVT_LOCAL_DB_CACHE_APPLY_FAILED:
            // This can happen when the local DB has changed.
        case PM_EVT_SERVICE_CHANGED_IND_SENT:
        case PM_EVT_SERVICE_CHANGED_IND_CONFIRMED:
        default:
            break;
    }
}

static void on_qwr_err(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

static uint32_t peer_manager_init(void)
{
    ble_gap_sec_params_t sec_param = {0};

    RET_ON_ERROR( pm_init() );

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    RET_ON_ERROR( pm_sec_params_set(&sec_param) );
    RET_ON_ERROR( pm_register(on_pm_evt) );

    return 0;
}

static uint32_t services_init(const SP_BLEcfg_t* const cfg)
{
    nrf_ble_qwr_init_t qwr_init = {0};

    // Initialize Queued Write Module.
    qwr_init.error_handler = on_qwr_err;
    RET_ON_ERROR( nrf_ble_qwr_init(&hqwr, &qwr_init) );

    /* Initialise the configuration service. */
    hServ_config.service.config.device = cfg->pDevice_cfg;
    hServ_config.service.config.cb_device = cfg->cb_device;
    hServ_config.service.config.dfu = cfg->pDFU;
    hServ_config.service.config.dfu_size = cfg->sDFU;
    hServ_config.service.config.cb_dfu = cfg->cb_dfu;
    hServ_config.service.config.dfuack = cfg->pDFUAck;
    hServ_config.conn_handle = BLE_CONN_HANDLE_INVALID;
    RET_ON_ERROR( SP_Service_Init(&hServ_config, SP_SERVICE_TYPE_CFG) );

#if 0
    
    err_code = SPS_SP_init(&SP_Service);
    APP_ERROR_CHECK(err_code);

    ACQ_Service.pData = (uint8_t*) cfg->pEWS;
    ACQ_Service.pData_size = sizeof(EWS_Data_t);
    err_code = SPS_ACQ_Init(&ACQ_Service);
    APP_ERROR_CHECK(err_code);

    CFG_Service.Data.Enabled = cfg->Data.Enabled;
    CFG_Service.Data.SupOx = cfg->Data.SupOx;
    CFG_Service.Data.Consc = cfg->Data.Consc;
    err_code = SPS_CFG_Init(&CFG_Service);
    APP_ERROR_CHECK(err_code);

    EWS_Service.pScore = cfg->Data.EWScore;
    err_code = SPS_EWS_Init(&EWS_Service);
    APP_ERROR_CHECK(err_code);

    service_BAT_init(&bas_service);

#endif

    return 0;
}

#if 0
#include <stdlib.h>
#include <string.h>


#include "ble_bas.h"



#include "ble_types.h"



#include "nrf_log.h"




#include "peer_manager_handler.h"
#include "sp_services.h"

/**
 * @brief      Function for initialising the Battery service.
 *
 * @param      p_service  Pointer to the service structure
 */
static void service_BAT_init(ble_bas_t* const p_service);

/*! Service handle for the acquisition service. */
static SPS_ACQ_t ACQ_Service = { .conn_handle = BLE_CONN_HANDLE_INVALID };
/*! Service handle for the SP service. */
static SPS_SP_t SP_Service = { .conn_handle = BLE_CONN_HANDLE_INVALID };
/*! Service handle for the configuration service. */
static SPS_CFG_t CFG_Service = { .conn_handle = BLE_CONN_HANDLE_INVALID };
/*! Service handle for the EWS service. */
static SPS_EWS_t EWS_Service = { .conn_handle = BLE_CONN_HANDLE_INVALID };

/*! BAS handle */
BLE_BAS_DEF(bas_service);

/*! Register a handler for ACQ events. */
NRF_SDH_BLE_OBSERVER(m_ACQ_observer, APP_BLE_OBSERVER_PRIO, sps_acq_evt_handler, (void*) &ACQ_Service);
/*! Register a handler for EWS events. */
NRF_SDH_BLE_OBSERVER(m_EWS_observer, APP_BLE_OBSERVER_PRIO, sps_ews_evt_handler, (void*) &EWS_Service);

uint32_t ble_update(const uint8_t* pData, const uint16_t offset, const uint16_t len)
{
    SPS_ACQ_Notify(&ACQ_Service, pData, offset, len);
    SPS_EWS_Notify(&EWS_Service);

    return 0;
}

// void ble_updateBAS(uint8_t level)
// {
//     ble_bas_battery_level_update(&bas_service, level, BLE_CONN_HANDLE_ALL);
// }

static void service_BAT_init(ble_bas_t* const p_service)
{
    ble_bas_init_t bas_init = {0};
    bas_init.evt_handler = NULL;
    bas_init.support_notification = true;
    bas_init.p_report_ref = NULL;
    bas_init.initial_batt_level = 100;

    bas_init.bl_rd_sec        = SEC_OPEN;
    bas_init.bl_cccd_wr_sec   = SEC_OPEN;
    bas_init.bl_report_rd_sec = SEC_OPEN;

    uint32_t err_code = ble_bas_init(p_service, &bas_init);
    APP_ERROR_CHECK(err_code);
}
#endif
