/**
 * @file sp_services.c
 * @brief      Smart Patch BLE service implementation.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       October 2020
 */

/* Includes ------------------------------------------------------------------*/
#include "ble.h"
#include "ble_types.h"

#include "macros.h"
#include "sp_board.h"
#include "sp_services.h"

#include "nrf_log.h"

/**
 * @addtogroup   SP_Services
 * @{
 */

/**
 * @defgroup   SP_Services_ic Internal Components
 * @{
 */

/* Private defines -----------------------------------------------------------*/

/**
 * @defgroup   SP_Services_UUIDs Service and characteristic uuid definitions
 * @{
 */

/*! Configuration service identifier */
#define SERV_CFG_UUID           0x4100
/*! Device configuration characteristic identifier */
#define CHAR_CFG_DEV_UUID       0x4101
/*! Device firware upgrade characteristic identifier */
#define CHAR_CFG_DFU_UUID       0x4102
/*! Device firware upgrade ACK characteristic identifier */
#define CHAR_CFG_DFUACK_UUID    0x4103

/**
 * @}
 */

/* Private macros ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/


/**
 * @brief      Adds the device configuration characteristic to the configuration
 *             service handler.
 *
 * @param      pServ      Pointer to the service handler struct.
 * @param[in]  base_uuid  Pointer to the base uuid.
 *
 * @return     { description_of_the_return_value }
 *
 * @todo       return description
 */
static uint32_t cfg_char_add_device(SP_Service_Handler_t* const pServ, const ble_uuid128_t* const base_uuid);

/**
 * @brief      Adds the device firmware upgrade characteristic to the
 *             configuration service handler.
 *
 * @param      pServ      Pointer to the service handler struct.
 * @param[in]  base_uuid  Pointer to the base uuid.
 *
 * @return     { description_of_the_return_value }
 *
 * @todo       return description
 */
static uint32_t cfg_char_add_dfu(SP_Service_Handler_t* const pServ, const ble_uuid128_t* const base_uuid);

/**
 * @brief      Adds the device firmware upgrade ACK characteristic to the
 *             configuration service handler.
 *
 * @param      pServ      Pointer to the service handler struct.
 * @param[in]  base_uuid  Pointer to the base uuid.
 *
 * @return     { description_of_the_return_value }
 *
 * @todo       return description
 */
static uint32_t cfg_char_add_dfuack(SP_Service_Handler_t* const pServ, const ble_uuid128_t* const base_uuid);

/**
 * @brief      Parses a GATTS write request.
 *
 * @param      pConfig  Pointer to the partial service struct.
 * @param[in]  pData    Pointer to the event data.
 */
static void cfg_gatts_write(SP_Service_CFG_t* const pConfig, const ble_gatts_evt_write_t* const pData);

/**
 * @brief      Initialises the configuration service handler.
 *
 * @param      pServ  Pointer to the service handler struct.
 *
 * @return     { description_of_the_return_value }
 *
 * @todo       return description
 */
static uint32_t cfg_service_init(SP_Service_Handler_t* const pServ);

/* Private variables ---------------------------------------------------------*/

/**
 * @}
 */

/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/

uint32_t SP_Service_Init(SP_Service_Handler_t* const pServ, const SP_Service_Type_t type)
{
    uint32_t err_code = -1;

    switch( type ) {
        case SP_SERVICE_TYPE_CFG:
            err_code = cfg_service_init(pServ);
            break;
        default:
            // No implementation needed.
            break;
    }

    return err_code;
}

void on_cfg_evt(ble_evt_t const* p_ble_evt, void* p_context)
{
    SP_Service_Handler_t* pServ = (SP_Service_Handler_t*) p_context;

    switch( p_ble_evt->header.evt_id ) {
        case BLE_GAP_EVT_CONNECTED:
            pServ->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            NRF_LOG_DEBUG("[SPS CFG] Connected to %u", pServ->conn_handle);
            break;
        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_DEBUG("[SPS CFG] Disconnected %u", pServ->conn_handle);
            pServ->conn_handle = BLE_CONN_HANDLE_INVALID;
            break;
        case BLE_GATTS_EVT_WRITE:
            cfg_gatts_write(&pServ->service.config, (ble_gatts_evt_write_t*) &p_ble_evt->evt.gatts_evt.params.write);
            break;
        default:
            NRF_LOG_WARNING("[SPS CFG] Unknown event: %u", p_ble_evt->header.evt_id);
            // No implementation needed.
            break;
    }
}

/* Private functions ---------------------------------------------------------*/

static uint32_t cfg_char_add_device(SP_Service_Handler_t* const pServ, const ble_uuid128_t* const base_uuid)
{
    ble_uuid_t char_uuid = { .uuid = CHAR_CFG_DEV_UUID };
    RET_ON_ERROR( sd_ble_uuid_vs_add(base_uuid, &char_uuid.type) );

    /* @todo set char_props */
    ble_gatts_char_md_t char_md = {
        .char_props = { .read = 1, .write = 1 }
    };

    ble_gatts_attr_md_t attr_md = { .vloc = BLE_GATTS_VLOC_USER };
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    ble_gatts_attr_t attr_char_value = {
        .p_uuid = &char_uuid,
        .p_attr_md = &attr_md,
        .max_len = 1,
        .init_len = 1,
        .p_value = pServ->service.config.device
    };

    RET_ON_ERROR(
        sd_ble_gatts_characteristic_add(
            pServ->serv_handle,
            &char_md,
            &attr_char_value,
            &pServ->service.config.hdevice
        )
    );

    NRF_LOG_DEBUG("Device config characteristic added (UUID: %u, handle: %u)", char_uuid.uuid, pServ->service.config.hdevice.value_handle);

    return 0;
}

static uint32_t cfg_char_add_dfu(SP_Service_Handler_t* const pServ, const ble_uuid128_t* const base_uuid)
{
    ble_uuid_t char_uuid = { .uuid = CHAR_CFG_DFU_UUID };
    RET_ON_ERROR( sd_ble_uuid_vs_add(base_uuid, &char_uuid.type) );

    /* @todo set char_props */
    ble_gatts_char_md_t char_md = {
        .char_props = { .write = 1 }
    };

    ble_gatts_attr_md_t attr_md = { .vloc = BLE_GATTS_VLOC_USER };
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    ble_gatts_attr_t attr_char_value = {
        .p_uuid = &char_uuid,
        .p_attr_md = &attr_md,
        .max_len = pServ->service.config.dfu_size,
        .init_len = 1,
        .p_value = pServ->service.config.dfu
    };

    RET_ON_ERROR(
        sd_ble_gatts_characteristic_add(
            pServ->serv_handle,
            &char_md,
            &attr_char_value,
            &pServ->service.config.hdfu
        )
    );

    NRF_LOG_DEBUG("DFU characteristic added (UUID: %u, handle: %u)", char_uuid.uuid, pServ->service.config.hdfu.value_handle);

    return 0;
}

static uint32_t cfg_char_add_dfuack(SP_Service_Handler_t* const pServ, const ble_uuid128_t* const base_uuid)
{
    ble_uuid_t char_uuid = { .uuid = CHAR_CFG_DFUACK_UUID };
    RET_ON_ERROR( sd_ble_uuid_vs_add(base_uuid, &char_uuid.type) );

    /* @todo set char_props */
    ble_gatts_char_md_t char_md = {
        .char_props = { .read = 1 }
    };

    ble_gatts_attr_md_t attr_md = { .vloc = BLE_GATTS_VLOC_USER };
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);

    ble_gatts_attr_t attr_char_value = {
        .p_uuid = &char_uuid,
        .p_attr_md = &attr_md,
        .max_len = 5,
        .init_len = 5,
        .p_value = pServ->service.config.dfuack
    };

    RET_ON_ERROR(
        sd_ble_gatts_characteristic_add(
            pServ->serv_handle,
            &char_md,
            &attr_char_value,
            &pServ->service.config.hdfuack
        )
    );

    NRF_LOG_DEBUG("DFU characteristic added (UUID: %u, handle: %u)", char_uuid.uuid, pServ->service.config.hdfuack.value_handle);

    return 0;
}

static void cfg_gatts_write(SP_Service_CFG_t* const pConfig, const ble_gatts_evt_write_t* const pData)
{
    if( pData->handle == pConfig->hdevice.value_handle ) {
        NRF_LOG_DEBUG("Device settings changed!");

        /* @todo check if set? */
        // if( pConfig->cb_device ) {
        pConfig->cb_device();
        // }
    }
    else if( pData->handle == pConfig->hdfu.value_handle ) {
        // NRF_LOG_DEBUG("Received DFU data!");

        /* @todo check if set? */
        pConfig->cb_dfu(pData->len);
    }

#if 0
    if( pData->handle == pService->hChars.Enabled.value_handle ) {
        if( pData->len != 1 )   return;

        // SP_Acquisition_Enable();
    }
    else if( pData->handle == pService->hChars.Consc.value_handle ) {
        NRF_LOG_DEBUG("Updated consciousness changed!");
    }
    else if( pData->handle == pService->hChars.SupOx.value_handle ) {
        NRF_LOG_DEBUG("Updated SupOx changed!");
    }
#endif
}

static uint32_t cfg_service_init(SP_Service_Handler_t* const pServ)
{
    if( pServ->type != SP_SERVICE_TYPE_INVALID ) {
        /* Service already initialised! */
        return -1;
    }

    pServ->type = SP_SERVICE_TYPE_CFG;

    ble_uuid128_t base_uuid = DEVICE_UUID;
    ble_uuid_t service_uuid = { .uuid = SERV_CFG_UUID };

    /* Add the UUID to the SD. */
    RET_ON_ERROR( sd_ble_uuid_vs_add(&base_uuid, &service_uuid.type) );

    /* Add the service to the SD. */
    RET_ON_ERROR( sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &service_uuid, &pServ->serv_handle) );

    /* Add the device config characteristic. */
    RET_ON_ERROR( cfg_char_add_device(pServ, &base_uuid) );

    /* Add the DFU characteristic */
    RET_ON_ERROR( cfg_char_add_dfu(pServ, &base_uuid) );

    /* Add the DFU ACK characteristic */
    RET_ON_ERROR( cfg_char_add_dfuack(pServ, &base_uuid) );

    return 0;

#if 0
    /* Add the enabled characteristic */
    err_code = _CFG_Char_EN_Add(pService, &base_uuid);
    RET_ON_ERROR(err_code);

    /* Add the supplementary oxygen characteristic */
    err_code = _CFG_Char_SupOx_Add(pService, &base_uuid);
    RET_ON_ERROR(err_code);

    /* Add the consciousness characteristic */
    err_code = _CFG_Char_Consc_Add(pService, &base_uuid);
    RET_ON_ERROR(err_code);

    /* Add the enabled characteristic */
    // err_code = _ACQ_Char_EN_Add(pService, &base_uuid);
    // RET_ON_ERROR(err_code);

    /* Add the acqusition characteristic */
    // err_code = _ACQ_Char_Data_Add(pService, &base_uuid);
    // RET_ON_ERROR(err_code);

    return err_code;
#endif
}

#if 0
#include "ble_gatts.h"
#include "ble_srv_common.h"
#include "nrf_log.h"
#include "macros.h"
#include "sp_ble.h"
#include "sp_board.h"
#include "sp_system.h"
#include "sp_services.h"

#define ACQ_EVT_NOTIFICATION_MASK           0x0001
#define ACQ_EVT_ACQUISITION_MASK            0x0010

static uint32_t _ACQ_Char_Data_Add(SPS_ACQ_t* const pService, const ble_uuid128_t* const base_uuid);

static void _ACQ_GATTS_Write(const ble_gatts_evt_t* const evt);

// static uint32_t _ACQ_Char_EN_Add(SPS_ACQ_t* const pService, const ble_uuid128_t* const base_uuid);

static uint32_t _CFG_Char_Consc_Add(SPS_CFG_t* const pService, const ble_uuid128_t* const base_uuid);



static uint32_t _CFG_Char_SupOx_Add(SPS_CFG_t* const pService, const ble_uuid128_t* const base_uuid);


static uint32_t _SP_Char_BPT_Add(SPS_SP_t* const pService, const ble_uuid128_t* const base_uuid);

static uint32_t _SP_Char_HR_Add(SPS_SP_t* const pService, const ble_uuid128_t* const base_uuid);

static uint32_t _SP_Char_SPO2_Add(SPS_SP_t* const pService, const ble_uuid128_t* const base_uuid);

static uint32_t _SP_Char_Temp_Add(SPS_SP_t* const pService, const ble_uuid128_t* const base_uuid);

uint32_t SPS_ACQ_Init(SPS_ACQ_t* const pService)
{
    ble_uuid128_t base_uuid = DEVICE_UUID;
    ble_uuid_t service_uuid = { .uuid = SPS_ACQ_SERVICE };

    /* Add the UUID to the SD. */
    uint32_t err_code = sd_ble_uuid_vs_add(&base_uuid, &service_uuid.type);
    RET_ON_ERROR(err_code);

    /* Add the service to the SD. */
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &service_uuid, &pService->srv_handle);
    RET_ON_ERROR(err_code);

    /* Add the enabled characteristic */
    // err_code = _ACQ_Char_EN_Add(pService, &base_uuid);
    // RET_ON_ERROR(err_code);

    /* Add the acqusition characteristic */
    err_code = _ACQ_Char_Data_Add(pService, &base_uuid);
    RET_ON_ERROR(err_code);




    

    

    /** Add the HR characteristic */
    // 

    // /** Add the SpO2 characteristic */
    // err_code = _SP_Char_SPO2_Add(pService, &base_uuid);
    // RET_ON_ERROR(err_code);

    // err_code = _SP_Char_BPT_Add(pService, &base_uuid);
    // RET_ON_ERROR(err_code);

    // err_code = _SP_Char_Temp_Add(pService, &base_uuid);

    // return err_code;




    /* Add the characteristics. */
    // ble_uuid_t char_uuid = {
    //     .uuid = SPS_HR_CHAR_UUID
    // };
    // err_code = sd_ble_uuid_vs_add(&base_uuid, &char_uuid.type);
    // RET_ON_ERROR(err_code);

    // ble_gatts_char_md_t char_md = {
    //     .char_props = { .read = 1, .write = 1 }
    // };

    // ble_gatts_attr_md_t attr_md = {
    //     .vloc = BLE_GATTS_VLOC_USER
    //     // .vloc = BLE_GATTS_VLOC_STACK
    // };
    // BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    // BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    // uint8_t val[] = {0xDE, 0xAD, 0xBE, 0xEF};

    // ble_gatts_attr_t attr_char_value = {
    //     .p_uuid = &char_uuid,
    //     .p_attr_md = &attr_md,
    //     .max_len = pService->dataSize,
    //     // .max_len = 4,
    //     .init_len = pService->dataSize,
    //     // .init_len = 4,
    //     .p_value = pService->pData
    //     // .p_value = val
    // };

    // err_code = sd_ble_gatts_characteristic_add(pService->handle,
    //                                &char_md,
    //                                &attr_char_value,
    //                                &pService->char_handle);

    return err_code;
}

uint32_t SPS_ACQ_Notify(SPS_ACQ_t* const pService, const uint8_t* pData, const uint16_t offset, uint16_t len)
{
    if( pService->conn_handle == BLE_CONN_HANDLE_INVALID ) {
        NRF_LOG_ERROR("[SPS ACQ] Notification without connection!");
        return -1;
    }

    // NRF_LOG_INFO("Updating %lu", pService->conn_handle);

    // uint16_t _len = len;

    ble_gatts_hvx_params_t hvx_params = {
        .handle = pService->char_handle.value_handle,
        .type = BLE_GATT_HVX_NOTIFICATION,
        .offset = offset,
        .p_len = &len,
        .p_data = pData
    };

    sd_ble_gatts_hvx(pService->conn_handle, &hvx_params);

    return 0;
}



uint32_t SPS_EWS_Init(SPS_EWS_t* const pService)
{
    ble_uuid128_t base_uuid = DEVICE_UUID;
    ble_uuid_t service_uuid = { .uuid = SPS_EWS_SERVICE };

    /* Add the UUID to the SD. */
    uint32_t err_code = sd_ble_uuid_vs_add(&base_uuid, &service_uuid.type);
    RET_ON_ERROR(err_code);

    /* Add the service to the SD. */
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &service_uuid, &pService->srv_handle);
    RET_ON_ERROR(err_code);

    /* Add the score characteristic */
    ble_uuid_t char_uuid = { .uuid = EWS_SCORE_CHAR_UUID };
    err_code = sd_ble_uuid_vs_add(&base_uuid, &char_uuid.type);
    RET_ON_ERROR(err_code);

    ble_gatts_attr_md_t cccd_md = {
        .vloc = BLE_GATTS_VLOC_STACK,
    };
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

    ble_gatts_char_md_t char_md = {
        /* @todo set char_props */
        .char_props = { .read = 1, .notify = 1 },
        .p_cccd_md = &cccd_md
    };

    ble_gatts_attr_md_t attr_md = { .vloc = BLE_GATTS_VLOC_USER };
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);

    ble_gatts_attr_t attr_char_value = {
        .p_uuid = &char_uuid,
        .p_attr_md = &attr_md,
        .max_len = 1,
        .init_len = 1,
        .p_value = pService->pScore
    };

    err_code = sd_ble_gatts_characteristic_add(pService->srv_handle,
                                   &char_md,
                                   &attr_char_value,
                                   &pService->char_handle);

    return err_code;
    
}

uint32_t SPS_EWS_Notify(SPS_EWS_t* const pService)
{
    if( pService->conn_handle == BLE_CONN_HANDLE_INVALID ) {
        NRF_LOG_ERROR("[SPS EWS] Notification without connection!");
        return -1;
    }

    // NRF_LOG_INFO("Updating %lu", pService->conn_handle);

    uint16_t len = 1;
    ble_gatts_hvx_params_t hvx_params = {
        .handle = pService->char_handle.value_handle,
        .type = BLE_GATT_HVX_NOTIFICATION,
        .offset = 0,
        .p_len = &len,
        .p_data = pService->pScore
    };

    sd_ble_gatts_hvx(pService->conn_handle, &hvx_params);

    return 0;
}

uint32_t SPS_SP_init(SPS_SP_t* const pService)
{
    ble_uuid128_t base_uuid = DEVICE_UUID;
    ble_uuid_t service_uuid = { .uuid = SPS_SP_SERVICE };

    /* Add the UUID to the SD. */
    uint32_t err_code = sd_ble_uuid_vs_add(&base_uuid, &service_uuid.type);
    RET_ON_ERROR(err_code);

    /* Add the service to the SD. */
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &service_uuid, &pService->handle);
    RET_ON_ERROR(err_code);

    /** Add the HR characteristic */
    err_code = _SP_Char_HR_Add(pService, &base_uuid);
    RET_ON_ERROR(err_code);

    /** Add the SpO2 characteristic */
    err_code = _SP_Char_SPO2_Add(pService, &base_uuid);
    RET_ON_ERROR(err_code);

    err_code = _SP_Char_BPT_Add(pService, &base_uuid);
    RET_ON_ERROR(err_code);

    err_code = _SP_Char_Temp_Add(pService, &base_uuid);

    return err_code;
}

void sps_acq_evt_handler(ble_evt_t const* p_ble_evt, void* p_context)
{
    SPS_ACQ_t* pService = (SPS_ACQ_t*) p_context;

    NRF_LOG_WARNING("ACQ EVT: %u", p_ble_evt->evt.common_evt.conn_handle);

    switch( p_ble_evt->header.evt_id ) {
        case BLE_GAP_EVT_CONNECTED:
            pService->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            NRF_LOG_DEBUG("[SPS ACQ] Connected to %u", pService->conn_handle);
            break;
        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_DEBUG("[SPS ACQ] Disconnected %u", pService->conn_handle);
            pService->conn_handle = BLE_CONN_HANDLE_INVALID;
            break;
        case BLE_GATTS_EVT_WRITE:
            NRF_LOG_DEBUG("[SPS ACQ] GATTS Write %u - %u", pService->conn_handle, p_ble_evt->evt.gatts_evt.params.write.handle);
            _ACQ_GATTS_Write((ble_gatts_evt_t*) &p_ble_evt->evt.gatts_evt);
            break;
        default:
            NRF_LOG_WARNING("[SPS ACQ] Unknown event: %u", p_ble_evt->header.evt_id);
            // No implementation needed.
            break;
    }
}



void sps_ews_evt_handler(ble_evt_t const* p_ble_evt, void* p_context)
{
    SPS_EWS_t* pService = (SPS_EWS_t*) p_context;

    switch( p_ble_evt->header.evt_id ) {
        case BLE_GAP_EVT_CONNECTED:
            pService->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            NRF_LOG_INFO("[SPS EWS] Connected to %u", pService->conn_handle);
            break;
        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("[SPS EWS] Disconnected %u", pService->conn_handle);
            pService->conn_handle = BLE_CONN_HANDLE_INVALID;
            break;
        default:
            // No implementation needed.
            break;
    }
}

static uint32_t _ACQ_Char_Data_Add(SPS_ACQ_t* const pService, const ble_uuid128_t* const base_uuid)
{
    ble_uuid_t char_uuid = { .uuid = ACQ_DATA_CHAR_UUID };
    uint32_t err_code = sd_ble_uuid_vs_add(base_uuid, &char_uuid.type);
    RET_ON_ERROR(err_code);

    ble_gatts_attr_md_t cccd_md = {
        .vloc = BLE_GATTS_VLOC_STACK,
    };
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

    ble_gatts_char_md_t char_md = {
        /* @todo set char_props */
        .char_props = { .read = 1, .notify = 1 },
        .p_cccd_md = &cccd_md
    };

    ble_gatts_attr_md_t attr_md = { .vloc = BLE_GATTS_VLOC_USER };
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);

    ble_gatts_attr_t attr_char_value = {
        .p_uuid = &char_uuid,
        .p_attr_md = &attr_md,
        .max_len = pService->pData_size,
        .init_len = pService->pData_size,
        .p_value = pService->pData
    };

    err_code = sd_ble_gatts_characteristic_add(pService->srv_handle,
                                   &char_md,
                                   &attr_char_value,
                                   &pService->char_handle);

    return err_code;
}

static void _ACQ_GATTS_Write(const ble_gatts_evt_t* const evt)
{
    if( evt->params.write.len != 2 ) {
        return;
    }

    // uint16_t data = *((uint16_t*) evt->params.write.data);

    // SP_ACQ_Notifications_Enabled(data & ACQ_EVT_NOTIFICATION_MASK);
}

// static uint32_t _ACQ_Char_EN_Add(SPS_ACQ_t* const pService, const ble_uuid128_t* const base_uuid)
// {
//     ble_uuid_t char_uuid = { .uuid = ACQ_EN_CHAR_UUID };
//     uint32_t err_code = sd_ble_uuid_vs_add(base_uuid, &char_uuid.type);
//     RET_ON_ERROR(err_code);

//     ble_gatts_char_md_t char_md = {
//         /* @todo set char_props */
//         .char_props = { .read = 1, .write = 1 }
//     };
//     ble_gatts_attr_md_t attr_md = { .vloc = BLE_GATTS_VLOC_USER };
//     BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
//     BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

//     ble_gatts_attr_t attr_char_value = {
//         .p_uuid = &char_uuid,
//         .p_attr_md = &attr_md,
//         .max_len = 1,
//         .init_len = 1,
//         .p_value =  &pService->Enabled
//     };

//     err_code = sd_ble_gatts_characteristic_add(pService->srv_handle,
//                                    &char_md,
//                                    &attr_char_value,
//                                    &pService->EN_handle);

//     return err_code;
// }

static uint32_t _CFG_Char_Consc_Add(SPS_CFG_t* const pService, const ble_uuid128_t* const base_uuid)
{
    ble_uuid_t char_uuid = { .uuid = CFG_CONSC_CHAR_UUID };
    uint32_t err_code = sd_ble_uuid_vs_add(base_uuid, &char_uuid.type);
    RET_ON_ERROR(err_code);

    ble_gatts_char_md_t char_md = {
        /* @todo set char_props */
        .char_props = { .write = 1 }
    };
    ble_gatts_attr_md_t attr_md = { .vloc = BLE_GATTS_VLOC_USER };
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    ble_gatts_attr_t attr_char_value = {
        .p_uuid = &char_uuid,
        .p_attr_md = &attr_md,
        .max_len = 1,
        .init_len = 1,
        .p_value = pService->Data.Consc
    };

    err_code = sd_ble_gatts_characteristic_add(pService->handle,
                                   &char_md,
                                   &attr_char_value,
                                   &pService->hChars.Consc);

    return err_code;
}



static uint32_t _CFG_Char_SupOx_Add(SPS_CFG_t* const pService, const ble_uuid128_t* const base_uuid)
{
    ble_uuid_t char_uuid = { .uuid = CFG_SUPOX_CHAR_UUID };
    uint32_t err_code = sd_ble_uuid_vs_add(base_uuid, &char_uuid.type);
    RET_ON_ERROR(err_code);

    ble_gatts_char_md_t char_md = {
        /* @todo set char_props */
        .char_props = { .write = 1 }
    };
    ble_gatts_attr_md_t attr_md = { .vloc = BLE_GATTS_VLOC_USER };
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    ble_gatts_attr_t attr_char_value = {
        .p_uuid = &char_uuid,
        .p_attr_md = &attr_md,
        .max_len = 1,
        .init_len = 1,
        .p_value = pService->Data.SupOx
    };

    err_code = sd_ble_gatts_characteristic_add(pService->handle,
                                   &char_md,
                                   &attr_char_value,
                                   &pService->hChars.SupOx);

    return err_code;
}


static uint32_t _SP_Char_BPT_Add(SPS_SP_t* const pService, const ble_uuid128_t* const base_uuid)
{
    ble_uuid_t char_uuid = { .uuid = SPS_BPT_CHAR_UUID };
    uint32_t err_code = sd_ble_uuid_vs_add(base_uuid, &char_uuid.type);
    RET_ON_ERROR(err_code);

    ble_gatts_char_md_t char_md = {
        /* @todo set char_props */
        .char_props = { .read = 1 }
    };
    ble_gatts_attr_md_t attr_md = { .vloc = BLE_GATTS_VLOC_USER };
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);

    ble_gatts_attr_t attr_char_value = {
        .p_uuid = &char_uuid,
        .p_attr_md = &attr_md,
        .max_len = 4,
        .init_len = 4,
        .p_value = (uint8_t*) &pService->Data.BPT
    };

    err_code = sd_ble_gatts_characteristic_add(pService->handle,
                                   &char_md,
                                   &attr_char_value,
                                   &pService->BPT_handle);

    return err_code;
}

static uint32_t _SP_Char_HR_Add(SPS_SP_t* const pService, const ble_uuid128_t* const base_uuid)
{
    ble_uuid_t char_uuid = { .uuid = SPS_HR_CHAR_UUID };
    uint32_t err_code = sd_ble_uuid_vs_add(base_uuid, &char_uuid.type);
    RET_ON_ERROR(err_code);

    ble_gatts_char_md_t char_md = {
        /* @todo set char_props */
        .char_props = { .read = 1 }
    };
    ble_gatts_attr_md_t attr_md = { .vloc = BLE_GATTS_VLOC_USER };
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);

    ble_gatts_attr_t attr_char_value = {
        .p_uuid = &char_uuid,
        .p_attr_md = &attr_md,
        .max_len = 2,
        .init_len = 2,
        .p_value = (uint8_t*) &pService->Data.HR
    };

    err_code = sd_ble_gatts_characteristic_add(pService->handle,
                                   &char_md,
                                   &attr_char_value,
                                   &pService->HR_handle);

    return err_code;
}

static uint32_t _SP_Char_SPO2_Add(SPS_SP_t* const pService, const ble_uuid128_t* const base_uuid)
{
    /* Add the characteristics. */
    ble_uuid_t char_uuid = { .uuid = SPS_SPO2_CHAR_UUID };
    uint32_t err_code = sd_ble_uuid_vs_add(base_uuid, &char_uuid.type);
    RET_ON_ERROR(err_code);

    ble_gatts_char_md_t char_md = {
        /* @todo set char_props */
        .char_props = { .read = 1 }
    };
    ble_gatts_attr_md_t attr_md = { .vloc = BLE_GATTS_VLOC_USER };
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);

    ble_gatts_attr_t attr_char_value = {
        .p_uuid = &char_uuid,
        .p_attr_md = &attr_md,
        .max_len = 1,
        .init_len = 1,
        .p_value = &pService->Data.SpO2
    };

    err_code = sd_ble_gatts_characteristic_add(pService->handle,
                                   &char_md,
                                   &attr_char_value,
                                   &pService->SPO2_handle);
    return err_code;
}

static uint32_t _SP_Char_Temp_Add(SPS_SP_t* const pService, const ble_uuid128_t* const base_uuid)
{
    /* Add the characteristics. */
    ble_uuid_t char_uuid = { .uuid = SPS_TEMP_CHAR_UUID };
    uint32_t err_code = sd_ble_uuid_vs_add(base_uuid, &char_uuid.type);
    RET_ON_ERROR(err_code);

    ble_gatts_char_md_t char_md = {
        /* @todo set char_props */
        .char_props = { .read = 1 }
    };
    ble_gatts_attr_md_t attr_md = { .vloc = BLE_GATTS_VLOC_USER };
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);

    ble_gatts_attr_t attr_char_value = {
        .p_uuid = &char_uuid,
        .p_attr_md = &attr_md,
        .max_len = 1,
        .init_len = 1,
        .p_value = &pService->Data.Temp
    };

    err_code = sd_ble_gatts_characteristic_add(pService->handle,
                                   &char_md,
                                   &attr_char_value,
                                   &pService->Temp_handle);
    return err_code;
}
#endif
