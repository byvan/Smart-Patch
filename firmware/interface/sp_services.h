/**
 * @file sp_services.h
 * @brief      Smart Patch BLE service definitions.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       October 2020
 */

#ifndef __SP_SERVICES_H
#define __SP_SERVICES_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "ble.h"

/**
 * @addtogroup   SP_FW
 * @{
 */

/**
 * @defgroup   SP_Services Smart Patch Service Module
 * @{
 */

/* Exported constants --------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/**
 * @brief      Partial configuration service structure.
 */
typedef struct {
    uint8_t*                    device;                 /*!< Pointer to the device configuration register */
    void                        (*cb_device)(void);     /*!< Pointer to the register changed callback */
    ble_gatts_char_handles_t    hdevice;                /*!< Handler for the device config characteristic */

    uint8_t*                    dfu;                    /*!< Pointer to the dfu buffer */
    uint32_t                    dfu_size;               /*!< Size of the DFU buffer */
    void                        (*cb_dfu)(uint16_t);    /*!< Pointer to the dfu update callback */
    ble_gatts_char_handles_t    hdfu;                   /*!< Handler for the dfu characteristic */

    uint8_t*                    dfuack;                 /*!< Pointer to the dfu ack register */
    ble_gatts_char_handles_t    hdfuack;                /*!< Handler for the dfuack characteristic */
} SP_Service_CFG_t;

/**
 * @brief      Partial acquisition service structure.
 */
typedef struct {
    void*   dummy;
} SP_Service_ACQ_t;

/**
 * @brief      Enumerator for the services running on the device.
 */
typedef enum {
    SP_SERVICE_TYPE_INVALID = 0,            /*!< invalid/unitialised service */
    SP_SERVICE_TYPE_CFG                     /*!< Configuration service type */
} SP_Service_Type_t;

typedef struct {
    SP_Service_Type_t       type;           /*!< The type of the service */
    uint16_t                conn_handle;    /*!< Connection handle set by the SD event handler */
    uint16_t                serv_handle;    /*!< Service handle as set by the SoftDevice */

    union {
        SP_Service_CFG_t    config;         /*!< Configuration service data */
        SP_Service_ACQ_t    acquisition;    /*!< Acquisition service data */
    }                   service;            /*!< The custom service data associated with the type */
} SP_Service_Handler_t;

/* Exported macros -----------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/

/**
 * @brief      Initialises a service according to its type.
 *
 * @param      pServ  The service handler struct to be initialised.
 * @param[in]  type   The type of the service.
 *
 * @return     { description_of_the_return_value }
 *
 * @todo       return description
 */
uint32_t SP_Service_Init(SP_Service_Handler_t* const pServ, const SP_Service_Type_t type);

/**
 * @brief      SP Config service event handler.
 *
 * @param      p_ble_evt  The ble event
 * @param      p_context  The context
 */
void on_cfg_evt(ble_evt_t const* p_ble_evt, void* p_context);

/**
 * @}
 */

/**
 * @}
 */

#if 0

#include <stdint.h>
#include "ble.h"
#include "sensors.h"

/*! SPS SP service identifier */
#define SPS_SP_SERVICE                      0x4000
/*! SPS SP acquisition service identifier */
#define SPS_ACQ_SERVICE                     0x4100

/*! SPS EWS service identifier */
#define SPS_EWS_SERVICE                     0x4400

/*! SPS HR characteristic */
#define SPS_HR_CHAR_UUID                    0x4001
/*! SPS SpO2 characteristic */
#define SPS_SPO2_CHAR_UUID                  0x4002
/*! SPS BPT characteristic */
#define SPS_BPT_CHAR_UUID                   0x4003
/*! SPS Temp characteristic */
#define SPS_TEMP_CHAR_UUID                  0x4004
/*! ACQ Enabled characteristic */
#define ACQ_EN_CHAR_UUID                    0x4101
/*! ACQ Data characteristic */
#define ACQ_DATA_CHAR_UUID                  0x4102

/*! CFG supplementary oxygen characteristic */
#define CFG_SUPOX_CHAR_UUID                 0x4202
/*! CFG consciousness characteristic */
#define CFG_CONSC_CHAR_UUID                 0x4203
/*! EWS characteristic */
#define EWS_SCORE_CHAR_UUID                 0x4401

/**
 * @brief      Smart Patch acquisition service structure to handle communication associated
 *             with this service.
 */
typedef struct {
    uint16_t                    conn_handle;    /*!< Connection handle, set by the BLE event callback. */
    uint16_t                    srv_handle;     /*!< Service handle, set by the SoftDevice. */
    ble_gatts_char_handles_t    char_handle;    /*!< Characteristcs handle, set by the SoftDevice. */
    uint8_t*                    pData;          /*!< Smart Patch Data struct */
    uint16_t                    pData_size;     /*!< Size of the data struct */
} SPS_ACQ_t;

/**
 * @brief      Smart Patch configuration service structure to handle
 *             communication associated with this service.
 */
typedef struct {
    uint16_t                    conn_handle;    /*!< Connection handle, set by the BLE event callback. */
    uint16_t                    handle;         /*!< Service handle, set by the SoftDevice. */

    struct {
        uint8_t*                Enabled;        /*!< Global flag, if acquisition is enabled */
        uint8_t*                SupOx;          /*!< Boolean if supplementary oxygen is administered */
        uint8_t*                Consc;          /*!< Enum value of consciousness */
    } Data;

    struct {
        ble_gatts_char_handles_t    Enabled;
        ble_gatts_char_handles_t    SupOx;
        ble_gatts_char_handles_t    Consc;
    } hChars;
} SPS_CFG_t;

/**
 * @brief      Smart Patch EWS service structure to handle communication
 *             associated with this service.
 */
typedef struct {
    uint16_t                    conn_handle;    /*!< Connection handle, set by the BLE event callback. */
    uint16_t                    srv_handle;     /*!< Service handle, set by the SoftDevice. */
    ble_gatts_char_handles_t    char_handle;    /*!< Characteristcs handle, set by the SoftDevice. */
    uint8_t*                    pScore;         /*!< Pointer to the variable holding the score. */
} SPS_EWS_t;

/**
 * @brief      Smart Patch service structure to handle communication associated
 *             with this service.
 */
typedef struct {
    uint16_t                    conn_handle;    /*!< Connection handle, set by the BLE event callback. */
    uint16_t                    handle;         /*!< Service handle, set by the SoftDevice. */
    ble_gatts_char_handles_t    HR_handle;      /*!< Characteristcs handle, set by the SoftDevice. */
    ble_gatts_char_handles_t    SPO2_handle;    /*!< Characteristcs handle, set by the SoftDevice. */
    ble_gatts_char_handles_t    BPT_handle;     /*!< Characteristcs handle, set by the SoftDevice. */
    ble_gatts_char_handles_t    Temp_handle;    /*!< Characteristcs handle, set by the SoftDevice. */

    SP_Data_t                   Data;           /*!< Smart Patch Data struct. */
} SPS_SP_t;

/**
 * @brief      Initialises the acquisition service.
 *
 * @param      pService  Pointer to the service structure.
 *
 * @return     any error code that might come up during initialisation.
 *
 *             This function initialises the Smart Patch service. It is used to
 *             collect all the vital parameters at the highest available rate.
 */
uint32_t SPS_ACQ_Init(SPS_ACQ_t* const pService);

/**
 * @brief      Notifies the application about new data available for the
 *             acquisition service.
 *
 * @param[in]  pService  Pointer to the service struct.
 * @param[in]  pData     Pointer to the data to be used.
 *
 * @return     { description_of_the_return_value }
 */
uint32_t SPS_ACQ_Notify(SPS_ACQ_t* const pService, const uint8_t* pData, const uint16_t offset, uint16_t len);

/**
 * @brief      Initialises the EWS service.
 *
 * @param      pService  Pointer to the service structure.
 *
 * @return     any error code that might come up during initialisation.
 *
 *             This function initialises the service used to retrieve the EWS.
 */
uint32_t SPS_EWS_Init(SPS_EWS_t* const pService);

/**
 * @brief      Notifies the application about new EWS available for the EWS
 *             service.
 *
 * @param[in]  pService  Pointer to the service struct.
 *
 * @return     { description_of_the_return_value }
 */
uint32_t SPS_EWS_Notify(SPS_EWS_t* const pService);

/**
 * @brief      Initialises the Smart Patch service.
 *
 * @param      pService  Pointer to the service structure.
 *
 * @return     any error code that might come up during initialisation.
 *
 *             This function initialises the Smart Patch service. It is used to
 *             collect all the vital parameters and broadcast them at the
 *             configured rate.
 */
uint32_t SPS_SP_init(SPS_SP_t* const pService);

/**
 * @brief      SP acquisition service event handler.
 *
 * @param      p_ble_evt  The ble event
 * @param      p_context  The context
 */
void sps_acq_evt_handler(ble_evt_t const* p_ble_evt, void* p_context);

/**
 * @brief      SP EWS service event handler.
 *
 * @param      p_ble_evt  The ble event
 * @param      p_context  The context
 */
void sps_ews_evt_handler(ble_evt_t const* p_ble_evt, void* p_context);

#endif

#endif /* __SP_SERVICES_H included */
