/**
 * @file sp_ble.h
 * @brief      Smart Patch BLE defintions.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       October 2020
 */

#ifndef __SP_BLE_H
#define __SP_BLE_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/**
 * @addtogroup   SP_FW
 * @{
 */

/**
 * @defgroup   SP_BLE Smart Patch BLE Module
 * @{
 */

/* Exported constants --------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/**
 * @brief      Configuration data struct for setting up the BLE stack.
 */
typedef struct {
    uint8_t*    pDevice_cfg;            /*!< Pointer to the device configuration register. */
    void        (*cb_device)(void);     /*!< Pointer to the callback on a device register changed event. */

    uint8_t*    pDFU;                   /*!< Pointer to the DFU buffer */
    uint32_t    sDFU;                   /*!< Size of the DFU buffer */
    void        (*cb_dfu)(uint16_t);    /*!< Pointer to the callback on DFU register changed event. */

    uint8_t*    pDFUAck;                /*!< Pointer to the two byte DFU ACK register */
} SP_BLEcfg_t;

/* Exported macros -----------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/

/**
 * @brief      Initialises the Bluetooth stack and adds all relevant services.
 *
 * @param[in]  cfg   The configuration to use.
 *
 * @return     any error that comes up during setup.
 */
uint32_t SP_BLE_Init(const SP_BLEcfg_t* const cfg);

/**
 * @}
 */

/**
 * @}
 */

#if 0
#include "sp_system.h"

typedef struct {
    uint8_t*        Enabled;
    uint8_t*        SupOx;
    uint8_t*        Consc;
    uint8_t*        EWScore;
} SP_BLEdata_t;

typedef struct {
    EWS_Data_t*     pEWS;
    SP_BLEdata_t    Data;
} SP_BLEcfg_t;

uint32_t ble_update(const uint8_t* pData, const uint16_t offset, const uint16_t len);

// void ble_updateBAS(uint8_t level);
#endif

#endif /* __SP_BLE_H included */
