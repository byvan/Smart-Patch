#! python3

import argparse
from BLEBridge import BridgeController, Utils
from datetime import datetime
import sys, threading, time

SERV_CFG        = 0x4100
CHAR_CFG_DEV    = 0x4101
CHAR_CFG_DFU    = 0x4102
CHAR_CFG_DFUACK = 0x4103

SERV_PAT        = 0x5100
CHAR_PAT_ID     = 0x5101

SP_UUIDS = {
    SERV_CFG : [CHAR_CFG_DEV, CHAR_CFG_DFU, CHAR_CFG_DFUACK],
    SERV_PAT : [CHAR_PAT_ID]
}

class SmartPatch(object):

    #---------------------------------------------------------------------------
    ## Constructs a new instance.
    ##
    ## :param      port:  The serial port to use
    ## :type       port:  str
    ##
    def __init__(self, port:str=None, logging:bool=False):
        super(SmartPatch, self).__init__()

        self._serialport = port
        self._bridge = BridgeController.BridgeController(port)
        self._device = None
        self._patientID = None
        self._logging = logging

    #---------------------------------------------------------------------------
    ## Deletes the object.
    ##
    def __del__(self):
        pass

    #---------------------------------------------------------------------------
    ## Connects to the previously found device and checks, if all necessary
    ## services and characteristics are present.
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    ## :raises     RuntimeError:  if the firmware of the device is not
    ##                            compatible with the host application.
    ##
    def Connect(self, addr:bytes=None) -> None:
        if addr:
            target = Utils.BLEAddress(addr)
        else:
            target = self._device
            addr = self._device.Address

        if not self._bridge.Connect(target):
            raise RuntimeError('Failed to conenct to {}!'.format(addr))

        self.log_print("Connected to Smart Patch with address {}!".format(target))

        if not self.check_characteristics():
            raise RuntimeError('Incompatible firmware version!')

    #---------------------------------------------------------------------------
    ## Scans BLE devices for the Smart Patch.
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    ## :raises     RuntimeError:  if the Smart Patch could not be found.
    ##
    def Scan(self) -> None:
        n_dev = self._bridge.Scan()
        for i in range(0, n_dev):
            data = self._bridge.get_device_info(i)

            if data.Name == 'Smart Patch':
                self._device = data
                break

        if self._device == None:
            raise RuntimeError('Unable to find Smart Patch!')

    #---------------------------------------------------------------------------
    ## Opens the serial port to the BLE Bridge dongle.
    ##
    ## If no serial port was passed during instantiation, the function will scan
    ## for the dongle and connect to it.
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    ## :raises     RuntimeError:  if the dongle could not be found.
    ##
    def Setup(self) -> None:
        if self._serialport == None:
            serial_port = self._bridge.FindDongle()

            if not serial_port:
                raise RuntimeError('Could not find compatible dongle!')

            self._serialport = serial_port.device
            self._bridge.set_serial_port(self._serialport)

        self._bridge.Open()

    #---------------------------------------------------------------------------
    ## Checks if all necessary services and characteristics have been
    ## discovered.
    ##
    ## :returns:   True if everything is as it should be
    ## :rtype:     bool
    ##
    def check_characteristics(self) -> bool:
        for serv in SP_UUIDS:
            if not (serv in self._bridge.ble_device.ServiceUUIDs):
                return False

            for char in SP_UUIDS[serv]:
                if not (char in self._bridge.ble_device.ServiceUUIDs[serv].Characteristics):
                    return False

        return True
        # if not ( self.SERV_CFG in self._bridge.ble_device.ServiceUUIDs ):
        #     return False

        # chars = self._bridge.ble_device.ServiceUUIDs[self.SERV_CFG].Characteristics
        # return (self.CHAR_CFG_DEV in chars) and (self.CHAR_CFG_DFU in chars) and (self.CHAR_CFG_DFUACK in chars)

    #---------------------------------------------------------------------------
    ## Clears device register bits.
    ##
    ## :param      bits:          The bits to be cleared
    ## :type       bits:          int
    ##
    ## :returns:   whether the bits have been cleared
    ## :rtype:     bool
    ##
    ## :raises     RuntimeError:  if invalid bits are set
    ##
    def clear_device_register(self, bits:int) -> bool:
        if bits > 15:
            raise RuntimeError('Invalid device register bits!')

        # read current device status register
        val = bytearray(self._bridge.read_char(SERV_CFG, CHAR_CFG_DEV))

        if( val[0] & bits == 0 ):
            return True

        # clear by or-ing with the corresponding bits
        val[0] &= (~bits & 0xFF)

        # write back and check success
        self._bridge.write_char(SERV_CFG, CHAR_CFG_DEV, bytes(val))
        val = self._bridge.read_char(SERV_CFG, CHAR_CFG_DEV)

        if( val[0] & bits == 0 ):
            return True
        else:
            return False

    #---------------------------------------------------------------------------
    ## Disables the acquisition procedure.
    ##
    ## :returns:   whether the procedure has been deactivated
    ## :rtype:     bool
    ##
    def disable_acquisition(self) -> bool:
        return self.clear_device_register(0x02)

    #---------------------------------------------------------------------------
    ## Disables the sensors.
    ##
    ## :returns:   whether the sensors have been deactivated
    ## :rtype:     bool
    ##
    def disable_sensors(self) -> bool:
        return self.clear_device_register(0x01)

    #---------------------------------------------------------------------------
    ## Enables the acquisition procedure.
    ##
    ## :returns:   whether the procedure has been activated
    ## :rtype:     bool
    ##
    def enable_acquisition(self) -> bool:
        return self.set_device_register(0x02)

    #---------------------------------------------------------------------------
    ## Enables the sensors.
    ##
    ## :returns:   whether the sensors have been activated
    ## :rtype:     bool
    ##
    def enable_sensors(self) -> bool:
        return self.set_device_register(0x01)

    #---------------------------------------------------------------------------
    ## Gets the patient identifier.
    ##
    ## :returns:   The patient identifier.
    ## :rtype:     int
    ##
    def get_patient_id(self) -> int:
        if self._patientID:
            return self._patientID

        # read patient id register
        val = bytearray(self._bridge.read_char(SERV_PAT, CHAR_PAT_ID))
        self._patientID = int.from_bytes(val, 'little')
        return self._patientID

    #---------------------------------------------------------------------------
    ## Wrapper function for print
    ##
    ## :param      args:  The arguments
    ## :type       args:  list
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def log_print(self, *args) -> None:
        if not self._logging:
            return

        print('[SP] ' + ' '.join(map(str, args)))


    #---------------------------------------------------------------------------
    ## Sets device register bits.
    ##
    ## :param      bits:          The bits to be set
    ## :type       bits:          int
    ##
    ## :returns:   whether the bits have been set
    ## :rtype:     bool
    ##
    ## :raises     RuntimeError:  if invalid bits are set
    ##
    def set_device_register(self, bits:int) -> bool:
        if bits > 15:
            raise RuntimeError('Invalid device register bits!')

        # read current device status register
        val = bytearray(self._bridge.read_char(SERV_CFG, CHAR_CFG_DEV))

        if( val[0] & bits == bits ):
            return True

        # activate by or-ing with the corresponding bits
        val[0] |= bits

        # write back and check success
        self._bridge.write_char(SERV_CFG, CHAR_CFG_DEV, bytes(val))
        val = self._bridge.read_char(SERV_CFG, CHAR_CFG_DEV)

        if( val[0] & bits == bits ):
            return True
        else:
            return False

    #---------------------------------------------------------------------------
    ## Sets the patient identifier.
    ##
    ## WARNING: This erases previously stored patient information.
    ##
    ## :param      pat_id:        The pattern identifier
    ## :type       pat_id:        int
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    ## :raises     RuntimeError:  { exception_description }
    ##
    def set_patient_id(self, pat_id:int) -> None:
        self._patientID = None

        # convert to uint16
        pat_id_b = pat_id.to_bytes(2, 'little')

        # write to device
        self._bridge.write_char(SERV_PAT, CHAR_PAT_ID, pat_id_b)
        res = self._bridge.read_char(SERV_PAT, CHAR_PAT_ID)

        if not res == pat_id_b:
            raise RuntimeError('Failed to set patient ID {}, {}!'.format(res, pat_id_b))

        self._patientID = pat_id

    #---------------------------------------------------------------------------
    ## Writes a measurement to the file.
    ##
    ## :param      file:    The log file
    ## :type       file:    { type_description }
    ## :param      pat_id:  The patient identifier
    ## :type       pat_id:  int
    ## :param      entry:   The measurements
    ## :type       entry:   dict
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def write_measurement(self, pat_id:int, entry:dict) -> None:

        self._file_lock.acquire()
        date = datetime.now().strftime('%Y-%m-%d %H:%M:%S:%f')
        self._file.write(date + ',' + str(pat_id))

        for e in entry:
            self._file.write(',' + str(entry[e]))

        self._file.write('\n')
        self._file_lock.release()

    #---------------------------------------------------------------------------
    ## CLI to the Smart Patch acquisition system. Should only be used, when this
    ## module is called as main
    ##
    ## :param      args:  The arguments
    ## :type       args:  argparse.Namespace
    ##
    ## :returns:   Nothing.
    ## :rtype:     None
    ##
    def _cli(self, args:argparse.Namespace) -> None:

        # setup the system
        self._logging = True
        self.Setup()
        self._file_lock = threading.Condition()

        # scan and connect
        if args.device:
            self.Connect(args.device)
        else:
            self.Scan()
            self.Connect()

        # query the current registered user
        print('Current subject ID: {}'.format(self.get_patient_id()))
        print('Correct? ', end='')

        if not cli_yesno():
            # Query patient data from user
            uid, bpm = cli_get_userdata()
            # write to device
            self.set_patient_id(uid)
            # todo: write bpm

        # open the log file
        self._file = open(args.file, "a")

        while 1:
            # get the first external measurements and write them to file
            entry = cli_get_sensordata()
            self.write_measurement(self.get_patient_id(), entry)

            # execute the acquisition run
            self._cli_run(900)

            print('\nPerform another run?')

            if not cli_yesno():
                break

        self._file.close()

    #---------------------------------------------------------------------------
    ## Performs an acquisition run.
    ##
    ## :param      timeout:  The timeout
    ## :type       timeout:  { type_description }
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def _cli_run(self, timeout) -> None:

        # create threading mechanisms
        evt_to      = threading.Event()     # the acquisition is complete
        evt_cancel  = threading.Event()     # the acquisition was cancelLed
        t_acq       = threading.Thread(     # the thread responsible for regular acquisition from the Smart Patch
            target  = self._t_acq,
            args    = (evt_to, evt_cancel, self.get_patient_id(), timeout, ),
            daemon  = True )

        input('Press [ENTER] when you are ready for the acquisition...')

        # Set up the device for acquisition

        print('Calibration complete! Acquisition started.')

        # Start the acquisition and user interaction
        t_acq.start()

        while 1:
            val = input('Enter [m] or [q]: ')

            if val == 'm':
                # get entry from user
                entry = cli_get_sensordata()
                # write data to file
                self.write_measurement(self.get_patient_id(), entry)
                print('Measurement complete!')
            elif val == 'q':
                evt_cancel.set()
                break

            if evt_to.is_set():
                break

        # wait for daemon thread to finish
        t_acq.join()

        # terminate acquisition

    #---------------------------------------------------------------------------
    ## Main thread function for the acquisition.
    ##
    ## :param      evt_to:      The event to
    ## :type       evt_to:      { type_description }
    ## :param      evt_cancel:  The event cancel
    ## :type       evt_cancel:  { type_description }
    ## :param      patient_id:  The patient identifier
    ## :type       patient_id:  { type_description }
    ## :param      timeout:     The timout
    ## :type       timeout:     { type_description }
    ##
    def _t_acq(self, evt_to, evt_cancel, patient_id, timeout):
        to = True

        for i in range(0, timeout):
            time.sleep(1)

            # read data from device

            self.write_measurement(patient_id, {})

            if evt_cancel.is_set():
                to = False
                break

        if to:
            evt_to.set()
            print('\nAcquisition completed. Press [ENTER] to continue')

#-------------------------------------------------------------------------------
## Queries the user for new user data.
##
## :returns:   The user id and the calibration blood pressure measurements.
## :rtype:     [int, list]
##
def cli_get_userdata() -> [int, list]:
    # get the user identifier
    while 1:
        try:
            uid = int(input('Enter the new user identifier (UID): '))
        except:
            print('Invalid data!')
            continue

        break

    # get the blood pressure measurements
    bpm = [None, None, None]
    for i in range(0,3):
        while 1:
            try:
                bp = input('Enter blood pressure measurement {}: '.format(i + 1))
                bpm[i] = list(map(int, bp.split(',')))
                if len(bpm[i]) != 3:
                    raise RuntimeError('')
            except:
                print('Invalid data!')
                continue

            break

    return uid, bpm

#-------------------------------------------------------------------------------
## Queries the user for external sensor data.
##
## :returns:   A dictionary with all measurements.
## :rtype:     dict
##
def cli_get_sensordata() -> dict:
    # first, read the three blood pressure measurements
    data = { 'bp': None, 'spo2': None, 'rr': None, 'temp': None}

    # Read BP data
    while 1:
        try:
            bp = input('Enter blood pressure measurement <SYS, DIA, PULSE>: ')
            data['bp'] = list(map(int, bp.split(',')))
            if len(data['bp']) != 3:
                raise RuntimeError('')
        except:
            print('Invalid data!')
            continue

        break

    # Read SPO2 data
    while 1:
        try:
            bp = input('Enter blood oxygen measurement <SPO2, PULSE>: ')
            data['spo2'] = list(map(int, bp.split(',')))
            if len(data['spo2']) != 2:
                raise RuntimeError('')
        except:
            print('Invalid data!')
            continue

        break

    # Read breath data
    input('Counting Breaths:\nPress [ENTER] to start 30s countdown.')
    for i in range(0, 30):
        _ = sys.stdout.write('\033[K')
        print('\r{}s remaining...'.format(30-i), end='')
        time.sleep(1)

    print("")

    while 1:
        try:
            data['rr'] = 2 * int(input('Enter number of breaths: '))
        except:
            print('Invalid data!')
            continue

        break

    # read temperature data
    while 1:
        try:
            data['temp'] = int(input('Enter your temperature (in degrees Celcius): '))
        except:
            print('Invalid data!')
            continue

        break


    entry = {
        'HR':       int((data['bp'][2] + data['spo2'][1])/2),
        'SPO2':     data['spo2'][0],
        'RR':       data['rr'],
        'BP':       data['bp'][0],
        'Temp':     data['temp'],
        'O2':       0,
        'ACVPU':    0,
        'EXT':      1
    }

    return entry

#-------------------------------------------------------------------------------
## Queries the user for yes or no.
##
## :returns:   Whether the user typed 'yes'
## :rtype:     bool
##
def cli_yesno() -> bool:
    while 1:
        res = input('Type [yes] or [no]: ')
        res.replace(' ', '')

        if res == 'yes':
            return True
        elif res == 'no':
            return False









def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Host application for the Smart Patch acquisition system.')
    parser.add_argument('-p', '--port', metavar='port', type=str, help='The serial port to use.')
    parser.add_argument('-d', '--device', metavar='device', type=str, help='The Bluetooth address of the device in the form xx:xx:xx:xx:xx:xx.')
    parser.add_argument('-f', '--file', metavar='file', type=str, help='The file to store the data in.')

    # parser.add_argument('file', metavar='Filename', type=str, help='Filename of the log.')
    # parser.add_argument('-c', '--confidence', help='Plot the confidence level.', action="store_true")
    # parser.add_argument('-o', '--oxygenation', help='Plot the SpO2 level.', action="store_true")
    # parser.add_argument('--peaks', help='Plot the breathing peaks.', action="store_true")
    # parser.add_argument('--rr', help='Plot the RR readings.', action="store_true")
    # parser.add_argument('--rvalue', help='Plot the RR readings.', action="store_true")
    # parser.add_argument('--reflectance', help='Plot the reflectance readings.', action="store_true")

    args = parser.parse_args()

    if args.device:
        args.device = args.device.split(':')
        if not len(args.device) == 6:
            raise RuntimeError('Invalid address supplied!')

        res = bytearray(6)
        for i in range(0, 6):
            res[5-i] = int(args.device[i], 16)

        args.device = bytes(res)

    if not args.file:
        args.file = datetime.now().strftime('%Y%m%d_%H%M%S') + '_log.csv'

    return args





if __name__ == '__main__':

    # parse the provided arguments
    args = parse_args()

    # initialise the object and run the CLI
    patch = SmartPatch(args.port, logging=True)
    patch._cli(args)

    exit()

    #---------------------------------------------------------------------------
    ## TO PORT
    ##
    print('Enabling acquisition.')
    if patch.enable_acquisition():
        print('  ok!')
    else:
        print('  failed!')

    print('Enabling sensor.')
    if patch.enable_sensors():
        print('  ok!')
    else:
        print('  failed!')

    print('Disabling sensor.')
    if patch.disable_sensors():
        print('  ok!')
    else:
        print('  failed!')

    print('Disabling acquisition.')
    if patch.disable_acquisition():
        print('  ok!')
    else:
        print('  failed!')
