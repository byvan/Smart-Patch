#! python3

import argparse
from datetime import datetime, timedelta
import re
import serial
import serial.tools.list_ports
import sys, threading, time

class SmartPatchDev(object):

    #---------------------------------------------------------------------------
    ## Constructs a new instance.
    ##
    ## :param      port:  The serial port to use
    ## :type       port:  str
    ##
    def __init__(self, port:str=None, subj:str=None):
        super(SmartPatchDev, self).__init__()

        self._stream = serial.Serial()
        self._stream.baudrate = 115200
        self._stream.timeout = 0.1        # 100 ms

        if port:
            self._stream.port = port

        if subj:
            self._patientID, self._patientData = read_BCV(subj)
        else:
            self._patientID = -1
            self._patientData = None

        # self._patientID = subj
        self.dummy = False

    #---------------------------------------------------------------------------
    ## Opens the serial port to the dev board.
    ##
    ## If no serial port was passed during instantiation, the function will scan
    ## for the board and connect to it.
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    ## :raises     RuntimeError:  if the dongle could not be found.
    ##
    def Setup(self) -> None:
        if self._stream.port == None:
            self._stream.port = self._find_board()

            if not self._stream.port:
                raise RuntimeError('Could not find compatible board!')

        else:
            self._stream.open()
            self._stop()

    #---------------------------------------------------------------------------
    ## Executes the commands necessary to calibrate the BP measurements.
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    ## :raises     RuntimeError:  { exception_description }
    ##
    def calib_bp(self) -> None:
        # write date and time
        res = self._write(b'set_cfg bpt date_time ' + bytes(datetime.now().strftime('%Y%m%d %H%M%S'), 'ascii') + b'\r')
        res = cli_parse_result(res.decode('ascii'))
        if not res['err'] == '0':
            raise RuntimeError('Failed to set date')

        # start the calibration procedure
        self._unpause()
        self._start(0)

        while 1:
            # read data from device
            data = self._read()
            data = data.decode('ascii').split(',')

            if int(data[4]) == 100:
                print('\nCalibration complete!')
                self._stop()

                break
            else:
                _ = sys.stdout.write('\033[K')
                print('\rCalibrating... ({}% done)'.format(int(data[4])), end='')

        res = self._write(b'get_cfg bpt cal_result\r')
        res = cli_parse_result(res.decode('ascii'))
        if not res['err'] == '0':
            raise RuntimeError('Failed to read calibration vector!')

        # write calibration vector to file
        file = open('./CV_User_{}.bcv'.format(self.get_patient_id()), "ab")
        file.write(self._patientID.to_bytes(4, 'little'))
        file.write(bytes.fromhex(res['value']))
        file.close()

    #---------------------------------------------------------------------------
    ## Gets the patient identifier.
    ##
    ## :returns:   The patient identifier.
    ## :rtype:     int
    ##
    def get_patient_id(self) -> int:
        return self._patientID

    #---------------------------------------------------------------------------
    ## Reads data from the serial port.
    ##
    ## :returns:   The dictionary containing the data.
    ## :rtype:     dict
    ##
    def read_data(self) -> dict:
        
        data = self._read()
        data = data.decode('ascii').split(',')

        entry = {
            'LED_IR':   int(data[1]),
            'LED_RED':  int(data[2]),
            'HR':       int(float(data[3])),
            'BPS':      data[5],
            'BPD':      data[6],
            'SPO2':     int(float(data[7])),
            'R':        data[8],
            'Conf':     int(float(data[11])),
            'Status':   int(data[0]),
            'Perc':     int(data[4]),
            'Temp':     0,
            'O2':       0,
            'ACVPU':    0,
            'EXT':      0
        }

        return entry

    def set_bcv(self, data):
        print(data.hex())
        res = self._write(b'set_cfg bpt cal_index 0\r')
        res = cli_parse_result(res.decode('ascii'))
        if not res['err'] == '0':
            raise RuntimeError('Failed to set bcv 1.')

        res = self._write(bytes('sset_cfg bpt cal_result {}\r'.format(data), 'ascii'))
        res = cli_parse_result(res.decode('ascii'))
        if not res['err'] == '0':
            raise RuntimeError('Failed to set bcv 2.')

    #---------------------------------------------------------------------------
    ## Sets the patient identifier.
    ##
    ## WARNING: This erases previously stored patient information.
    ##
    ## :param      pat_id:        The pattern identifier
    ## :type       pat_id:        int
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    ## :raises     RuntimeError:  { exception_description }
    ##
    def set_patient_id(self, pat_id:int) -> None:
        self._patientID = pat_id

    #---------------------------------------------------------------------------
    ## Sets the patient bp calibration values.
    ##
    ## :param      bpm:           The new values [[sys, dia, bpm] x 3] to store.
    ## :type       bpm:           list
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    ## :raises     RuntimeError:  { exception_description }
    ##
    def set_patient_bp(self, bpm) -> None:
        res = self._write(bytes('set_cfg bpt sys_dia 0 {} {}\r'.format(bpm[0], bpm[1]), 'ascii'))
        res = cli_parse_result(res.decode('ascii'))
        if not res['err'] == '0':
            raise RuntimeError('Failed to set bp.')

    #---------------------------------------------------------------------------
    ## Writes a measurement to the file.
    ##
    ## :param      file:    The log file
    ## :type       file:    { type_description }
    ## :param      pat_id:  The patient identifier
    ## :type       pat_id:  int
    ## :param      entry:   The measurements
    ## :type       entry:   dict
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def write_measurement(self, pat_id:int, entry:dict) -> None:

        self._file_lock.acquire()
        date = datetime.now().strftime('%Y-%m-%d %H:%M:%S:%f')
        self._file.write(date + ',' + str(pat_id))

        for e in entry:
            self._file.write(',' + str(entry[e]))

        self._file.write('\n')
        self._file_lock.release()

    #---------------------------------------------------------------------------
    ## CLI to the Smart Patch DEV acquisition system. Should only be used, when
    ## this module is called as main
    ##
    ## :param      args:  The arguments
    ## :type       args:  argparse.Namespace
    ##
    ## :returns:   Nothing.
    ## :rtype:     None
    ##
    def _cli(self, args:argparse.Namespace) -> None:

        # setup the system
        self.Setup()
        self._file_lock = threading.Condition()

        res = self._write(b'get_device_info\r')
        print(cli_parse_result(res.decode('ascii')))

        res = self._write(b'set_cfg bpt date_time ' + bytes(datetime.now().strftime('%Y%m%d %H%M%S'), 'ascii') + b'\r')
        res = cli_parse_result(res.decode('ascii'))
        if not res['err'] == '0':
            raise RuntimeError('Failed to set date {}'.format(res))

        # # query the current registered user
        print('Current subject ID: {}'.format(self.get_patient_id()))
        print('Correct? ', end='')

        if not cli_yesno():
            # Query patient data from user
            self._cli_bp()
            self.calib_bp()
        else:
            self.set_bcv(self._patientData)

        # open the log file
        # self._file = open(args.file, "a")

        if args.mode == 'regular':
            self._cli_regular()
        elif args.mode == 'continuous':
            self._cli_continuous()
        else:
            raise RuntimeError('Invalid mode {} set!'.format(args.mode))


        # while 1:
        #     # get the first external measurements and write them to file
        #     entry = cli_get_sensordata(self.dummy)
        #     self.write_measurement(self.get_patient_id(), entry)

        #     # execute the acquisition run
        #     self._cli_run(1800)

        #     print('\nPerform another run?')

        #     if not cli_yesno():
        #         break

        # self._file.close()

    def _cli_continuous(self):
        # open the log file
        self._file = open(args.file, "a")

        for i in range(10):
            input('Press [ENTER] to start measurement {}/10.'.format(i+1))

            start = datetime.now()
            end = start + timedelta(seconds=60)

            self._pause()
            self._start(1)
            time.sleep(5)
            self._unpause()

            while 1:
                now = datetime.now()

                if now > end:
                    print('Estimation took over 60s!')
                    break

                # read data from device
                entry = self.read_data()
                self.write_measurement(self.get_patient_id(), entry)
                elapsed = datetime.now() - start

                _ = sys.stdout.write('\033[K')
                print('\r{}s elapsed ({}%)...'.format(elapsed.seconds, entry['Perc']), end='')

                if entry['Status'] == 2:
                    print('\r\nEstimation complete.')
                    entry = self._cli_userdata()
                    entry['R'] = 60 * entry['R']/elapsed.seconds
                    self.write_measurement(self.get_patient_id(), entry)
                    break

            self._stop()
            while datetime.now() < end:
                time.sleep(0.5)

        print('\nAcquisition completed.')

    def _cli_userdata(self):
        # first, read the three blood pressure measurements
        data = { 'bp': None, 'spo2': None, 'rr': None}

        # Read breath data
        while 1:
            try:
                data['rr'] = int(input('Enter number of breaths: '))
            except:
                print('Invalid data!')
                continue

            break

        # Read BP data
        while 1:
            try:
                bp = input('Enter blood pressure measurement <SYS, DIA, PULSE>: ')
                data['bp'] = list(map(int, bp.split(',')))
                if len(data['bp']) != 3:
                    raise RuntimeError('')
            except:
                print('Invalid data!')
                continue

            break

        # Read SPO2 data
        while 1:
            try:
                bp = input('Enter blood oxygen measurement <SPO2, PULSE>: ')
                data['spo2'] = list(map(int, bp.split(',')))
                if len(data['spo2']) != 2:
                    raise RuntimeError('')
            except:
                print('Invalid data!')
                continue

            break

        entry = {
            'LED_IR':   0,
            'LED_RED':  0,
            'HR':       int((data['bp'][2] + data['spo2'][1])/2),
            'BPS':      data['bp'][0],
            'BPD':      data['bp'][1],
            'SPO2':     data['spo2'][0],
            'R':        data['rr'],
            'Conf':     1,
            'Status':   2,
            'Perc':     0,
            'Temp':     37,
            'O2':       0,
            'ACVPU':    0,
            'EXT':      1
        }

        return entry

    def _cli_regular(self):
        # open the log file
        self._file = open(args.file, "a")
        while 1:
            # get the first external measurements and write them to file
            entry = cli_get_sensordata(self.dummy)
            self.write_measurement(self.get_patient_id(), entry)
            # execute the acquisition run
            self._cli_run(1800)

            print('\nPerform another run?')

            if not cli_yesno():
                break

        self._file.close()

    #---------------------------------------------------------------------------
    ## Runs the user BP calibration procedure.
    ##
    def _cli_bp(self):
        uid = cli_get_UID(self.dummy)
        self.set_patient_id(uid)

        bpm = cli_get_bp(self.dummy)
        self.set_patient_bp(bpm)

    #---------------------------------------------------------------------------
    ## Performs an acquisition run.
    ##
    ## :param      timeout:  The timeout
    ## :type       timeout:  { type_description }
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    def _cli_run(self, timeout) -> None:

        # create threading mechanisms
        evt_to      = threading.Event()     # the acquisition is complete
        evt_cancel  = threading.Event()     # the acquisition was cancelLed
        t_acq       = threading.Thread(     # the thread responsible for regular acquisition from the Smart Patch
            target  = self._t_acq,
            args    = (evt_to, evt_cancel, self.get_patient_id(), timeout, ),
            daemon  = True )

        # Start the acquisition and user interaction
        input('Press [ENTER] when you are ready for the acquisition...')
        t_acq.start()
        time.sleep(5)
        print('Calibration complete! Acquisition started.')

        sleep_until = datetime.now() + timedelta(seconds=300)

        try:

            while 1:
                start = datetime.now()

                if evt_to.is_set():
                    break

                if sleep_until > start:
                    time.sleep(0.5)
                    continue

                entry = cli_get_sensordata(self.dummy)
                # write data to file
                self.write_measurement(self.get_patient_id(), entry)
                print('Measurement complete!')
                sleep_until = datetime.now() + timedelta(seconds=300)
        except KeyboardInterrupt:
            pass
        finally:
            t_acq.join()

            # terminate acquisition
            self._stop()

            entry = cli_get_sensordata(self.dummy)
            # write data to file
            self.write_measurement(self.get_patient_id(), entry)
            print('Measurement complete!')


            # val = input('Enter [m] or [q]: ')

            # if val == 'm':
            #     # get entry from user
            #     entry = cli_get_sensordata(self.dummy)
            #     # write data to file
            #     self.write_measurement(self.get_patient_id(), entry)
            #     print('Measurement complete!')
            # elif val == 'q':
            #     evt_cancel.set()
            #     break

        # # wait for daemon thread to finish
        # t_acq.join()

        # # terminate acquisition
        # self._stop()

    #---------------------------------------------------------------------------
    ## Finds the board connected to any serial port.
    ##
    ## :returns:   The name of the serial port
    ## :rtype:     str
    ##
    ## :raises     RuntimeError:  { exception_description }
    ##
    def _find_board(self) -> str:
        print('Scanning for dev board...')
        ports = serial.tools.list_ports.comports()

        for port in ports:
            print('\tTrying {}'.format(port.device))
            self._stream.port = port.device

            try:

                if not self._stream.is_open:
                    self._stream.open()

                    if not self._stream.is_open:
                        raise RuntimeError('failed to open port')

                res = self._stop()

                if res['err'] == '0':
                    break
                else:
                    raise RuntimeError('incompatible device')
                
            except Exception:
                self._stream.close()
                self._stream.port = None
            else:
                break

        return self._stream.port

    #---------------------------------------------------------------------------
    ## Pauses measurement dump.
    ##
    ## :raises     RuntimeError:  { exception_description }
    ##
    def _pause(self):
        res = self._write(b'pause 1\r')
        res = cli_parse_result(res.decode('ascii'))

        if not res['err'] == '0':
            raise RuntimeError('Failed to pause')

    #---------------------------------------------------------------------------
    ## Reads a line from the serial port.
    ##
    ## :returns:   The data read.
    ## :rtype:     bytes
    ##
    def _read(self) -> bytes:
        res = bytearray()

        while 1:
            res += self._stream.read(1)

            if bytes(res[-2:]) == b'\r\n':
                break

        return bytes(res[:-2])

    #---------------------------------------------------------------------------
    ## Starts the BPT algorithm in the given mode.
    ##
    ## :param      mode:          The mode
    ## :type       mode:          int
    ##
    ## :returns:   Nothing
    ## :rtype:     None
    ##
    ## :raises     RuntimeError:  { exception_description }
    ##
    def _start(self, mode:int) -> None:
        res = self._write(bytes('read bpt {}\r'.format(mode), 'ascii'))
        res = cli_parse_result(res.decode('ascii'))

        if not res['err'] == '0':
            raise RuntimeError('Failed to start')

    #---------------------------------------------------------------------------
    ## Stops the BPT algorithm.
    ##
    ## :returns:   The response dictionary
    ## :rtype:     dict
    ##
    ## :raises     RuntimeError:  { exception_description }
    ##
    def _stop(self) -> dict:
        res = self._write(b'stop\r')
        res = cli_parse_result(res.decode('ascii'))
        if not res['err'] == '0':
            raise RuntimeError('Failed to stop')

        return res

    #---------------------------------------------------------------------------
    ## Main thread function for the acquisition.
    ##
    ## :param      evt_to:      The event to
    ## :type       evt_to:      { type_description }
    ## :param      evt_cancel:  The event cancel
    ## :type       evt_cancel:  { type_description }
    ## :param      patient_id:  The patient identifier
    ## :type       patient_id:  { type_description }
    ## :param      timeout:     The timout
    ## :type       timeout:     { type_description }
    ##
    def _t_acq(self, evt_to, evt_cancel, patient_id, timeout):
        to = True

        start = datetime.now()
        sleep_until = start
        end = start + timedelta(seconds=timeout)

        while 1:

            self._stop()
            start = datetime.now()

            if start > end:
                break

            if sleep_until > start:
                time.sleep(0.5)
                continue

            print('Estimation started!')
            self._pause()
            self._start(1)
            time.sleep(5)
            self._unpause()

            while 1:
                if datetime.now() > end:
                    break

                # read data from device
                entry = self.read_data()

                self.write_measurement(patient_id, entry)

                if evt_cancel.is_set():
                    to = False
                    break

                if entry['Status'] == 2:
                    print('Estimation complete.')
                    break

                if datetime.now() > start + timedelta(seconds=45):
                    print('Estimation cancelled! Took > 45s.')
                    break

            if to == False:
                break

            sleep_until = datetime.now() + timedelta(seconds=10)

        self._stop()

        if to:
            evt_to.set()
            print('\nAcquisition completed. Press [ENTER] to continue')

    #---------------------------------------------------------------------------
    ## Resumes measurement dump.
    ##
    ## :raises     RuntimeError:  { exception_description }
    ##
    def _unpause(self):
        res = self._write(b'pause 0\r')
        res = cli_parse_result(res.decode('ascii'))

        if not res['err'] == '0':
            raise RuntimeError('Failed to unpause')

    #---------------------------------------------------------------------------
    ## Write data to the serial port
    ##
    ## :param      data:  The data to be written
    ## :type       data:  bytes
    ##
    ## :returns:   The resulting line
    ## :rtype:     bytes
    ##
    def _write(self, data:bytes) -> bytes:
        self._stream.write(data)

        res = bytearray()
        while 1:
            res += self._stream.read(1)
            if bytes(res[-len(data):]) == data:
                break

        res = bytearray()
        while 1:
            res += self._stream.read(1)
            if bytes(res[-len(data):-1]) == data[:-1]:
                break

        res = bytearray()
        while 1:
            res += self._stream.read(1)
            if bytes(res[-2:]) == b'\r\n':
                break

        return bytes(res[:-2])

#-------------------------------------------------------------------------------
## Queries the user for BP measurements.
##
## :type       dummy:  { type_description }
## :param      dummy:  The dummy
##
## :returns:   { description_of_the_return_value }
## :rtype:     list
##
def cli_get_bp(dummy) -> list:
    if not dummy:
        while 1:
            try:
                bp = input('Enter blood pressure measurement <SYS, DIA, PULSE>: ')
                data = list(map(int, bp.split(',')))
                if len(data) != 3:
                    raise RuntimeError('')
            except (RuntimeError, ValueError):
                print('Invalid data!')
                continue

            break

        return data
    else:
        return [121, 71, 65]

#-------------------------------------------------------------------------------
## Queries the user for external sensor data.
##
## :returns:   A dictionary with all measurements.
## :rtype:     dict
##
def cli_get_sensordata(dummy) -> dict:

    if not dummy:

        # first, read the three blood pressure measurements
        data = { 'bp': None, 'spo2': None, 'rr': None, 'temp': None}

        # Read BP data
        while 1:
            try:
                bp = input('Enter blood pressure measurement <SYS, DIA, PULSE>: ')
                data['bp'] = list(map(int, bp.split(',')))
                if len(data['bp']) != 3:
                    raise RuntimeError('')
            except:
                print('Invalid data!')
                continue

            break

        # Read SPO2 data
        while 1:
            try:
                bp = input('Enter blood oxygen measurement <SPO2, PULSE>: ')
                data['spo2'] = list(map(int, bp.split(',')))
                if len(data['spo2']) != 2:
                    raise RuntimeError('')
            except:
                print('Invalid data!')
                continue

            break

        # Read breath data
        input('Counting Breaths:\nPress [ENTER] to start 30s countdown.')
        for i in range(0, 30):
            _ = sys.stdout.write('\033[K')
            print('\r{}s remaining...'.format(30-i), end='')
            time.sleep(1)

        print("")

        while 1:
            try:
                data['rr'] = 2 * int(input('Enter number of breaths: '))
            except:
                print('Invalid data!')
                continue

            break

        # read temperature data
        while 1:
            try:
                data['temp'] = int(input('Enter your temperature (in degrees Celcius): '))
            except:
                print('Invalid data!')
                continue

            break

        entry = {
            'LED_IR':   0,
            'LED_RED':  0,
            'HR':       int((data['bp'][2] + data['spo2'][1])/2),
            'BPS':      data['bp'][0],
            'BPD':      data['bp'][1],
            'SPO2':     data['spo2'][0],
            'R':        data['rr'],
            'Conf':     1,
            'Status':   2,
            'Perc':     0,
            'Temp':     data['temp'],
            'O2':       0,
            'ACVPU':    0,
            'EXT':      1
        }

        return entry

    else:
        return {
        'LED_IR':   0,
        'LED_RED':  0,
        'HR':       99,
        'BPS':      120,
        'BPD':      70,
        'SPO2':     99,
        'R':        0,
        'Conf':     1,
        'Status':   2,
        'Perc':     0,
        'Temp':     37,
        'O2':       0,
        'ACVPU':    0,
        'EXT':      1
        }

#-------------------------------------------------------------------------------
## Queries the user for the UID
##
## :type       dummy:  bool
## :param      dummy:  if dummy patient is used.
##
## :returns:   the UID
## :rtype:     int
##
def cli_get_UID(dummy:bool) -> int:
    if not dummy:
        while 1:
            try:
                uid = int(input('Enter the new user identifier (UID): '))
            except:
                print('Invalid UID!')
                continue

            break

        return uid
    else:
        return 99999

#-------------------------------------------------------------------------------
## Queries the user for new user data.
##
## :returns:   The user id and the calibration blood pressure measurements.
## :rtype:     [int, list]
##
def cli_get_userdata(dummy) -> [int, list]:

    if not dummy:

        # get the user identifier
        while 1:
            try:
                uid = int(input('Enter the new user identifier (UID): '))
            except:
                print('Invalid data!')
                continue

            break

        # get the blood pressure measurements
        bpm = [None, None, None]
        for i in range(0,3):
            while 1:
                try:
                    bp = input('Enter blood pressure measurement {}: '.format(i + 1))
                    bpm[i] = list(map(int, bp.split(',')))
                    if len(bpm[i]) != 3:
                        raise RuntimeError('')
                except:
                    print('Invalid data!')
                    continue

                break

        return uid, bpm

    else:
        return 99999, [[120, 75, 68], [121, 78, 70], [120, 79, 69]]

#-------------------------------------------------------------------------------
## Queries the user for yes or no.
##
## :returns:   Whether the user typed 'yes'
## :rtype:     bool
##
def cli_yesno() -> bool:
    while 1:
        res = input('Type [yes] or [no]: ')
        res.replace(' ', '')

        if res == 'yes':
            return True
        elif res == 'no':
            return False

#-------------------------------------------------------------------------------
## Parses the serial byte array into a dict.
##
## :param      input:  The input bytes
## :type       input:  bytes
##
## :returns:   Dictionary containing the results of the previous query
## :rtype:     dict
##
def cli_parse_result(input:bytes) -> dict:
    return dict(re.findall(r'(\S+)=(".*?"|\S+)', input))

#-------------------------------------------------------------------------------
## Parse the command line arguments for this module's main.
##
## :returns:   The arguments
## :rtype:     argparse.Namespace
##
def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Host application for the Smart Patch DEV acquisition system.')
    parser.add_argument('-p', '--port', metavar='port', type=str, help='The serial port to use.')
    parser.add_argument('-f', '--file', metavar='file', type=str, help='The file to store the data in.')
    parser.add_argument('-u', '--user', metavar='user', type=str, help='The user file to use (in case of returning user).')
    parser.add_argument('-m', '--mode', metavar='Mode', type=str, help='The CLI mode to use (options are <regular> or <continuous>')
    parser.add_argument('--dummy', action='store_true', help='Use dummy patient.')

    args = parser.parse_args()

    if not args.file:
        args.file = datetime.now().strftime('%Y%m%d_%H%M%S') + '_log.csv'

    if not args.mode:
        args.mode = 'regular'

    # if not args.user:
    #     args.user = -1

    return args

def read_BCV(file:str):
    f = open(file, "rb")
    data = f.read()
    f.close()

    uid = int.from_bytes(data[:4], 'little')
    data = data[4:]

    return uid, data

if __name__ == '__main__':
    # parse the provided arguments
    args = parse_args()

    # initialise the object and run the CLI
    patch = SmartPatchDev(args.port, subj=args.user)
    patch.dummy = args.dummy
    patch._cli(args)
